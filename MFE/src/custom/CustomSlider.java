package custom;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JSlider;

import view.MainView;

/**
 * Custom slide (no need to rewrite its syle everytime) 
 * @author nicolasbernier
 *
 */
public class CustomSlider extends JSlider implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Font fontSlider=new Font(MainView.FONT, Font.PLAIN, 14);
	/**
	 * Constructor
	 * @param orientation - orientation of the slider
	 * @param min - minimum value
	 * @param max - maximum value
	 * @param defaultValue - default value
	 */
	public CustomSlider(int orientation, int min, int max, int defaultValue){
		super(orientation,min,max,defaultValue);
		this.init();
	}
	/**
	 * Constructor
	 * @param orientation - orientation of the slider
	 */
	public CustomSlider(int orientation){
		super(orientation);
		this.init();
	}
	private void init(){
		this.setPaintTicks(true);
		this.setPaintLabels(true);
		this.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.setFocusTraversalKeysEnabled(false);
		this.setForeground(MainView.COLOR_TEXT);
		this.setBackground(MainView.CONTROL_COLOR);
		this.setFont(this.fontSlider);
		this.setOpaque(true);
		this.setFocusable(false);
		this.addMouseListener(this);
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
	}
	@Override
	public void mouseExited(MouseEvent e) {
		this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
