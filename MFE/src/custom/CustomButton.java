package custom;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;

import view.MainView;
/**
 * Custom button (no need to rewrite its style everytime)
 * @author nicolasbernier
 *
 */
public class CustomButton extends JButton implements MouseListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color colorText=new Color(60,60,60);
	private Color colorTextHover=new Color(20,20,20);
	private Font font=new Font(MainView.FONT, Font.BOLD, 15);
	/**
	 * Constructor
	 * @param label - label of the button
	 */
	public CustomButton(String label){
		super(label);
		this.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.setHorizontalAlignment(JFormattedTextField.CENTER);
		this.setFont(this.font);
		this.setBorder(BorderFactory.createMatteBorder(0,0,2,0,MainView.COLOR_INPUT));
		this.setBackground(Color.WHITE);
		this.setForeground(this.colorText);
		this.setOpaque(true);
		this.setFocusPainted(false);
		this.addMouseListener(this);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		this.setForeground(this.colorTextHover);
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.setOpaque(true);
	}
	@Override
	public void mouseExited(MouseEvent e) {
		this.setForeground(this.colorText);
		this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		this.setOpaque(true);
	}
	@Override
	public void mousePressed(MouseEvent e) {
		this.setBackground(MainView.COLOR_BUTTON_SELECTED);
		this.setOpaque(true);		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		this.setBackground(Color.WHITE);
		this.setOpaque(true);		
	}
	
}
