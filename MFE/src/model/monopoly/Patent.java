package model.monopoly;

import model.Agent;

public class Patent {
	private Agent agent;
	private int product;
	private int startingStep;
	private int endingStep = -1;
	private boolean alive;
	
	public Patent(Agent agent, int product, int startingStep, boolean alive) {
		super();
		this.agent = agent;
		this.product = product;
		this.startingStep = startingStep;
		this.alive = alive;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public int getStartingStep() {
		return startingStep;
	}

	public void setStartingStep(int startingStep) {
		this.startingStep = startingStep;
	}

	public int getEndingStep() {
		return endingStep;
	}

	public void setEndingStep(int endingStep) {
		this.endingStep = endingStep;
	}

	public boolean isAlive() {
		return alive;
	}

	private void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public void setDead(int currentStep){
		endingStep = currentStep;
		setAlive(false);
	}
}
