package model.monopoly;

import helper.Helper;

import java.util.ArrayList;

import model.Agent;
import model.World;

public class PatentOffice {
	private int numberOfPatents = 0;
	
	private ArrayList<Patent> patents;
	
	public PatentOffice(){
		patents = new ArrayList<Patent>();
	}

	public void init(Agent[] theAgents) {
		int numberOfProducts = theAgents[0].getNumberOfProducts();
		int patentLimit = Math.min(numberOfProducts, numberOfPatents);
		for(int i = 0 ; i < patentLimit ; ++i){
			boolean done = false;
			int attempts = 0;
			while(!done && attempts < World.MAXNUMBEROFATTEMPTS){
				Agent a = theAgents[World.ran.nextInt(theAgents.length)];
				if(!hasPatent(a)){
					double[] skills = a.getMyProducer().getMySkills();
					ArrayList<Integer> bestProducts = Helper.getMinIndexes(skills);
					int bestProduct = bestProducts.get(World.ran.nextInt(bestProducts.size()));
					if(!isPatented(bestProduct)){
						Patent patent = new Patent(a, bestProduct, 0,true);
						patents.add(patent);
						done = true;
					}
				}
				++attempts;
			}
			if(!done){
				int position = 1;
				while(!done && position < theAgents[0].getNumberOfProducts()){
					attempts = 0;
					while(!done && attempts < World.MAXNUMBEROFATTEMPTS){
						Agent a = theAgents[World.ran.nextInt(theAgents.length)];
						if(!hasPatent(a)){
							double[] skills = a.getMyProducer().getMySkills();
							ArrayList<Integer> bestProducts = Helper.getAscendingIndexes(skills);
							int bestProduct = bestProducts.get(position);
							if(!isPatented(bestProduct)){
								Patent patent = new Patent(a, bestProduct, 0,true);
								patents.add(patent);
								done = true;
							}
						}
						++attempts;
					}
					++position;
				}
			}
		}
	}
	
	public boolean canProduce(Agent a, int product){
		boolean result = true;
		Agent patentOwner = getPatentOwner(product);
		if(patentOwner != null){
			result = a.equals(patentOwner);
		}
		return result;
	}
	
	
	public int getPatentedProduct(Agent agent){
		int patentedProduct = -1;
		for(int i = 0 ; i < patents.size() ; ++i){
			Patent patent = patents.get(i);
			if(!patent.isAlive()){
				break;
			} else if(agent.equals(patent.getAgent())){
				patentedProduct = patent.getProduct();
				break;
			}
		}
		
		return patentedProduct;
	}
	
	public boolean hasPatent(Agent agent){
		return getPatentedProduct(agent) > -1;
	}
	
	public Agent getPatentOwner(int product){
		Agent owner = null;
		for(int i = 0 ; i < patents.size() ; ++i){
			Patent patent = patents.get(i);
			if(!patent.isAlive()){
				break;
			} else if(product == patent.getProduct()){
				owner = patent.getAgent();
				break;
			}
		}
		
		return owner;
	}
	
	public boolean isPatented(int product){
		return getPatentOwner(product) != null;
	}

	public int getNumberOfPatents() {
		return numberOfPatents;
	}

	public void setNumberOfPatents(int numberOfPatents) {
		this.numberOfPatents = numberOfPatents;
	}

	public ArrayList<Patent> getPatents() {
		return patents;
	}
}
