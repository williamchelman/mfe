package model;

import helper.Helper;

/**
 * The competitive buyer part of the agent
 * @author nicolasbernier
 *
 */
public class CompetitiveBuyer extends Buyer
{
	/**
     * Constructor of the competitive buyer
     * @param myAgent - the parent agent
     */
	public CompetitiveBuyer(Agent myAgent){
	      super(myAgent);
    }
	@SuppressWarnings("deprecation")
	@Override
	public Offer buy(Offer sellingOffer) {
		myAgent.incrementBuyingChances();
		int product=-1;
		if(sellingOffer==null){
			product = selectProductToBuy();
		}
		else{
			product=sellingOffer.getTheProduct();
		}
        Offer o1 = null;
        double price = 0.0;
        if (product != -1)
        {
        	Offer o2=null;
        	Market m=myAgent.getTheMarket();
        	if(m instanceof CompetitiveMarket && m!=null && ((CompetitiveMarket)m).getBestBuyingOffers()!=null){
        		o2=((CompetitiveMarket)m).getBestBuyingOffers()[product];
        	}
        	else if(m instanceof MixedMarket && m!=null && ((MixedMarket)m).getBestBuyingOffers()!=null){
        		o2=((MixedMarket)m).getBestBuyingOffers()[product];
        	}
            double reservPrice = myAgent.getMyConsumer().getMyTastes()[product] * (myAgent.getMoney() * m.getTimeIndex());
            if (o2!=null && reservPrice >= o2.getPrice())
            {
                price = World.getRandomNumber(o2.getPrice(), reservPrice);
                o1 = new Offer(product, myAgent, price, Direction.buy);
                if(m instanceof CompetitiveMarket){
                	((CompetitiveMarket)myAgent.getTheMarket()).getBestBuyingOffers()[product] = o1;
            	}
            	else if(m instanceof MixedMarket){
            		((MixedMarket)myAgent.getTheMarket()).getBestBuyingOffers()[product] = o1;
            	}
            }
        }
        
        return o1;
	}
	@SuppressWarnings("deprecation")
	@Override
	public int selectProductToBuy() {
        double maxUtility = 0;
        int bestProduct = -1;
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
        	if(!Helper.isInside(this.productProposed, i)){
	            double reservPrice = myAgent.getMyConsumer().getMyTastes()[i] * (myAgent.getMoney() * myAgent.getTheMarket().getTimeIndex()); // Taken into account the budget constraint
	            Market m=myAgent.getTheMarket();
	            double price=0.0;
	            Offer bestOffer=null;
	        	if(m instanceof CompetitiveMarket && m!=null && ((CompetitiveMarket)m).getBestBuyingOffers()!=null){
	        		bestOffer=((CompetitiveMarket)m).getBestBuyingOffers()[i];
	        	}
	        	else if(m instanceof MixedMarket && m!=null && ((MixedMarket)m).getBestBuyingOffers()!=null){
	        		bestOffer=((MixedMarket)m).getBestBuyingOffers()[i];
	        	}
	        	if(bestOffer!=null){
	        		price=bestOffer.getPrice();
	        	}
	            if (reservPrice >= price)
	            {
	                if (myAgent.getMyConsumer().getMyTastes()[i] > maxUtility)
	                {
	                    maxUtility = myAgent.getMyConsumer().getMyTastes()[i];
	                    bestProduct = i;
	                }
	            }
        	}
        }
        if (bestProduct == -1)
        {
            myAgent.frustrate(); // The agent can't buy anything. 
        }
        //Allow exploration
        if(bestProduct!=-1){
	        this.productProposedAttemps[bestProduct]++;
	        if(this.productProposedAttemps[bestProduct]>=World.NUM_REPETITION_PRODUCT_SELLER_BUYER){
	        	this.productProposed.add((Integer)bestProduct);
	        }
        }
        return bestProduct;
    }
}
