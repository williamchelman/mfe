package model;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * The class component of the model, the central one
 * @author nicolasbernier
 *
 */
public abstract class Market
{
    protected Agent[] theAgents;
    private ArrayList<Transaction> allTransactions;
    protected double[] productPrice;  // Store the last price of the product
    protected int numberOfMarketFailures;
    protected int numberOfTransactions;
    protected int numberOfLastTransactions;
    protected int numberOfProducts,initialProduction,numberOfAgents;
    protected double maxBenefit;
    protected Agent lastBuyer;
    protected Agent lastSeller;
    protected int lastProductSold; // Last product sold
    protected double lastPrice; // Last transaction price
    protected int lastProduction; // last production done by the random producer
    protected int[] productions;
    protected int[] productsSold;
    protected double timeIndex;
    protected double randomness;
    private double[] avgPrices;

    /**
     * Constructor
     * @param theAgents - agents of the market
     * @param initialProduction - the amount of initial production
     * @param numberOfLastTransactions - the number of last transactions on which the productions are based
     * @param numberOfProducts - the amount of product in the market
     * @param maxBenefit - the maximum benefit of a transaction (upper bound)
     */
    public Market(Agent[] theAgents,int initialProduction,int numberOfLastTransactions,int numberOfProducts,double maxBenefit)
    {
        this.theAgents = theAgents;
        this.numberOfAgents=theAgents.length;
        this.numberOfMarketFailures = 0;
        this.initialProduction=initialProduction;
        this.numberOfLastTransactions=numberOfLastTransactions;
        this.numberOfProducts=numberOfProducts;
        this.allTransactions = new ArrayList<Transaction>();
        this.productPrice = new double[this.numberOfProducts];
        this.productions = new int[this.numberOfProducts + this.initialProduction];
        this.productsSold = new int[this.numberOfProducts];
        this.maxBenefit=maxBenefit;
        this.computeTimeIndex();
    }
    /**
     * Compute the time index of the market (for budget constraint)
     */
    private void computeTimeIndex() {
    	double val=0;
    	for(int i=0;i<this.theAgents.length;i++){
    		if(this.theAgents[i].getInitialMoney()>val){
    			val=this.theAgents[i].getInitialMoney();
    		}
    	}
    	this.timeIndex=1/val;
	}
    /**
     * Update the last transactions 
     * @param t - the last transaction 
     */
	public void updateLastTransactions(Transaction t)
    {
		allTransactions.add(t);
        productPrice[t.getTheProduct()] = t.getPrice();
        productsSold[t.getTheProduct()]++;
    }
	/**
	 * Create a transaction according the type of market
	 * @return - the transaction created
	 */
    public abstract Transaction createTransaction();
    /**
     * Initial production of the market
     */
    public void onlyProduce() //RJ version
    {
        for (Agent ag : theAgents)
        {
            ag.actProduce();
            if(ag.getMyProducer().getLastProduction()!=-1){
            	productions[ag.getMyProducer().getLastProduction()]++;
            	ag.earn(ag.getMyProducer().getLastProductionCost()); //no expense during the initial production
            }
        }
    }
    /**
     * One step of evolution for the market
     * @param i - the number of the current step
     * @throws MarketFailingException
     */
    public void tick(int i) throws MarketFailingException
    {
    	computeAveragePrices();
    	//first reinitialize agent's seller and buyer
    	for(int j=0;j<this.theAgents.length;j++){
    		this.theAgents[j].reinit();
        }
        // At every tick one random agent produce and one transaction (selling/buying and consuming what has been bought) takes place
    	Boolean done = false;
        int numberOfAttempts = 0;
        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
        {
        	//1)chose one agent randomly
            int random = World.ran.nextInt(theAgents.length);
            //2)Production
            done = theAgents[random].actProduce();
            if (done)
            {
            	//get the product produced
            	lastProduction = theAgents[random].getMyProducer().getLastProduction();
            	//increase production of product produced
                productions[lastProduction]++;
                break;
            }
            numberOfAttempts++;
        }
        if(!done){
        	System.out.println("nop prod");
        }
        //3)Transaction - competitive or random
        Transaction t = createTransaction();
        if ((t == null) || (!done))
        {
            numberOfMarketFailures++;
            if (numberOfMarketFailures > 1000)
            {
                throw new MarketFailingException(i);
            }
        }
        else
        {
            t.executeTransaction(i);
            lastBuyer = t.getBuyer();
            lastSeller = t.getSeller();
            lastPrice = t.getPrice();
            lastProductSold = t.getTheProduct();
            updateLastTransactions(t);

        }
    }
    /**
     * Get the average price of each product over the last transactions to consider
     * @return - an array of price (1 per product)
     */
    public double[] getAvgPrices(){
    	if(avgPrices == null){
    		computeAveragePrices();
    	}
        return avgPrices;
    }
    
    private void computeAveragePrices(){
    	if(avgPrices == null){
    		avgPrices = new double[numberOfProducts];
    	}
    	computeAveragePrices(avgPrices, numberOfLastTransactions);
    }
    
    private void computeAveragePrices(double[] avgPrices, int numberOfTransactions){
    	int[] occurences = new int[numberOfProducts];
    	for(int i = 0 ; i < numberOfProducts ; ++i){
    		avgPrices[i] = 0;
    		occurences[i] = 0;
    	}
    	int numberConsidered = 0;
    	for(int i = allTransactions.size() - 1 ; i >= 0 && numberConsidered <= numberOfTransactions ; --i){
    		Transaction t = allTransactions.get(i);
    		int product = t.getTheProduct();
    		avgPrices[product] += t.getPrice();
    		occurences[product]++;
    		++numberConsidered;
    	}
    	
    	for(int i = 0 ; i < avgPrices.length ; ++i){
    		if(occurences[i] != 0)
    			avgPrices[i] /= occurences[i];
    	}
    }
    /**
     * Return the last buyer 
     * @return index of the last buyer
     */
    public int getLastBuyer()
    {
        int index = 1000;
        for (int i = 0; i < this.theAgents.length; i++)
        {
            if (theAgents[i] == lastBuyer)
            {
                index = i;
                break;
            }
        }
        return index;
    }
    /**
     * Return the last seller 
     * @return index of the last seller
     */
    public int getLastSeller()
    {
        int index = 1000;
        for (int i = 0; i < this.theAgents.length; i++)
        {
            if (theAgents[i] == lastSeller)
            {
                index = i;
                break;
            }
        }
        return index;
    }
    /*
     * Getter/Setter
     */
    /**
	 * Return the number of market failures
	 * @return numberOfMarketFailures
	 */
    public int getNumberOfMarketFailures()
    {
        return numberOfMarketFailures;
    }
    /**
     * Return the number of transactions
     * @return numberOfTransactions
     */
    public int getNumberOfTransactions()
    {
        return numberOfTransactions;
    }
    /**
     * Return the last production
     * @return lastProduction
     */
    public int getLastProduction()
    {
        return lastProduction;
    }
    /**
     * @return lastPrice
     */
    public double getLastPrice()
    {
        return lastPrice;
    }
    /**
     * @return lastProductSold
     */
    public int getLastProductSold()
    {
        return lastProductSold;
    }

	/**
	 * @return the theAgents
	 */
	public Agent[] getTheAgents() {
		return theAgents;
	}

	/**
	 * @param theAgents the theAgents to set
	 */
	public void setTheAgents(Agent[] theAgents) {
		this.theAgents = theAgents;
	}


	/**
	 * @return the productPrice
	 */
	public double[] getProductPrice() {
		return productPrice;
	}

	/**
	 * @param productPrice the productPrice to set
	 */
	public void setProductPrice(double[] productPrice) {
		this.productPrice = productPrice;
	}

	/**
	 * @return the numberOfLastTransactions
	 */
	public int getNumberOfLastTransactions() {
		return numberOfLastTransactions;
	}

	/**
	 * @param numberOfLastTransactions the numberOfLastTransactions to set
	 */
	public void setNumberOfLastTransactions(int numberOfLastTransactions) {
		this.numberOfLastTransactions = numberOfLastTransactions;
	}

	/**
	 * @return the numberOfProducts
	 */
	public int getNumberOfProducts() {
		return numberOfProducts;
	}

	/**
	 * @param numberOfProducts the numberOfProducts to set
	 */
	public void setNumberOfProducts(int numberOfProducts) {
		this.numberOfProducts = numberOfProducts;
	}

	/**
	 * @return the initialProduction
	 */
	public int getInitialProduction() {
		return initialProduction;
	}

	/**
	 * @param initialProduction the initialProduction to set
	 */
	public void setInitialProduction(int initialProduction) {
		this.initialProduction = initialProduction;
	}

	/**
	 * @return the numberOfAgents
	 */
	public int getNumberOfAgents() {
		return numberOfAgents;
	}

	/**
	 * @param numberOfAgents the numberOfAgents to set
	 */
	public void setNumberOfAgents(int numberOfAgents) {
		this.numberOfAgents = numberOfAgents;
	}

	/**
	 * @return the maxBenefit
	 */
	public double getMaxBenefit() {
		return maxBenefit;
	}

	/**
	 * @param maxBenefit the maxBenefit to set
	 */
	public void setMaxBenefit(double maxBenefit) {
		this.maxBenefit = maxBenefit;
	}

	/**
	 * @return the productions
	 */
	public int[] getProductions() {
		return productions;
	}

	/**
	 * @param productions the productions to set
	 */
	public void setProductions(int[] productions) {
		this.productions = productions;
	}

	/**
	 * @return the productsSold
	 */
	public int[] getProductsSold() {
		return productsSold;
	}

	/**
	 * @param productsSold the productsSold to set
	 */
	public void setProductsSold(int[] productsSold) {
		this.productsSold = productsSold;
	}

	/**
	 * @return the randomness
	 */
	public double getRandomness() {
		return randomness;
	}

	/**
	 * @param randomness the randomness to set
	 */
	public void setRandomness(double randomness) {
		this.randomness = randomness;
	}

	/**
	 * @param numberOfMarketFailures the numberOfMarketFailures to set
	 */
	public void setNumberOfMarketFailures(int numberOfMarketFailures) {
		this.numberOfMarketFailures = numberOfMarketFailures;
	}

	/**
	 * @param numberOfTransactions the numberOfTransactions to set
	 */
	public void setNumberOfTransactions(int numberOfTransactions) {
		this.numberOfTransactions = numberOfTransactions;
	}

	/**
	 * @param lastBuyer the lastBuyer to set
	 */
	public void setLastBuyer(Agent lastBuyer) {
		this.lastBuyer = lastBuyer;
	}

	/**
	 * @param lastSeller the lastSeller to set
	 */
	public void setLastSeller(Agent lastSeller) {
		this.lastSeller = lastSeller;
	}

	/**
	 * @param lastProductSold the lastProductSold to set
	 */
	public void setLastProductSold(int lastProductSold) {
		this.lastProductSold = lastProductSold;
	}

	/**
	 * @param lastPrice the lastPrice to set
	 */
	public void setLastPrice(double lastPrice) {
		this.lastPrice = lastPrice;
	}

	/**
	 * @param lastProduction the lastProduction to set
	 */
	public void setLastProduction(int lastProduction) {
		this.lastProduction = lastProduction;
	}

	/**
	 * @return the timeIndex
	 */
	public double getTimeIndex() {
		return timeIndex;
	}

	/**
	 * @param timeIndex the timeIndex to set
	 */
	public void setTimeIndex(double timeIndex) {
		this.timeIndex = timeIndex;
	}
	public ArrayList<Transaction> getAllTransactions() {
		return allTransactions;
	}
}
