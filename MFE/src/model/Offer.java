package model;

/**
 * The direction of the offers
 * @author nicolasbernier
 *
 */
enum Direction
{
    sell,
    buy
} ;

/**
 * The Offer of the model (buying or selling offer)
 * @author nicolasbernier
 *
 */
public class Offer
{
    private int theProduct;
    private Agent theAgent;
    private double price;
    private Direction dir;
    /**
     * Constructor
     * @param theProduct - the product of the offer
     * @param theAgent - the agent proposing the offer
     * @param price - the price of the proposition
     * @param dir - the direction of the offer (sell or buy)
     */
    public Offer(int theProduct, Agent theAgent, double price, Direction dir)
    {
        this.theProduct = theProduct;
        this.theAgent = theAgent;
        this.price = price;
        this.dir = dir;
    }

	/**
	 * @return the theProduct
	 */
	public int getTheProduct() {
		return theProduct;
	}

	/**
	 * @param theProduct the theProduct to set
	 */
	public void setTheProduct(int theProduct) {
		this.theProduct = theProduct;
	}

	/**
	 * @return the theAgent
	 */
	public Agent getTheAgent() {
		return theAgent;
	}

	/**
	 * @param theAgent the theAgent to set
	 */
	public void setTheAgent(Agent theAgent) {
		this.theAgent = theAgent;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the dir
	 */
	public Direction getDir() {
		return dir;
	}

	/**
	 * @param dir the dir to set
	 */
	public void setDir(Direction dir) {
		this.dir = dir;
	}

    

}