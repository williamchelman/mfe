package model;

import helper.Helper;

/**
 * The competitive seller part of the agent
 * @author nicolasbernier
 *
 */
public class CompetitiveSeller extends Seller
{
	/**
     * Constructor of the competitive seller
     * @param myAgent - the parent agent
     */
    public CompetitiveSeller(Agent myAgent){
        super(myAgent);
    }
	@SuppressWarnings("deprecation")
	@Override
	public Offer sell() {
		Offer o1 = null;
        int product = selectProductToSell();
        myAgent.incrementSellingChances();
        if (product != -1)
        {
            double price = 0.0;
            Offer o2=null;
        	Market m=myAgent.getTheMarket();
        	
        	if(m instanceof CompetitiveMarket && m!=null && ((CompetitiveMarket)m).getBestBuyingOffers()!=null){
        		o2=((CompetitiveMarket)m).getBestSellingOffers()[product];
        	}
        	else if(m instanceof MixedMarket && m!=null && ((MixedMarket)m).getBestBuyingOffers()!=null){
        		o2=((MixedMarket)m).getBestSellingOffers()[product];
        	}
        	
            double reservPrice = getReservePrice(product);
            if ((o2 != null))
            {
                if (reservPrice <= o2.getPrice())
                {
                    price = reservPrice + (World.ran.nextDouble() * (o2.getPrice() - reservPrice));
                    o1 = new Offer(product, myAgent, price, Direction.sell);
                    if(m instanceof CompetitiveMarket){
                    	((CompetitiveMarket)m).getBestSellingOffers()[product] = o1;
                	}
                	else if(m instanceof MixedMarket){
                		((MixedMarket)m).getBestSellingOffers()[product] = o1;
                	}
                }
            }
            else
            {
                o1 = new Offer(product, myAgent, reservPrice, Direction.sell);
                if(m instanceof CompetitiveMarket){
                	((CompetitiveMarket)m).getBestSellingOffers()[product] = o1;
            	}
            	else if(m instanceof MixedMarket){
            		((MixedMarket)m).getBestSellingOffers()[product] = o1;
            	}
                
            }
        }
        return o1;
	}
	@SuppressWarnings("deprecation")
	@Override
	public int selectProductToSell() // RJ version
    {
        double minCost = 100;
        int bestProduct = -1;
        //If there is still products
        if (myAgent.getMyProducer().getNumberOfProducts() > 0)
        {
            for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
            {
            	if(!Helper.isInside(this.productProposed, i)){
	            	Offer bO=null;
	            	Market m=myAgent.getTheMarket();
	            	//Get best selling offer for product i
	            	if(m instanceof CompetitiveMarket && m!=null && ((CompetitiveMarket)m).getBestSellingOffers()!=null){
	            		 bO=((CompetitiveMarket)m).getBestSellingOffers()[i];
	            	}
	            	else if(m instanceof MixedMarket && m!=null && ((MixedMarket)m).getBestSellingOffers()!=null){
	            		bO=((MixedMarket)m).getBestSellingOffers()[i];
	            	}
	            	
	                double reservPrice = getReservePrice(i);
	                if (myAgent.getMyProducer().getProducts()[i] > 0)
	                {
	                    if (bO != null)
	                    {
	                        if (reservPrice <= bO.getPrice())
	                        {
	                            if (myAgent.getMyProducer().getMySkills()[i] < minCost)
	                            {
	                                minCost = myAgent.getMyProducer().getMySkills()[i];
	                                bestProduct = i;
	                            }
	                        }
	                    }
	                    else
	                    {
	                        if (myAgent.getMyProducer().getMySkills()[i] < minCost)
	                        {
	                            minCost = myAgent.getMyProducer().getMySkills()[i];
	                            bestProduct = i;
	                        }
	                    }
	                }
            	}
            }
        }
        if(bestProduct!=-1){
	        this.productProposedAttemps[bestProduct]++;
	        if(this.productProposedAttemps[bestProduct]>=World.NUM_REPETITION_PRODUCT_SELLER_BUYER){
	        	this.productProposed.add((Integer)bestProduct);
	        }
        }
        return bestProduct;
    }
	
	private double getReservePrice(int productType){
		return myAgent.getMyProducer().getProductionCost(productType);
	}
}
