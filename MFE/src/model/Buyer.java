package model;

import java.util.ArrayList;

/**
 * The buyer part of the agent
 * @author nicolasbernier
 *
 */
public abstract class Buyer
{
    protected Agent myAgent;
    protected ArrayList<Integer> productProposed=new ArrayList<Integer>();
    protected int[] productProposedAttemps;

    /**
     * Constructor of the buyer
     * @param myAgent - the parent agent
     */
    public Buyer(Agent myAgent)
    {
        this.myAgent = myAgent;
        this.productProposedAttemps=new int[this.myAgent.getNumberOfProducts()];
        for(int i=0;i<this.productProposedAttemps.length;i++){
        	this.productProposedAttemps[i]=0;
        }
    }
    /**
     * Reinitialize the buyer before a transaction
     */
    public void reinit(){
    	this.productProposed.clear();
    	for(int i=0;i<this.productProposedAttemps.length;i++){
        	this.productProposedAttemps[i]=0;
        }
    }
    /**
     * Create a buying offer
     * @param sellingOffer - the selling offer on which is based the buying offer
     * @return
     */
    public abstract Offer buy(Offer sellingOffer);
    
    /**
     * Select a product to buy (only use by the competitive buyer)
     * @return the product
     */
    public abstract int selectProductToBuy(); // RJ version
	/**
	 * @return the productProposed
	 */
	public ArrayList<Integer> getProductProposed() {
		return productProposed;
	}
	/**
	 * @return the productProposedAttemps
	 */
	public int[] getProductProposedAttemps() {
		return productProposedAttemps;
	}
}