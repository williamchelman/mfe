package model;

//import java.io.*;

/**
 * Class used to run the simulation when the GUI did not exist 
 * @author Raphael Jehotte
 *
 */
@Deprecated
public class Program
{
	/*
    private double[][] indicators = new double[4][];
    private double PartOfRandomInRandomProdCase;
    private int nbSimulationsDesired;
    private String filepath;
    private int runs = 0, aborted = 0, aborted_rr = 0, aborted_ri = 0, aborted_cr = 0, aborted_ci = 0;
    private File desc_fs, global_fs, agents_fs, agentsfU_fs, products_fs, fs1, fs2, fs3, fs4;
    private PrintWriter desc_output, global_output, agents_output, agentsfU_output, products_output, outputRR, outputRI, outputCR, outputCI;
/*

    public Program(double PartOfRandomInRandomProdCase, int nbSimulationsDesired, String filepath, double initialMoney, int numberOfLastTransactions, double timeIndex) throws FileNotFoundException
    {
    	
    }
        for (int i = 0; i < indicators.length; i++){
            indicators[i] = new double[14];
        }
        this.PartOfRandomInRandomProdCase = PartOfRandomInRandomProdCase;
        this.nbSimulationsDesired = nbSimulationsDesired;
        this.filepath = filepath;
        World.INITIALMONEY = initialMoney;
        World.TIMEINDEX = (double)timeIndex / (double)initialMoney;
        World.NUMBEROFLASTTRANSACTIONS = numberOfLastTransactions;
        desc_fs = new File(filepath + "SimulationDescription.txt");
        desc_fs.getParentFile().mkdirs();
        desc_output = new PrintWriter(desc_fs);
        this.SimulationDescriptor();
        global_fs = new File(filepath + "GlobalStats.csv");
        global_fs.getParentFile().mkdirs();

        global_output = new PrintWriter(global_fs);
        global_output.println("Run;RR_U;RI_U;CR_U;CI_U;RR_M;RI_M;CR_M;CI_M;RR_AV;RI_AV;CR_AV;CI_AV;RR_W;RI_W;CR_W;CI_W;RR_P;RI_P;CR_P;CI_P;RR_PV;RI_PV;CR_PV;CI_PV;RR_B;RI_B;CR_B;CI_B;RR_TB;RI_TB;CR_TB;CI_TB;RR_GU;RI_GU;CR_GU;CI_GU;RR_GM;RI_GM;CR_GM;CI_GM;RR_GAV;RI_GAV;CR_GAV;CI_GAV;RR_GW;RI_GW;CR_GW;CI_GW;RR_MF;RI_MF;CR_MF;CI_MF");

    }
    public void SimulationDescriptor()
    {
        desc_output.println("SIMULATIONS SET DESCRIPTION");

        desc_output.println("INITIAL MONEY: " + World.INITIALMONEY);
        desc_output.println("INITIAL UTILITY: " + World.INITIALUTILITY);
        desc_output.println("INITIAL PRODUCTION: " + World.INITIALPRODUCTION);
        desc_output.println("NB OF PRODUCTS: " + World.NUMBEROFPRODUCTS);
        desc_output.println("NB OF AGENTS: " + World.NUMBEROFAGENTS);
        desc_output.println("NB OF TICKS: " + World.NUMBEROFSIMULATIONSTEPS);
        desc_output.println("NB OF LAST TRANSACTIONS/INFO: " + World.NUMBEROFLASTTRANSACTIONS);
        desc_output.println("MAX NB OF ATTEMPTS: " + World.MAXNUMBEROFATTEMPTS);
        desc_output.println("MAX BENEFIT: " + World.MAXBENEFIT);
        desc_output.println("INCLUDES STOCKS IN WEALTH: " + World.INCLUDESTOCKSINWEALTH);
        desc_output.println("SHARE OF RANDOM IN RANDOM PRODUCTION MODEL: " + PartOfRandomInRandomProdCase);
        desc_output.println("NB OF SIMULATIONS (IN GROUPS OF 4): " + nbSimulationsDesired);
        desc_output.println("\nMEANING OF ACRONYMS IN CSV FILES");
        desc_output.println("\tRR_: Model with random market and random production");
        desc_output.println("\tRI_: Model with random market and informed production");
        desc_output.println("\tCR_: Model with competitive market and random production");
        desc_output.println("\tCI_: Model with competitive market and informed production");
        desc_output.println("\t_U: Total Utility");
        desc_output.println("\t_M: Total Money");
        desc_output.println("\t_AV: Total Added Value");
        desc_output.println("\t_W: Total Wealth");
        desc_output.println("\t_PiS: Total Number of Products in Stocks");
        desc_output.println("\t_VoPis: Total Value of Products in Stocks (at last market prices)");
        desc_output.println("\t_B: Total Number of Agents With No Money Left");
        desc_output.println("\t_TB: Total Number of Agents With No Money and No Products Left");
        desc_output.println("\t_GU: Gini Index of Total Utility");
        desc_output.println("\t_GM: Gini Index of Total Money");
        desc_output.println("\t_GM: Gini Index of Total Added Value");
        desc_output.println("\t_GW: Gini Index of Total Wealth");
        desc_output.println("\t_MF: Total Number of Market Failures");
        desc_output.println("\t_P : Nature of Production");
        desc_output.println("\t_TBuy : Buyer of last Transaction");
        desc_output.println("\t_TSell : Seller of last Transaction");
        desc_output.println("\t_TPri : Price of last Trasaction");
        desc_output.println("\t_TPro : Product of last Transaction");
    }
    public void CloseOutputFiles()
    {
        global_output.close();
        desc_output.println("Number of runs overall: " + runs);
        desc_output.println("Aborted runs overall: " + aborted + " (" + (int)Math.round((double)(100 * aborted / runs)) + "%) | RR: " + aborted_rr + " - RI: " + aborted_ri + " - CR: " + aborted_cr + " - CI: " + aborted_ci);
        desc_output.close();

    }
    public void StartSimulation() throws FileNotFoundException
    {
        for (runs = 1; runs <= nbSimulationsDesired; runs++)
        {
            System.out.println("\n***** SIMULATION #" + runs + " *****");
            Boolean thisRunIsAborted = false;
            World w = new World();
			// FIRST TRY RANDOM MARKET WITH RANDOM PRODUCTION
            w.createAgents();
            System.out.println("1. RANDOM MARKET - RANDOM PRODUCTION");
            fs1 = new File(filepath + "Stats_RR_" + runs + ".csv");
            fs1.getParentFile().mkdirs();

			outputRR = new PrintWriter(fs1);
			outputRR.println("Tick;U;M;AV;W;PiS;VoPiS;B;TB;GU;GM;GAV;GW;MF;P;TBuy;TSell;TPri;TPro");
			World.PARTOFRANDOMPRODUCTION = PartOfRandomInRandomProdCase;
			
			w.createRandomMarket(); // CREATERANDOMMARKET OR
									// CREATECOMPETITIVEMARKET
			try {
				w.run(outputRR); // TRUE IF RANDOM, FALSE ELSEWHERE
				this.logIndicators(0, w);
			} catch (Exception e) {
				System.out.println("Simulation RR #" + runs + " aborted");
				String s = e.getMessage();
				System.out.println(s);
				thisRunIsAborted = true;
				aborted_rr++;
				System.out.println("% AGENTS WITH NO MONEY: " + (int) 100
						* R2(indicators[0][6]));
				System.out.println("% AGENTS WITH NO MONEY OR STOCK: "
						+ (int) 100 * R2(indicators[0][7]));
			}
			this.outputAgents(w, runs, "RR");
			this.outputUtilityFunctions(w, runs, "RR");
			this.outputProducts(w, runs, "RR");
			outputRR.close();
			System.out.println("Global money of agents at beginning: "
					+ World.NUMBEROFAGENTS * World.INITIALMONEY);
//*/
			// NOW TRY RANDOM MARKET WITH INFORMED PRODUCTION
			/*
			 * System.out.println("\n2. RANDOM MARKET - INFORMED PRODUCTION");
			 * fs2 = new File(filepath + "Stats_RI_" + runs + ".csv"); outputRI
			 * = new PrintWriter(fs2); outputRI.println(
			 * "Tick;U;M;AV;W;PiS;VoPiS;B;TB;GU;GM;GAV;GW;MF;P;TBuy;TSell;TPri;TPro"
			 * ); w.reset(true); World.PARTOFRANDOMPRODUCTION =
			 * PartOfRandomInRandomProdCase; // NVZ: Pure informed production
			 * w.createRandomMarket(); // CREATERANDOMMARKET OR
			 * CREATECOMPETITIVEMARKET try { w.run(outputRI); // TRUE IF RANDOM,
			 * FALSE ELSEWHERE this.logIndicators(1, w); } catch (Exception e) {
			 * System.out.println("Simulation RI #" + runs + " aborted"); String
			 * s = e.Message; thisRunIsAborted = true; aborted_ri++;
			 * System.out.println("% AGENTS WITH NO MONEY: " + (int)100 *
			 * R2(indicators[1][6]));
			 * System.out.println("% AGENTS WITH NO MONEY OR STOCK: " + (int)100
			 * * R2(indicators[1][7])); } this.outputAgents(w, runs, "RI");
			 * this.outputUtilityFunctions(w, runs, "RI");
			 * this.outputProducts(w, runs, "RI"); outputRI.Close();
			 * agents_output.Close(); agentsfU_output.Close();
			 * products_output.Close();
			 * System.out.println("Global money of agents at beginning: " +
			 * World.NUMBEROFAGENTS * World.INITIALMONEY);
			 */

			// NOW TRY COMPETITIVE MARKET WITH RANDOM PRODUCTION
			/*
			 * System.out.println("\n3. COMPETITIVE MARKET - RANDOM PRODUCTION");
			 * fs3 = new File(filepath + "Stats_CR_" + runs + ".csv"); outputCR
			 * = new PrintWriter(fs3); outputCR.println(
			 * "Tick;U;M;AV;W;PiS;VoPiS;B;TB;GU;GM;GAV;GW;MF;P;TBuy;TSell;TPri;TPro"
			 * ); w.reset(false); World.PARTOFRANDOMPRODUCTION =
			 * PartOfRandomInRandomProdCase; w.createCompetitiveMarket(); //
			 * CREATERANDOMMARKET OR CREATECOMPETITIVEMARKET try {
			 * w.run(outputCR); // TRUE IF RANDOM, FALSE ELSEWHERE
			 * this.logIndicators(2, w); } catch (Exception e) {
			 * System.out.println("Simulation CR #" + runs + " aborted"); String
			 * s = e.Message; thisRunIsAborted = true; aborted_cr++;
			 * System.out.println("% AGENTS WITH NO MONEY: " + (int)100 *
			 * R2(indicators[2][6]));
			 * System.out.println("% AGENTS WITH NO MONEY OR STOCK: " + (int)100
			 * * R2(indicators[2][7])); } this.outputAgents(w, runs, "CR");
			 * this.outputUtilityFunctions(w, runs, "CR");
			 * this.outputProducts(w, runs, "CR"); outputCR.Close();
			 * System.out.println("Global money of agents at beginning: " +
			 * World.NUMBEROFAGENTS * World.INITIALMONEY);
			 */
			/*
            // FINALLY TRY COMPETITIVE MARKET WITH INFORMED PRODUCTION
            System.out.println("\n4. COMPETITIVE MARKET - INFORMED PRODUCTION");
            fs4 = new File(filepath + "Stats_CI_" + runs + ".csv");
            fs4.getParentFile().mkdirs();

            outputCI = new PrintWriter(fs4);
            outputCI.println("Tick;U;M;AV;W;PiS;VoPiS;B;TB;GU;GM;GAV;GW;MF;P;TBuy;TSell;TPri;TPro");
            w.reset();
            World.PARTOFRANDOMPRODUCTION = PartOfRandomInRandomProdCase; // NVZ: Pure informed production
            
            w.createCompetitiveMarket();
            
            try
            {
                w.run(outputCI);
                this.logIndicators(3, w);
            }
            catch (Exception e)
            {
                System.out.println("Simulation CI #" + runs + " aborted");
                //String s = e.getMessage();
                thisRunIsAborted = true;
                aborted_ci++;
                System.out.println("% AGENTS WITH NO MONEY: " + (int)100 * R2(indicators[3][6]));
                System.out.println("% AGENTS WITH NO MONEY OR STOCK: " + (int)100 * R2(indicators[3][7]));
            }
            this.outputAgents(w, runs, "CI");
            this.outputUtilityFunctions(w, runs, "CI");
            this.outputProducts(w, runs, "CI");
            outputCI.close();
            agents_output.close();
            agentsfU_output.close();
            products_output.close();

            System.out.println("Global money of agents at beginning: " + World.NUMBEROFAGENTS * World.INITIALMONEY);
            System.out.println("Production based on # transactions: " + World.NUMBEROFLASTTRANSACTIONS);

            // STATS
            if (thisRunIsAborted)
                aborted++;
            else
            {
                this.outputIndicators();
            }
            System.out.println("\nNumber of runs so far: " + runs);
            System.out.println("Aborted runs so far: " + aborted + " (" + (int)Math.round((double)(100 * aborted / runs)) + "%) | RR: " + aborted_rr + " - RI: " + aborted_ri + " - CR: " + aborted_cr + " - CI: " + aborted_ci);
        }
        runs--;
        System.out.println("Number of runs overall: " + runs);
        System.out.println("Aborted runs overall: " + aborted + " (" + (int)Math.round((double)(100 * aborted / runs)) + "%) | RR: " + aborted_rr + " - RI: " + aborted_ri + " - CR: " + aborted_cr + " - CI: " + aborted_ci);
        this.CloseOutputFiles();
    }
    public void logIndicators(int row, World w)
    {
        if (row < indicators.length)
        {
            indicators[row][0] = w.computeGlobalUtility();
            indicators[row][1] = w.computeGlobalMoney();
            indicators[row][2] = w.computeGlobalAddedValue();
            indicators[row][3] = w.computeGlobalWealth();
            indicators[row][4] = w.nbProductsInStocks();
            indicators[row][5] = w.computeValueOfProductsInStocks();
            indicators[row][6] = w.shareAgentsWithNoMoney();
            indicators[row][7] = w.shareAgentsWithNoMoneyOrProduct();
            indicators[row][8] = w.computeGiniUtility();
            indicators[row][9] = w.computeGiniMoney();
            indicators[row][10] = w.computeGiniAddedValue();
            indicators[row][11] = w.computeGiniWealth();
            indicators[row][12] = w.getNumberOfMarketFailures();
            indicators[row][13] = w.getNumberOfTransactions();
            System.out.println("TOTAL UTILITY: " + indicators[row][0]);
            System.out.println("TOTAL MONEY: " + indicators[row][1]);
            System.out.println("TOTAL ADDED VALUE: " + indicators[row][2]);
            System.out.println("TOTAL WEALTH: " + indicators[row][3]);
            System.out.println("NB PRODUCTS IN STOCK: " + indicators[row][4]);
            System.out.println("VALUE PRODUCTS IN STOCK: " + indicators[row][5]);
            System.out.println("% AGENTS WITH NO MONEY: " + (int)100 * R2(indicators[row][6]));
            System.out.println("% AGENTS WITH NO MONEY OR STOCK: " + (int)100 * R2(indicators[row][7]));
            System.out.println("UTILITY GINI: " + indicators[row][8]);
            System.out.println("MONEY GINI: " + indicators[row][9]);
            System.out.println("ADDED VALUE GINI: " + indicators[row][10]);
            System.out.println("WEALTH GINI: " + indicators[row][11]);
            System.out.println("NUMBER OF MARKET FAILURES: " + indicators[row][12]);
            System.out.println("NUMBER OF TRANSACTIONS :" + indicators[row][13]);
        }
        else
            System.out.println("Sorry, logging of indicators impossible due to excess number of models in set");
    }
    public void outputIndicators()
    {
        String line = runs + ";";
        for (int j = 0; j < 14; j++)
            for (int i = 0; i < indicators.length; i++)
            {
                line += indicators[i][j] + ";";
            }
        global_output.println(line);
    }

    public void outputAgents(World w, int run, String model) throws FileNotFoundException
    {
        agents_fs = new File(filepath + "Agents_" + model + "_" + run + ".csv");
        agents_fs.getParentFile().mkdirs();

        agents_output = new PrintWriter(agents_fs);
        agents_output.println("Agent;Utility;Money;Added Value;Products in Stock;Products Sold;Products Bought; Frustration; Buying Chances; Selling Chances ; Gini Tastes");
        String line;
        int i = 0;
        Agent[] agentCopy = w.getAgentsCopy();
        agentCopy = w.getAgentsCopy();
        for (Agent ag : agentCopy)
        {
            line = i + ";" + ag.getUtility() + ";" + ag.getMoney() + ";" + ag.getAddedValue() + ";" + ag.getMyProducer().getNumberOfProducts() + ";" + ag.getNumberOfProductsSold() + ";" + ag.getNumberOfProductsBought() + ";" + ag.getFrustration() + ";" + ag.getBuyingChances() + ";" + ag.getSellingChances() + ";" + ag.getMyConsumer().computeGiniTastes() + ";";
            agents_output.println(line);
            i++;
        }
    }

    public void outputUtilityFunctions(World w, int run, String model) throws FileNotFoundException
    {
        agentsfU_fs = new File(filepath + "Agents_fU" + model + "_" + run + ".csv");
        agents_fs.getParentFile().mkdirs();

        agentsfU_output = new PrintWriter(agentsfU_fs);
        agentsfU_output.println("Agent;Product;Utilities");
        String line;
        int i = 0;
        Agent[] agentCopy = new Agent[World.NUMBEROFAGENTS];
        agentCopy = w.getAgentsCopy();
        for (Agent ag :  agentCopy)
        {
            for (int j = 0; j < World.NUMBEROFPRODUCTS; j++)
            {
                line = i + ";" + j + ";";
                for (int k = 0; k < ag.getNumberOfProductsBought(); k++)
                {
                    line += ag.getMyConsumer().getMyUtilityFunction()[j][k] + ";";
                }
                agentsfU_output.println(line);
            }
            String blank = " ";
            agentsfU_output.println(blank);
            i++;
        }
    }

    public void outputProducts(World w, int run, String model) throws FileNotFoundException
    {
        products_fs = new File(filepath + "Products" + model + "_" + run + ".csv");
        products_fs.getParentFile().mkdirs();

        products_output = new PrintWriter(products_fs);
        products_output.println(";p0;p1;p2;p3;p4;p5;p6;p7;p8;p9");
        String line = "Productions;";
        int[] productions = w.getProductions();
        for (int i = 0; i < World.NUMBEROFPRODUCTS; i++)
        {
            line += productions[i] + ";";
        }
        products_output.println(line);
        String line2 = "Products sold;";
        int[] productsSold = w.getProductsSold();
        for (int i = 0; i < World.NUMBEROFPRODUCTS; i++)
        {
            line2 += productsSold[i] + ";";
        }
        products_output.println(line2);
    }

    public static double R2(double x)
    {
        double y = x * 100;
        int z = (int)Math.round(y);
        double r = z / 100.0;
        return r;
    }
    
	public static void main(String[] args) throws FileNotFoundException {
        Program sm;

        sm = new Program(0, 1, "Projects/CompetitiveMarket/1/",100, 1000, 1.0);
        sm.StartSimulation();

      //  sm = new Program(0, 1, "Projects/CompetitiveMarket/2/",80, 1000, 1.0);
      //  sm.StartSimulation();
	}
	*/
}
