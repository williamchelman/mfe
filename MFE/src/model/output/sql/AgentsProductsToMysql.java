package model.output.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import model.Agent;

public class AgentsProductsToMysql {
	public static final String world_id = "world_id";
	public static final String Agent_index = "agent_index";
	public static final String Product_index = "product_index";
	public static final String Taste = "taste";
	public static final String Skill = "skill";
	public static final String sold = "sold";
	public static final String bought = "bought";
	public static final String in_stock = "in_stock";
	private final String tableName = "agents_products";
	
	private Connection connection;
	
	public AgentsProductsToMysql(Connection connection) throws SQLException{
		this.connection = connection;
		init();
	}
	
	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+world_id+" INT UNSIGNED"
			+","+Agent_index+" INT UNSIGNED"
			+","+Product_index+" INT UNSIGNED"
			+","+Taste+" DOUBLE"
			+","+Skill+" DOUBLE"
			+","+sold+" INT"
			+","+bought+" INT"
			+","+in_stock+" INT"
			+", PRIMARY KEY("+world_id+","+Agent_index+","+Product_index+")"
			+", FOREIGN KEY("+world_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		state.execute(query);
	}
	
	public void save(Agent[] agents, int worldId) throws SQLException{
		String query = "INSERT INTO "+getTableName()+"("
				+world_id
				+","+Agent_index
				+","+Product_index
				+","+Taste
				+","+Skill
				+","+sold
				+","+bought
				+","+in_stock
			    + ")";
		query += " VALUES ";
		boolean first = true;
		for(Agent a : agents){
			int agent_index = a.getIndex();
			double[] tastes = a.getMyConsumer().getMyTastes();
			double[] skills = a.getMyProducer().getMySkills();
			for(int product_index = 0 ; product_index < tastes.length ; ++product_index){
				if(first){
					first = false;
				} else {
					query += ",";
				}
				
				query+="(" 
						+worldId
						+","+agent_index
						+","+product_index
						+","+tastes[product_index]
						+","+skills[product_index]
						+","+a.getNumberOfProductsSold()[product_index]
						+","+a.getNumberOfProductsBought()[product_index]
						+","+a.getMyProducer().getProducts()[product_index]
						+")";
			}
		}
		Statement state = connection.createStatement();
		state.executeUpdate(query);
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
