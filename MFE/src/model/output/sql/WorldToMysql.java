package model.output.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.World;
import model.output.WorldOutput;

public class WorldToMysql implements WorldOutput {
	public static final String steps = "steps";
	public static final String intended_steps = "intended_steps";
	public static final String number_of_products = "number_of_products";
	public static final String number_of_agents = "number_of_agents";
	public static final String initial_agent_money = "initial_agent_money";
	public static final String initial_agent_utility = "initial_agent_utility";
	public static final String number_of_initial_production = "number_of_initial_production";
	public static final String maximum_possible_benefit = "maximum_possible_benefit";
	public static final String number_of_last_transactions_to_consider = "number_of_last_transactions_to_consider";
	public static final String part_of_random_production = "part_of_random_production";
	public static final String market_randomness = "market_randomness";
	public static final String production_number_for_specialization = "production_number_for_specialization";
	public static final String diminishing_marginal_utility = "diminishing_marginal_utility";
	public static final String influenceability = "influenceability";
	public static final String number_of_patents = "number_of_patents";
	public static final String number_of_lowcost = "number_of_lowcost";
	public static final String min_quality = "min_quality";
	
	public static final String Gini_added_value = "gini_added_value";
	public static final String Global_added_value = "global_added_value";
	public static final String Gini_money = "gini_money";
	public static final String Global_money = "global_money";
	public static final String Gini_utility = "gini_utility";
	public static final String Global_utility = "global_utility";
	public static final String Gini_wealth = "gini_wealth";
	public static final String Global_wealth = "global_wealth";
	public static final String Value_of_products_in_stock = "value_of_products_in_stock";
	public static final String Number_of_market_failure = "number_of_market_failure";
	public static final String Share_agent_with_no_money = "share_agent_with_no_money";
	public static final String Share_agent_with_no_money_and_no_products = "share_agent_with_no_money_and_no_products";
	
	private Connection connection;
	
	private AgentToMysql agentToMYSQL;
	private ProductsToMysql productsToMYSQL;
	private AgentsProductsToMysql agentsProductsToMYSQL;
	private TransactionToMysql transactionToMysql;
	private PatentsToMysql patentsToMysql;
	private LorentzToMysql lorentzToMysql;
	private HistoryToMysql historyToMysql;
	
	public final static String tableName = "worlds";
	public static String tableSuffix = "";
	
	public WorldToMysql(String url, String port, String db, String user, String psw, String tableSuffix,boolean saveHistory,boolean saveLorentz, boolean saveAgents,boolean saveProducts, boolean saveAgentsProducts, boolean saveTransactions, boolean savePatents) throws SQLException{
		WorldToMysql.tableSuffix = tableSuffix;
		connection = MYSQL.getConnection(url, port, db, user, psw);
		init();
		if(saveAgents)
			agentToMYSQL = new AgentToMysql(connection);
		if(saveTransactions)
			transactionToMysql = new TransactionToMysql(connection);
		if(saveAgentsProducts)
			agentsProductsToMYSQL = new AgentsProductsToMysql(connection);
		if(savePatents)
			patentsToMysql = new PatentsToMysql(connection);
		if(saveLorentz)
			lorentzToMysql = new LorentzToMysql(connection);
		if(saveHistory)
			historyToMysql = new HistoryToMysql(connection);
		if(saveProducts)
			productsToMYSQL = new ProductsToMysql(connection);
		
	}
	
	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+"id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY"
			+","+intended_steps+" INT UNSIGNED"
			+","+steps+" INT UNSIGNED"
			+","+number_of_products+" INT UNSIGNED"
			+","+number_of_agents+" INT UNSIGNED"
			+","+initial_agent_money+" DOUBLE"
			+","+initial_agent_utility+" DOUBLE"
			+","+number_of_initial_production+" INT UNSIGNED"
			+","+maximum_possible_benefit+" DOUBLE"
			+","+number_of_last_transactions_to_consider+" INT UNSIGNED"
			+","+part_of_random_production+" DOUBLE"
			+","+market_randomness+" DOUBLE"
			+","+production_number_for_specialization+" INT UNSIGNED" 
			+","+diminishing_marginal_utility+" BOOL"
			+","+influenceability+" DOUBLE"
			+","+number_of_patents+" INT UNSIGNED"
			+","+number_of_lowcost+" INT UNSIGNED"
			+","+min_quality+" DOUBLE UNSIGNED"
			
			+","+Gini_added_value+" DOUBLE"
			+","+Global_added_value+" DOUBLE" 
			+","+Gini_money+" DOUBLE"
			+","+Global_money+" DOUBLE"
			+","+Gini_utility+" DOUBLE"
			+","+Global_utility+" DOUBLE"
			+","+Gini_wealth+" DOUBLE"
			+","+Global_wealth+" DOUBLE"
			+","+Value_of_products_in_stock+" DOUBLE" 
			+","+Number_of_market_failure+" INT UNSIGNED"
			+","+Share_agent_with_no_money+" DOUBLE"
			+","+Share_agent_with_no_money_and_no_products+" DOUBLE"
		    + ")";
		state.execute(query);
	}
	
	public void saveWorld(World w,int maxStep) throws SQLException{
		World[] worlds = new World[1];
		worlds[0] = w;
		saveWorlds(worlds,maxStep);
	}
	
	
	private String getBaseInsert(){
		String query = "INSERT INTO "+getTableName()+"("
				+intended_steps+","
				+steps+","
				+number_of_products+","
				+number_of_agents+","
				+initial_agent_money+","
				+initial_agent_utility+","
				+number_of_initial_production+","
				+maximum_possible_benefit+","
				+number_of_last_transactions_to_consider+","
				+part_of_random_production+","
				+market_randomness+","
				+production_number_for_specialization+"," 
			    +diminishing_marginal_utility+","
			    +influenceability+","
			    +number_of_patents+","
    			+number_of_lowcost+","
    			+min_quality+","
			    +Gini_added_value+","
			    +Global_added_value+"," 
			    +Gini_money+","
			    +Global_money+","
			    +Gini_utility+","
			    +Global_utility+","
				+Gini_wealth+","
			    +Global_wealth+","
			    +Value_of_products_in_stock+"," 
			    +Number_of_market_failure+","
				+Share_agent_with_no_money+","
			    +Share_agent_with_no_money_and_no_products
			    + ")";
		query += " VALUES ";
		return query;
	}
	
	private String getValues(World w, int maxStep){
		String values = "";
		
		values += maxStep;
		values += "," + w.getCurrentStep();
		
		//Parameters
		values += "," + w.getNumberOfProducts();
		values += "," + w.getNumberOfAgents();
		values += "," + w.getInitialMoneyAgent();
		values += "," + w.getInitialUtilityAgent();
		values += "," + w.getInitialProduction();
		values += "," + w.getMaxBenefit();
		values += "," + w.getNumberOfLastTransactions();
		values += "," + w.getPartRandomProduction();
		values += "," + w.getPartRandomMarket();
		values += "," + w.getSpecializedProduction();
		values += "," + Boolean.toString(w.getDecreasingUtility());
		values += "," + w.getInfluenceability();
		values += "," + w.getNumberOfPatents();
		values += "," + w.getNumberOfLowcostAgents();
		values += "," + w.getMinQuality();
		
		//Measures
		values += "," + w.computeGiniAddedValue();
		values += "," + w.computeGlobalAddedValue();
		values += "," + w.computeGiniMoney();
		values += "," + w.computeGlobalMoney();
		values += "," + w.computeGiniUtility();
		values += "," + w.computeGlobalUtility();
		values += "," + w.computeGiniWealth();
		values += "," + w.computeGlobalWealth();
		values += "," + w.computeValueOfProductsInStocks();
		values += "," + w.getNumberOfMarketFailures();
		values += "," + w.shareAgentsWithNoMoney();
		values += "," + w.shareAgentsWithNoMoneyAndNoProduct();
		
		return values;
	}

	@Override
	public void close() throws SQLException {
		connection.close();
	}

	@Override
	public void saveWorlds(World[] worlds, int maxStep) throws SQLException {
		boolean allNull = true;
		String query = getBaseInsert();
		boolean first = true;
		for(World w : worlds){
			if(w != null)
			{
				allNull = false;
				if(first){
					first = false;
				} else {
					query += ",";
				}
				query += "("+getValues(w, maxStep)+")";
			}
		}
		if(!allNull){
			Statement s = connection.createStatement();
			s.execute(query,Statement.RETURN_GENERATED_KEYS);
			
			int[] worldsIds = new int[worlds.length];
			ResultSet ids = s.getGeneratedKeys();
			int index = 0;
			while(ids.next()){
				worldsIds[index] = ids.getInt(1);
				++index;
			}
			ids.close();
			saveOtherInformation(worlds,worldsIds);
		}
	}
	
	private void saveOtherInformation(World[] worlds, int[] ids) throws SQLException{
		for(int i = 0 ; i < worlds.length ; ++i){
			World w = worlds[i];
			int worldId = ids[i];
			if(agentToMYSQL != null){
				agentToMYSQL.saveAgents(w.getTheAgents(), worldId);
			}
			if(agentsProductsToMYSQL != null){
				agentsProductsToMYSQL.save(w.getTheAgents(), worldId);
			}
			if(transactionToMysql != null){
				transactionToMysql.save(w, worldId);
			}
			if(patentsToMysql != null){
				patentsToMysql.save(w.getPatentOffice(), worldId);
			}
			if(lorentzToMysql != null){
				lorentzToMysql.save(w, worldId);
			}
			if(historyToMysql != null){
				historyToMysql.save(w, worldId);
			}
			if(productsToMYSQL != null){
				productsToMYSQL.save(w, worldId);
			}
		}
	}
	
	public static String getTableName(){
		return tableSuffix+tableName;
	}
	
}
