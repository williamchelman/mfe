package model.output.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Transaction;
import model.World;

public class HistoryToMysql {
	public static final String tableName = "history";
	
	public static final String world_id ="world_id";
	public static final String step = "step";
	public static final String utility = "global_utility";
	public static final String money = "global_money";
	public static final String addedValue = "global_added_value";
	public static final String wealth = "global_wealth";
	public static final String productsInStock = "products_in_stock";
	public static final String valueProductsInStock = "value_products_in_stock";
	public static final String shareAgentsWithNoMoney = "share_agents_with_no_money";
	public static final String marketFailure = "market_failure";
	public static final String giniUtility = "gini_utility";
	public static final String giniAddedValue = "gini_added_value";
	public static final String giniMoney = "gini_money";
	public static final String giniWealth = "gini_wealth";
	
	private Connection connection;
	
	public HistoryToMysql(Connection connection) throws SQLException{
		this.connection = connection;
		init();
	}

	private void init() throws SQLException {
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+world_id+" INT UNSIGNED"
			+","+step+" INT UNSIGNED"
			+","+utility + " DOUBLE UNSIGNED"
			+","+money + " DOUBLE UNSIGNED"
			+","+addedValue + " DOUBLE UNSIGNED"
			+","+wealth + " DOUBLE UNSIGNED"
			+","+productsInStock + " DOUBLE UNSIGNED"
			+","+valueProductsInStock + " DOUBLE UNSIGNED"
			+","+shareAgentsWithNoMoney + " DOUBLE UNSIGNED"
			+","+marketFailure + " DOUBLE UNSIGNED"
			+","+giniUtility + " DOUBLE UNSIGNED"
			+","+giniAddedValue + " DOUBLE UNSIGNED"
			+","+giniMoney + " DOUBLE UNSIGNED"
			+","+giniWealth + " DOUBLE UNSIGNED"
			+", PRIMARY KEY("+world_id+","+step+")"
			+", FOREIGN KEY("+world_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		state.execute(query);
	}
	
	public void save(World w, int worldId) throws SQLException{
		Statement state = connection.createStatement();
		int size = w.getHistoryAddedValue().size();
		
		boolean first = true;
		String query = getBaseInsert();
		for(int step = 1 ; step <= size ; ++step){
			if(first){
				first = false;
			} else {
				query += ",";
			}
			
			query+= "("
					+worldId
					+","+step
					+","+getIndexOrLast(w.getHistoryUtility(),step-1)
					+","+getIndexOrLast(w.getHistoryMoney(),step-1)
					+","+getIndexOrLast(w.getHistoryAddedValue(),step-1)
					+","+getIndexOrLast(w.getHistoryWealth(),step-1)
					+","+getIndexOrLast(w.getHistoryProductsInStock(), step-1)
					+","+getIndexOrLast(w.getHistoryValueProductsInStock(),step-1)
					+","+getIndexOrLast(w.getHistoryShareAgentsWithNoMoney(),step-1)
					+","+getIndexOrLast(w.getHistoryMarketFailure(),step-1)
					+","+getIndexOrLast(w.getHistoryGiniUtility(),step-1)
					+","+getIndexOrLast(w.getHistoryGiniAddedValue(),step-1)
					+","+getIndexOrLast(w.getHistoryGiniMoney(),step-1)
					+","+getIndexOrLast(w.getHistoryGiniWealth(),step-1)
					+")";
			if(step%1000 == 0){
				state.execute(query);
				query = getBaseInsert();
				first = true;
			}
		}
		if(!query.equals(getBaseInsert())){
			state.execute(query);
		}
	}
	
	private String getIndexOrLast(ArrayList<Double> array, int index){
		if(index >= array.size())
			index = array.size() - 1;
		
		return String.valueOf(array.get(index));
	}
	
	public String getBaseInsert(){
		String query = "INSERT INTO "+getTableName()+"("
				+world_id
				+","+step
				+","+utility
				+","+money
				+","+addedValue
				+","+wealth
				+","+productsInStock
				+","+valueProductsInStock
				+","+shareAgentsWithNoMoney
				+","+marketFailure
				+","+giniUtility
				+","+giniAddedValue
				+","+giniMoney
				+","+giniWealth
		    + ") VALUES";
		return query;
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
