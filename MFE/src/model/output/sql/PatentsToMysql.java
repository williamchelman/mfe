package model.output.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import model.monopoly.Patent;
import model.monopoly.PatentOffice;

public class PatentsToMysql {
	public static final String World_id = "world_id";
	public static final String agent_index = "agent_index";
	public static final String product_index = "product_index";
	
	private Connection connection;
	private String tableName = "patents";
	
	public PatentsToMysql(Connection connection) throws SQLException {
		super();
		this.connection = connection;
		init();
	}

	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+World_id+" INT UNSIGNED"
			+","+agent_index+" INT UNSIGNED"
			+","+product_index+" INT UNSIGNED"
			+", PRIMARY KEY("+World_id+","+product_index+","+agent_index+")"
			+", FOREIGN KEY("+World_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		
		state.execute(query);
	}
	
	public void save(PatentOffice po, int worldID) throws SQLException{
		if(po.getPatents().size()>0){
			String query = "INSERT INTO "+getTableName()+"("
					+World_id
					+","+agent_index
					+","+product_index
				    + ")";
			query += " VALUES ";
			
			boolean first = true;
			for(Patent patent : po.getPatents()){
				if(first){
					first = false;
				} else {
					query += ",";
				}
				query += "("
						+worldID
						+","+patent.getAgent().getIndex()
						+","+patent.getProduct()
						+")";
			}
			Statement state = connection.createStatement();
			state.executeUpdate(query);
		}
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
