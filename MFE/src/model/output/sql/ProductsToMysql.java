package model.output.sql;

import helper.Helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Agent;
import model.Transaction;
import model.World;

public class ProductsToMysql {
	public static final String World_id = "world_id";
	public static final String product_index = "product_index";
	public static final String produced = "produced";
	public static final String sold = "sold";
	public static final String average_price = "average_price";
	public static final String std_price = "std_price";
	public static final String average_taste = "average_taste";
	public static final String std_taste = "std_taste";
	public static final String max_taste = "max_taste";
	public static final String average_skill = "average_skill";
	public static final String std_skill = "std_skill";
	public static final String min_skill = "min_skill";
	public static final String patented = "patented";
	
	private Connection connection;
	private String tableName = "products";
	
	public ProductsToMysql(Connection connection) throws SQLException {
		super();
		this.connection = connection;
		init();
	}

	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+World_id+" INT UNSIGNED"
			+","+product_index+" INT UNSIGNED"
			+","+produced+" INT UNSIGNED"
			+","+sold+" INT UNSIGNED"
			+","+average_price+" DOUBLE UNSIGNED"
			+","+std_price+" DOUBLE UNSIGNED"
			+","+average_taste+" DOUBLE UNSIGNED"
			+","+std_taste+" DOUBLE UNSIGNED"
			+","+max_taste+" DOUBLE UNSIGNED"
			+","+average_skill+" DOUBLE UNSIGNED"
			+","+std_skill+" DOUBLE UNSIGNED"
			+","+min_skill+" DOUBLE UNSIGNED"
			+","+patented+" BOOLEAN"
			+", PRIMARY KEY("+World_id+","+product_index+")"
			+", FOREIGN KEY("+World_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		
		state.execute(query);
	}
	
	public void save(World w, int worldID) throws SQLException{
		String query = "INSERT INTO "+getTableName()+"("
				+World_id
				+","+product_index
				+","+produced
				+","+sold
				+","+average_price
				+","+std_price
				+","+average_taste
				+","+std_taste
				+","+max_taste
				+","+average_skill
				+","+std_skill
				+","+min_skill
				+","+patented
			    + ")";
		query += " VALUES ";
		
		boolean first = true;
		
		int[] numberProduced = new int[w.getNumberOfProducts()];
		int[] numberSold = new int[w.getNumberOfProducts()];
		
		@SuppressWarnings("unchecked")
		ArrayList<Transaction>[] transactions = new ArrayList[w.getNumberOfProducts()];
		Double[][] tastes = new Double[w.getNumberOfProducts()][w.getNumberOfAgents()];
		Double[][] skills = new Double[w.getNumberOfProducts()][w.getNumberOfAgents()];
		
		for(int i = 0 ; i < w.getNumberOfProducts() ; ++i){
			numberProduced[i] = 0;
			numberSold[i] = 0;
			transactions[i] = new ArrayList<Transaction>();
		}
		
		for(int j = 0 ; j < w.getTheAgents().length ; ++j){
			Agent a = w.getTheAgents()[j];
			for(int product : a.getMyProducer().getMyProductions()){
				numberProduced[product]++;
			}
			for(int product = 0 ; product < w.getNumberOfProducts() ; ++product){
				tastes[product][j] = a.getMyConsumer().getMyTastes()[product];
				skills[product][j] = a.getMyProducer().getMySkills()[product];
			}
		}
		
		for(Transaction t : w.getMarket().getAllTransactions()){
			numberSold[t.getTheProduct()]++;
			transactions[t.getTheProduct()].add(t);
		}
		
		for(int product = 0 ; product < w.getNumberOfProducts() ; ++product){
			if(first){
				first = false;
			} else {
				query += ",";
			}
			Double[] prices = new Double[transactions[product].size()];
			for(int i = 0 ; i < prices.length ; ++i){
				prices[i] = transactions[product].get(i).getPrice();
			}
			query += "("
					+worldID
					+","+product
					+","+numberProduced[product]
					+","+numberSold[product]
					+","+Helper.getMean(prices)
					+","+Helper.getStdDev(prices)
					+","+Helper.getMean(tastes[product])
					+","+Helper.getStdDev(tastes[product])
					+","+Helper.getMax(tastes[product])
					+","+Helper.getMean(skills[product])
					+","+Helper.getStdDev(skills[product])
					+","+Helper.getMin(skills[product])
					+","+w.getPatentOffice().isPatented(product)
					+")";
		}
		Statement state = connection.createStatement();
		state.executeUpdate(query);
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
