package model.output.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Transaction;
import model.World;

public class TransactionToMysql {
	public static final String tableName = "transactions";
	
	public static final String world_id ="world_id";
	public static final String seller = "seller_index";
	public static final String buyer = "buyer_index";
	public static final String product = "product";
	public static final String price = "price";
	public static final String step = "step";
	
	private Connection connection;
	
	public TransactionToMysql(Connection connection) throws SQLException{
		this.connection = connection;
		init();
	}

	private void init() throws SQLException {
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+world_id+" INT UNSIGNED"
			+","+seller+" INT UNSIGNED"
			+","+buyer+" INT UNSIGNED"
			+","+product+" INT UNSIGNED"
			+","+price+" DOUBLE UNSIGNED"
			+","+step+" INT UNSIGNED"
			+", PRIMARY KEY("+world_id+","+step+")"
			+", FOREIGN KEY("+world_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		state.execute(query);
	}
	
	public void save(World w, int worldId) throws SQLException{
		Statement state = connection.createStatement();
		ArrayList<Transaction> transactions = w.getMarket().getAllTransactions();
		
		boolean first = true;
		int step = 1;
		String query = getBaseInsert();
		for(Transaction t : transactions){
			if(first){
				first = false;
			} else {
				query += ",";
			}
			query+= "("
					+worldId
					+","+t.getSeller().getIndex()
					+","+t.getBuyer().getIndex()
					+","+t.getSellingOffer().getTheProduct()
					+","+t.getPrice()
					+","+step
					+")";
			if(step%1000 == 0){
				state.execute(query);
				query = getBaseInsert();
				first = true;
			}
			++step;
		}
		if(!query.equals(getBaseInsert())){
			state.execute(query);
		}
	}
	
	public String getBaseInsert(){
		String query = "INSERT INTO "+getTableName()+"("
			+world_id
			+","+seller
			+","+buyer
			+","+product
			+","+price
			+","+step
		    + ") VALUES";
		return query;
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
