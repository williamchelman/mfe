package model.output.sql;

import java.sql.SQLException;

import model.output.WorldOutput;
import model.output.WorldOutputFactory;

public class MYSQLFactory implements WorldOutputFactory {
	private String url;
	private String port;
	private String db;
	private String user;
	private String psw;
	private String tableSuffix;
	
	private boolean saveHistory;
	private boolean saveLorentz;
	private boolean saveAgents;
	private boolean saveProducts;
	private boolean saveAgentsProducts;
	private boolean saveTransactions;
	private boolean savePatents;
	
	public MYSQLFactory(String url, String port, String db, String user,
			String psw, String tableSuffix,boolean saveHistory,boolean saveLorentz, boolean saveAgents, boolean saveProducts, boolean saveAgentsProducts, boolean saveTransactions,boolean savePatents) {
		super();
		this.url = url;
		this.port = port;
		this.db = db;
		this.user = user;
		this.psw = psw;
		this.tableSuffix = tableSuffix;
		this.saveHistory = saveHistory;
		this.saveLorentz = saveLorentz;
		this.saveAgents = saveAgents;
		this.saveProducts = saveProducts;
		this.saveAgentsProducts = saveAgentsProducts;
		this.saveTransactions = saveTransactions;
		this.savePatents = savePatents;
	}

	@Override
	public WorldOutput createWorldOutput() throws SQLException {
		WorldToMysql wo = new WorldToMysql(url, port, db, user, psw, tableSuffix,saveHistory,saveLorentz,saveAgents,saveProducts,saveAgentsProducts,saveTransactions,savePatents);
		return wo;
	}
}
