package model.output.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import model.World;

public class LorentzToMysql {
	public static final String world_id = "world_id";
	public static final String agent_proportion = "agent_proportion";
	public static final String wealth = "wealth";
	public static final String money = "money";
	public static final String utility = "utility";
	public static final String added_value = "added_value";
	
	private final String tableName = "lorentz_curves";
	
	private Connection connection;
	
	public LorentzToMysql(Connection connection) throws SQLException{
		this.connection = connection;
		init();
	}
	
	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+world_id+" INT UNSIGNED"
			+","+agent_proportion+" DOUBLE"
			+","+wealth+" DOUBLE"
			+","+money+" DOUBLE"
			+","+utility+" DOUBLE"
			+","+added_value+" DOUBLE"
			+", FOREIGN KEY("+world_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		state.execute(query);
	}
	
	public void save(World world, int worldId) throws SQLException{
		String query = "INSERT INTO "+getTableName()+"("
				+world_id
				+","+agent_proportion
				+","+wealth
				+","+money
				+","+utility
				+","+added_value
			    + ")";
		query += " VALUES ";
		query += "("
			+worldId
			+",0"
			+",0"
			+",0"
			+",0"
			+",0"
			+")";
		for(int index = 0 ; index < world.getLorentzAddedValue().length ; ++index){
			double agentProportion = 1.0*(index+1)/world.getLorentzAddedValue().length;
			query += ",";
			query+="(" 
					+worldId
					+","+agentProportion
					+","+world.getLorentzWealth()[index]
					+","+world.getLorentzMoney()[index]
					+","+world.getLorentzUtility()[index]
					+","+world.getLorentzAddedValue()[index]
					+")";
		}
		Statement state = connection.createStatement();
		state.executeUpdate(query);
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
