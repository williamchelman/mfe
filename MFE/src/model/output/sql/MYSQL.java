package model.output.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MYSQL {
	private static String driver = "jdbc:mysql";
	
	private MYSQL(){}
	
	public static Connection getConnection(String url, String port, String db, String user, String psw) throws SQLException{
		Connection connection = DriverManager.getConnection(driver+"://"+url+":"+port+"/"+db, user, psw);
		
		return connection;
	}
}
