package model.output.sql;

import helper.Helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import model.Agent;

public class AgentToMysql {
	public static final String index = "agent_index";
	public static final String World_id = "world_id";
	
	public static final String added_value = "added_value";
	public static final String frustration = "frustration";
	public static final String money = "money";
	public static final String products_brought = "products_brought";
	public static final String products_sold = "products_sold";
	public static final String products_produced = "products_produced";
	public static final String utility = "utility";
	public static final String wealth = "wealth";
	public static final String max_taste = "max_taste";
	public static final String min_skill = "min_skill";
	public static final String products_in_stock = "products_in_stock";
	public static final String own_a_patent = "own_a_patent";
	public static final String is_lowcost = "is_lowcost";
	public static final String lowcost_products_brought = "lowcost_products_brought";
	
	
	private Connection connection;
	private String tableName = "agents";
	
	public AgentToMysql(Connection connection) throws SQLException {
		super();
		this.connection = connection;
		init();
	}

	public void init() throws SQLException{
		Statement state = connection.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS "+getTableName()+"("
			+index+" INT UNSIGNED"
			+","+World_id+" INT UNSIGNED"
			+","+added_value+" DOUBLE"
			+","+frustration+" INT UNSIGNED"
			+","+money+" DOUBLE"
			+","+utility+" DOUBLE"
			+","+wealth+" DOUBLE"
			+","+min_skill+ " DOUBLE UNSIGNED"
			+","+max_taste+" DOUBLE UNSIGNED"
			+","+products_brought+" INT UNSIGNED"
			+","+products_sold+" INT UNSIGNED"
			+","+products_produced+" INT UNSIGNED"
			+","+products_in_stock+" INT UNSIGNED"
			+","+own_a_patent+" BOOL"
			+","+is_lowcost+" BOOL"
			+","+lowcost_products_brought+" INT UNSIGNED"
			+", PRIMARY KEY("+World_id+","+index+")"
			+", FOREIGN KEY("+World_id+") REFERENCES " + WorldToMysql.getTableName() + "(id) ON DELETE CASCADE"
		    + ")";
		
		state.execute(query);
	}
	
	public void saveAgents(Agent[] agents, int worldID) throws SQLException{
		String query = "INSERT INTO "+getTableName()+"("
				+World_id
				+","+index
				+","+added_value
				+","+frustration
				+","+money
				+","+utility
				+","+wealth
				+","+min_skill
				+","+max_taste
				+","+products_brought
				+","+products_sold
				+","+products_produced
				+","+products_in_stock
				+","+own_a_patent
				+","+is_lowcost
				+","+lowcost_products_brought
			    + ")";
		query += " VALUES ";
		
		Agent[] agentsUtility = copyAgents(agents);
    	Arrays.sort(agentsUtility, Agent.sortUtilityAscending);
    	Agent[] agentsMoney = copyAgents(agents);
    	Arrays.sort(agentsMoney, Agent.sortMoneyAscending);
    	Agent[] agentsAddedValue = copyAgents(agents);
    	Arrays.sort(agentsAddedValue, Agent.sortAddedValueAscending);
		
		boolean first = true;
		for(Agent a : agents){
			if(first){
				first = false;
			} else {
				query += ",";
			}
			
			int productsInStock = 0;
			for(int quantity : a.getMyProducer().getProducts()){
				productsInStock += quantity;
			}
			int addedValuePosition = agents.length - findPosition(agentsAddedValue, a);
			int moneyPosition = agents.length - findPosition(agentsMoney, a);
			int utilityPosition = agents.length - findPosition(agentsUtility, a);
			
			boolean isLowCost = false;
			if(a.getMyProducer().getQuality() != 1)
				isLowCost = true;
			
			query += "("
					+worldID
					+","+a.getIndex()
					+","+a.getAddedValue()
					+","+a.getFrustration()
					+","+a.getMoney()
					+","+a.getUtility()
					+","+a.getWealth()
					+","+addedValuePosition
					+","+moneyPosition
					+","+Helper.getMin(a.getMyProducer().getMySkills())
					+","+utilityPosition
					+","+Helper.getMax(a.getMyConsumer().getMyTastes())
					+","+a.getTotalNumberOfProductsBought()
					+","+a.getTotalNumberOfProductsSold()
					+","+a.getMyProducer().getMyProductions().size()
					+","+ productsInStock
					+","+a.getMyProducer().getPatentOffice().hasPatent(a)
					+","+isLowCost
					+","+a.getTotalNumberOfLowcostProductsBought()
					+")";
		}
		Statement state = connection.createStatement();
		state.executeUpdate(query);
	}
	
	private Agent[] copyAgents(Agent[] agents){
		Agent[] result = new Agent[agents.length];
		
		for(int i = 0 ; i < agents.length ; ++i){
			result[i] = agents[i];
		}
		
		return result;
	}
	
	private int findPosition(Agent[] agents, Agent a){
		for(int i = 0 ; i < agents.length ; ++i){
			if(agents[i] == a)
				return i;
		}
		return -1;
	}
	
	public String getTableName(){
		return WorldToMysql.tableSuffix+tableName;
	}
}
