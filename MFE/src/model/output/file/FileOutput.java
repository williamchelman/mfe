package model.output.file;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

import model.World;
import model.output.WorldOutput;

public class FileOutput implements WorldOutput {
	private PrintWriter pw;
	private int index;
	private char separator;
	private char decimal;
	
	public FileOutput(String fileName, char separator, char decimal) throws FileNotFoundException{
		this.separator = separator;
		this.decimal = decimal;
		pw = new PrintWriter(fileName);
		index = 1;
		
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("World");
		
		columns.add("intended steps");
		columns.add("steps");
		columns.add("number of products");
		columns.add("number of agents");
		columns.add("initial agent's money");
		columns.add("initial agent's utility");
		columns.add("number of initial production");
		columns.add("maximum possible benefit");
		columns.add("number of last transactions to consider");
		columns.add("part of random production");
		columns.add("market randomness");
		columns.add("production number for specialization");
		columns.add("diminishing marginal utility");
		columns.add("influenceability");
		
		columns.add("Gini added value");
		columns.add("Global added value");
		columns.add("Gini money");
		columns.add("Global money");
		columns.add("Gini utility");
		columns.add("Global utility");
		columns.add("Gini wealth");
		columns.add("Global wealth");
		columns.add("Value of products in stock");
		columns.add("Number of market failure");
		columns.add("Share agent with no money");
		columns.add("Share agent with no money and no products");
		
		pw.println(createLine(columns));
	}
	
	private String createLine(ArrayList<String> values){
		String out = "";
		for(String s : values){
			if(!out.equals("")){
				out += separator;
			}
			out += s;
		}
		if(decimal != '.'){
			out = out.replace('.', decimal);
		}
		
		return out;
	}
	
	public void saveWorld(World w, int maxStep){
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("World " + index);
		columns.add(String.valueOf(maxStep));
		columns.add(String.valueOf(w.getCurrentStep()));
		
		//Parameters
		columns.add(String.valueOf(w.getNumberOfProducts()));
		columns.add(String.valueOf(w.getNumberOfAgents()));
		columns.add(String.valueOf(w.getInitialMoneyAgent()));
		columns.add(String.valueOf(w.getInitialUtilityAgent()));
		columns.add(String.valueOf(w.getInitialProduction()));
		columns.add(String.valueOf(w.getMaxBenefit()));
		columns.add(String.valueOf(w.getNumberOfLastTransactions()));
		columns.add(String.valueOf(w.getPartRandomProduction()));
		columns.add(String.valueOf(w.getPartRandomMarket()));
		columns.add(String.valueOf(w.getSpecializedProduction()));
		columns.add(String.valueOf(w.getDecreasingUtility()));
		columns.add(String.valueOf(w.getInfluenceability()));
		
		//Values
		columns.add(String.valueOf(w.computeGiniAddedValue()));
		columns.add(String.valueOf(w.computeGlobalAddedValue()));
		columns.add(String.valueOf(w.computeGiniMoney()));
		columns.add(String.valueOf(w.computeGlobalMoney()));
		columns.add(String.valueOf(w.computeGiniUtility()));
		columns.add(String.valueOf(w.computeGlobalUtility()));
		columns.add(String.valueOf(w.computeGiniWealth()));
		columns.add(String.valueOf(w.computeGlobalWealth()));
		columns.add(String.valueOf(w.computeValueOfProductsInStocks()));
		columns.add(String.valueOf(w.getNumberOfMarketFailures()));
		columns.add(String.valueOf(w.shareAgentsWithNoMoney()));
		columns.add(String.valueOf(w.shareAgentsWithNoMoneyAndNoProduct()));
		
		pw.println(createLine(columns));
		
		pw.flush();
		
		++index;
	}
	
	public void close(){
		pw.close();
	}

	@Override
	public void saveWorlds(World[] worlds, int maxStep) throws Exception {
		for(World w : worlds){
			saveWorld(w, maxStep);
		}
		
	}
}
