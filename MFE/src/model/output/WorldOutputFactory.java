package model.output;

public interface WorldOutputFactory {
	public WorldOutput createWorldOutput() throws Exception;
}
