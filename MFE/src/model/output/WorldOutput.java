package model.output;

import model.World;

public interface WorldOutput {
	public void saveWorld(World w, int maxStep) throws Exception;
	public void saveWorlds(World[] w, int maxStep) throws Exception;
	public void close() throws Exception;
}
