package model;

/**
 * Old class for the competitive market (use mixed market now)
 * @author nicolasbernier
 *
 */
@Deprecated
public class CompetitiveMarket extends Market
{
    private Offer[] bestSellingOffers;
    private Offer[] bestBuyingOffers;

    public CompetitiveMarket(Agent[] theAgents,int initialProduction,int numberOfLastTransactions,int numberOfProducts,double maxBenefit) {
		super(theAgents,initialProduction,numberOfLastTransactions,numberOfProducts,maxBenefit);
    }

    public void initialize()
    {
        bestSellingOffers = new Offer[this.numberOfProducts];
        int numberOfAttempts = 0;
        double price = this.maxBenefit;
        for (int i = 0; i < this.numberOfProducts; i++)
        {
            Boolean done = false;
            while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
            {
            	//get an agent randomly
                Agent ag = theAgents[World.ran.nextInt(this.theAgents.length)];
                if (ag.getMyProducer().getProducts()[i] > 0)
                {
                    bestSellingOffers[i] = new Offer(i, ag, price, Direction.sell);
                    done = true;
                }
                else
                {
                    numberOfAttempts++;
                }
            }
        }
        bestBuyingOffers = new Offer[this.numberOfProducts];
        price = 0.0;
        for (int i = 0; i < this.numberOfProducts; i++)
        {
            Agent ag = theAgents[World.ran.nextInt(this.theAgents.length)];
            bestBuyingOffers[i] = new Offer(i, ag, price, Direction.buy);
        }
    }

    public void tick(int i) throws MarketFailingException //RJ version
    {
        this.initialize();
        super.tick(i);
    }

    

	@Override
	public Transaction createTransaction() {
		Boolean done = false;
        int numberOfAttempts = 0;
        Transaction t = null;

        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
        {
            Offer sellof = theAgents[World.ran.nextInt(this.theAgents.length)].getCompetitiveSeller().sell();
            if ((sellof != null) && (bestBuyingOffers[sellof.getTheProduct()] != null) && (sellof.getTheAgent() != bestBuyingOffers[sellof.getTheProduct()].getTheAgent()) && (sellof.getPrice() <= bestBuyingOffers[sellof.getTheProduct()].getPrice()))
            {
                done = true;
                t = new Transaction(sellof, bestBuyingOffers[sellof.getTheProduct()], this);
                numberOfTransactions++;
            }
            else
            {
                if (!done)
                {
                    Offer buyof = theAgents[World.ran.nextInt(this.theAgents.length)].getCompetitiveBuyer().buy(null);
                    if ((buyof != null) && (bestSellingOffers[buyof.getTheProduct()] != null) && (buyof.getTheAgent() != bestSellingOffers[buyof.getTheProduct()].getTheAgent()) && (buyof.getPrice() >= bestSellingOffers[buyof.getTheProduct()].getPrice()))
                    {
                        done = true;
                        t = new Transaction(bestSellingOffers[buyof.getTheProduct()], buyof, this);
                        numberOfTransactions++;
                    }
                    else
                    {
                        numberOfAttempts++;
                    }
                }
                numberOfAttempts++;
            }
        }
        return t;
	}

	/**
	 * @return the bestSellingOffers
	 */
	public Offer[] getBestSellingOffers() {
		return bestSellingOffers;
	}

	/**
	 * @param bestSellingOffers the bestSellingOffers to set
	 */
	public void setBestSellingOffers(Offer[] bestSellingOffers) {
		this.bestSellingOffers = bestSellingOffers;
	}

	/**
	 * @return the bestBuyingOffers
	 */
	public Offer[] getBestBuyingOffers() {
		return bestBuyingOffers;
	}

	/**
	 * @param bestBuyingOffers the bestBuyingOffers to set
	 */
	public void setBestBuyingOffers(Offer[] bestBuyingOffers) {
		this.bestBuyingOffers = bestBuyingOffers;
	}
	
}
