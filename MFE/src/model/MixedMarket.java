package model;



/**
 * Mixed Market
 * @author nicolasbernier
 *
 */
public class MixedMarket extends Market {

	private Offer[] bestSellingOffers;
    private Offer[] bestBuyingOffers;
    
    Double currentRandom;
    /**
     * Constructor 
     * @param theAgents - the agents of the market
     * @param initialProduction - the amount of initial production
     * @param maxNumberOfSavedTransactions - the max number of saved transacions
     * @param numberOfProducts - the amount of products
     * @param maxBenefit - the max benefit
     */
	public MixedMarket(Agent[] theAgents,int initialProduction,int maxNumberOfSavedTransactions,int numberOfProducts,double maxBenefit) {
		super(theAgents,initialProduction,maxNumberOfSavedTransactions,numberOfProducts,maxBenefit);
	}
	/**
	 * Initialize the best buying and selling offers
	 */
	 
    public void initialize()
    {
    	//To operate the up down bidding system
        bestSellingOffers = new Offer[this.numberOfProducts];
        int numberOfAttempts = 0;
        double price = this.maxBenefit;
        for (int i = 0; i < this.numberOfProducts; i++)
        {
            Boolean done = false;
            while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
            {
            	//get an agent randomly
                Agent ag = theAgents[World.ran.nextInt(this.theAgents.length)];
                if (ag.getMyProducer().getProducts()[i] > 0)
                {
                    bestSellingOffers[i] = new Offer(i, ag, price, Direction.sell);
                    done = true;
                }
                else
                {
                    numberOfAttempts++;
                }
            }
        }
        bestBuyingOffers = new Offer[this.numberOfProducts];
        price = 0.0;
        for (int i = 0; i < this.numberOfProducts; i++)
        {
            Agent ag = theAgents[World.ran.nextInt(this.theAgents.length)];
            bestBuyingOffers[i] = new Offer(i, ag, price, Direction.buy);
        }
    }
    /**
     * On step of the market
     */
    public void tick(int i) throws MarketFailingException //RJ version
    {
    	this.currentRandom=World.ran.nextDouble();
        super.tick(i);
    }
    
	@Override
	public Transaction createTransaction() {
		//Handle randomness
		if(this.randomness>this.currentRandom){
			return randomTransaction();
		}
		else{
			return competitiveTransaction();
		}
	}
	/**
	 * Create a random transaction
	 * @return the transaction
	 */
	private Transaction randomTransaction(){
		Boolean done = false;
        int numberOfAttempts = 0;
        Transaction t = null;

        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
        {
            Offer sellof = theAgents[World.ran.nextInt(this.theAgents.length)].getRandomSeller().sell();
            if (sellof != null)
            {
                Offer buyof = theAgents[World.ran.nextInt(this.theAgents.length)].getRandomBuyer().buy(sellof);
                if (buyof != null)
                {
                    done = true;
                    t = new Transaction(sellof, buyof, this);
                    numberOfTransactions++;
                }
                else
                {
                    numberOfAttempts++;
                }
            }
            else
            {
                numberOfAttempts++;
            }
        }
        return t;
	}
	/**
	 * Create a Competitive transaction
	 * @return the transaction
	 */
	private Transaction competitiveTransaction(){
		Boolean done = false;
        int numberOfAttempts = 0;
        Transaction t = null;
        this.initialize();
        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
        {
            Offer sellof = theAgents[World.ran.nextInt(this.theAgents.length)].getCompetitiveSeller().sell();
            Offer buyof=null;
            if(sellof !=null){
	            buyof = bestBuyingOffers[sellof.getTheProduct()];
	            if ( (buyof != null) && (sellof.getTheAgent() != buyof.getTheAgent()) && (sellof.getPrice() <= buyof.getPrice())){
	                done = true;
	                t = new Transaction(sellof,buyof, this);
	                numberOfTransactions++;
	            }
            }
            else{
            	if(!done){
	            	buyof = theAgents[World.ran.nextInt(this.theAgents.length)].getCompetitiveBuyer().buy(null);
	                if(buyof !=null){
	                	sellof = bestSellingOffers[buyof.getTheProduct()];
	                	//verify that the product is available
	                	if(sellof !=null && sellof.getTheAgent().getMyProducer().getProducts()[buyof.getTheProduct()]>0){
		                   if ((buyof.getTheAgent() != sellof.getTheAgent()) && (buyof.getPrice() >= sellof.getPrice())){
		                	   done = true;
			                   t = new Transaction(sellof, buyof, this);
			                   numberOfTransactions++;
			               }
	                    }
	                }
            	}
                numberOfAttempts++;
            }
        } 
        return t;
	}
	
	/**
	 * @return the bestSellingOffers
	 */
	public Offer[] getBestSellingOffers() {
		return bestSellingOffers;
	}

	/**
	 * @param bestSellingOffers the bestSellingOffers to set
	 */
	public void setBestSellingOffers(Offer[] bestSellingOffers) {
		this.bestSellingOffers = bestSellingOffers;
	}

	/**
	 * @return the bestBuyingOffers
	 */
	public Offer[] getBestBuyingOffers() {
		return bestBuyingOffers;
	}

	/**
	 * @param bestBuyingOffers the bestBuyingOffers to set
	 */
	public void setBestBuyingOffers(Offer[] bestBuyingOffers) {
		this.bestBuyingOffers = bestBuyingOffers;
	}

}
