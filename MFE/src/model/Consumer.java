package model;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * The consumer part of the agent
 * @author nicolasbernier
 *
 */
public class Consumer
{
	
    private Agent myAgent;
    private double[] myTastes;
    private ArrayList<Double[]> myUtilityFunction = new ArrayList<Double[]>(); // Utility modification for product i at each consumption of any product !
    
    /**
     * Constructor
     * @param myAgent - the parent agent
     */
    public Consumer(Agent myAgent)
    {
        this.myAgent = myAgent;
        this.initTaste();
    }
    /**
     * Initialize the taste of the consumer
     */
    public void initTaste(){
    	double totalTaste = 0.0;
        //One taste for every product
        myTastes = new double[this.myAgent.getNumberOfProducts()];
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            myTastes[i] = World.ran.nextDouble(); 
            totalTaste += myTastes[i];
        }
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            myTastes[i] = (myTastes[i] / totalTaste); // random normalized tastes between [0,1] --> around 0.5/NbOfAgents --> 0.01
        }
        this.logUtilityFunction();
    }
    /**
     * Consume a product
     * @param p - the product
     * @param quality 
     */
    public void consume(int p, double quality)
    {
        myAgent.increaseUtility(myTastes[p]*quality);
        if(myAgent.isDecreasingUtility()){
        	double range = myTastes[p] * 0.1;
        	double min = Math.max((myTastes[p] - range), 0.0);
        	myTastes[p] = World.getRandomNumber(min, myTastes[p]); // utility of a product decreases with consumption
	        double totalTaste = 0.0;
	        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
	        {
	            totalTaste += myTastes[i];
	        }
	        //Normalization
	        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
	        {
	            myTastes[i] = (myTastes[i] / totalTaste);
	        }
	        //Could be removed
	        this.logUtilityFunction();
        }
    }
    /**
     * Reset the customer
     */
    public void reset()
    {
    	
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            myTastes[i] = this.myUtilityFunction.get(0)[i];
        }
        this.logUtilityFunction();  
    }

    /**
     * Save the current state of the tastes
     */
    public void logUtilityFunction()
    {
    	Double[] d=new Double[this.myAgent.getNumberOfProducts()];
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            d[i] = myTastes[i];
        }
        this.myUtilityFunction.add(d);
    }
    
    /**
     * Compute the Gini coefficient of the tastes
     * @return
     */
    public double computeGiniTastes() // Compute Gini Tastes
    {
        double gini = 0.0;
        double[] tastes =Arrays.copyOf(myTastes, this.myAgent.getNumberOfProducts());
        Arrays.sort(tastes);

        double share = 1.0 / (double)this.myAgent.getNumberOfProducts();
        double cumul = 0.0;
        double a = 0.0;
        double prevCumul = 0.0;

        for (double t : tastes)
        {
            cumul += t;
            a = prevCumul + cumul;
            prevCumul = cumul;
            gini += a * share;
        }
        return Math.abs(1 - gini);
    }
    
    /*
     * Getter/Setter
     */

	/**
	 * @return the myAgent
	 */
	public Agent getMyAgent() {
		return myAgent;
	}

	/**
	 * @param myAgent the myAgent to set
	 */
	public void setMyAgent(Agent myAgent) {
		this.myAgent = myAgent;
	}

	/**
	 * @return the myTastes
	 */
	public double[] getMyTastes() {
		return myTastes;
	}

	/**
	 * @param myTastes the myTastes to set
	 */
	public void setMyTastes(double[] myTastes) {
		this.myTastes = myTastes;
	}

	/**
	 * @return the myUtilityFunction
	 */
	public ArrayList<Double[]> getMyUtilityFunction() {
		return myUtilityFunction;
	}

	/**
	 * @param myUtilityFunction the myUtilityFunction to set
	 */
	public void setMyUtilityFunction(ArrayList<Double[]> myUtilityFunction) {
		this.myUtilityFunction = myUtilityFunction;
	}




}