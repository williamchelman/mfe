package model;

/**
 * The random buyer part of the agent
 * @author nicolasbernier
 *
 */
public class RandomSeller extends Seller
{
	/**
	 * Constructor of the random seller
	 * @param myAgent - the parent agent
	 */
    public RandomSeller(Agent myAgent){
       super(myAgent);
    }

	@Override
	public Offer sell() {
		int temp = selectProductToSell();
        this.myAgent.incrementSellingChances();
        if (temp != -1)
        {
            return new Offer(temp, myAgent, myAgent.getMyProducer().getProductionCost(temp) / (myAgent.getMoney() * myAgent.getTheMarket().getTimeIndex()), Direction.sell);
        }
        else
        {
            return null;
        }
	}

	@Override
	public int selectProductToSell() {
		{
	        Boolean done = false;
	        int pro = -1;
	        int numberOfAttempts = 0;
	        if (myAgent.getMyProducer().getNumberOfProducts() > 0)
	        {
	            while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
	            {
	                int ra = World.ran.nextInt(this.myAgent.getNumberOfProducts());
	                numberOfAttempts++;
	                if (myAgent.getMyProducer().getProducts()[ra] != 0)
	                {
	                    pro = ra;
	                    done = true;
	                }
	            }
	        }
	        return pro;
	    }
	}
}