package model;

import java.util.ArrayList;
import java.util.Random;

public class Mimicry {
	private Agent[][] agentsSpace;
	private int width;
	private int height;
	private Random r = new Random();
	private double influenceability = 0;
	
	public Mimicry(){
		this.influenceability = 0;
	}
	
	public Mimicry(double influenceability){
		this.influenceability = influenceability;
	}
	
	public void init(Agent[] agents){
		width = (int) Math.round(Math.sqrt(agents.length)+0.5);
		height = width;
		agentsSpace = new Agent[width][height];
		int x = 0;
		int y = 0;
		
		for(int i = 0 ; i < agents.length ; ++i){
			agentsSpace[x][y] = agents[i];
			++x;
			if(x == width){
				++y;
				x = 0;
			}
		}
	}
	
	public void tick(){
		if(this.influenceability > 0 && agentsSpace != null){
			int x0 = 0;
			int y0 = 0;
			while(true){
				x0 = r.nextInt(width);
				y0 = r.nextInt(height);
				if(agentsSpace[x0][y0] != null){
					break;
				}
			}
			
			ArrayList<Agent> neighbours = new ArrayList<Agent>();
			
			for(int i = -1 ; i <= 1 ; ++i){
				for(int j = -1 ; j <= 1 ; ++j){
					if(i != 0 || j != 0){
						int x = x0 + i;
						int y = y0 + j;
						
						if(x >= 0 && x < width && y >= 0 && y < height && agentsSpace[x][y] != null){
							neighbours.add(agentsSpace[x][y]);
						}
					}
				}
			}
			
			double[] neighboursAverageTastes = computeAverageTastes(neighbours);
			double[] oldTastes = agentsSpace[x0][y0].getMyConsumer().getMyTastes();
			double[] newTastes = new double[oldTastes.length];
			double sum = 0;
			for(int i = 0 ; i < neighboursAverageTastes.length ; ++i){
				newTastes[i] = (1 - influenceability)*oldTastes[i] + influenceability*neighboursAverageTastes[i];
				sum+=newTastes[i];
			}
			for(int i = 0 ; i < newTastes.length ; ++i){
				newTastes[i] = newTastes[i]/sum;
			}
			agentsSpace[x0][y0].getMyConsumer().setMyTastes(newTastes);
		}
	}
	
	private double[] computeAverageTastes(ArrayList<Agent> agents){
		double[] averageTastes = new double[agents.get(0).getNumberOfProducts()];
		
		for(int i = 0 ; i < averageTastes.length ; ++i){
			averageTastes[i] = 0.0;
		}
		
		int size = agents.size();
		for(int i = 0 ; i < size ; ++i){
			Agent a = agents.get(i);
			Consumer c = a.getMyConsumer();
			for(int j = 0 ; j < c.getMyTastes().length ; ++j){
				averageTastes[j] += c.getMyTastes()[j];
			}
		}
		
		for(int i = 0 ; i < averageTastes.length ; ++i){
			averageTastes[i] = averageTastes[i]/size;
		}
		
		return averageTastes;
	}

	public double getInfluenceability() {
		return influenceability;
	}

	public void setInfluenceability(double influenceability) {
		this.influenceability = influenceability;
	}
	
	
}
