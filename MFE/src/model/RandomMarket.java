package model;

/**
 * Old class for the random market (use mixed market now)
 * @author nicolasbernier
 *
 */
@Deprecated
public class RandomMarket extends Market
{
    public RandomMarket(Agent[] theAgents,int initialProduction,int maxNumberOfSavedTransactions,int numberOfProducts,double maxBenefit) {
		super(theAgents,initialProduction,maxNumberOfSavedTransactions,numberOfProducts,maxBenefit);
    }

	@Override
	public Transaction createTransaction() {
		Boolean done = false;
        int numberOfAttempts = 0;
        Transaction t = null;

        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS))
        {
            Offer sellof = theAgents[World.ran.nextInt(this.theAgents.length)].getRandomSeller().sell();
            if (sellof != null)
            {
                Offer buyof = theAgents[World.ran.nextInt(this.theAgents.length)].getRandomBuyer().buy(sellof);
                if (buyof != null)
                {
                    done = true;
                    t = new Transaction(sellof, buyof, this);
                    numberOfTransactions++;
                }
                else
                {
                    numberOfAttempts++;
                }
            }
            else
            {
                numberOfAttempts++;
            }
        }
        return t;
	}
}
