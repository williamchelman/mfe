package model.stat.parameter;

import model.World;

public class DiminishingMarginaleUtility extends BoolParameter {

	public DiminishingMarginaleUtility() {
		super("diminishing marginality");
	}

	@Override
	public void applyToWorld(World w) {
		w.setDecreasingUtility(getValue());
	}

	@Override
	public DiminishingMarginaleUtility clone() {
		DiminishingMarginaleUtility p = new DiminishingMarginaleUtility();
		BoolParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getDecreasingUtility());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
