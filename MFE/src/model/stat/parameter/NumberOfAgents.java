package model.stat.parameter;

import model.World;

public class NumberOfAgents extends IntParameter {

	public NumberOfAgents() {
		super("Number of agents");
	}

	@Override
	public void applyToWorld(World w) {
		w.setNumberOfAgents(this.getValue());

	}

	@Override
	public Parameter clone() {
		NumberOfAgents p = new NumberOfAgents();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getNumberOfAgents());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
