package model.stat.parameter;

import model.World;

public class NumberOfLowcostAgents extends IntParameter {

	public NumberOfLowcostAgents() {
		super("lowcost agents");
	}

	@Override
	public void applyToWorld(World w) {
		w.setNumberOfLowcostAgents(this.getValue());
	}

	@Override
	public Parameter clone() {
		NumberOfLowcostAgents p = new NumberOfLowcostAgents();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getNumberOfLowcostAgents());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
