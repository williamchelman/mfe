package model.stat.parameter;

public abstract class BoolParameter extends Parameter {
	private boolean value;
	private boolean first = true;
	private boolean allDone = false;
	private boolean variates;
	
	public BoolParameter(String label) {
		super(label);
		value = false;
		variates = false;
	}

	@Override
	public void init() {
		first = true;
		allDone = false;
	}

	@Override
	public boolean nextValue() {
		if(variates){
			if(!allDone){
				if(first){
					first = false;
				} else {
					value = !value;
					allDone = true;
				}
				return true;
			} else {
				return false;
			}
		} else {
			if(first){
				first = false;
				return true;
			} else {
				return false;
			}
		}
	}

	@Override
	public int valueNumber() {
		if(variates){
			return 2;
		}
		return 1;
	}

	@Override
	public String getStringValue() {
		return Boolean.toString(value);
	}
	
	@Override
	public void setValue(String value){
		this.setValue(Boolean.valueOf(value));
	}

	public boolean getValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}
	
	public void setVariates(boolean variates){
		this.variates = variates;
	}
	
	public boolean isVariates() {
		return variates;
	}
	
	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public static <T extends BoolParameter> void clone(T toCopy, T copy){
		copy.setLabel(toCopy.getLabel());
		copy.setValue(toCopy.getValue());
		copy.setVariates(toCopy.isVariates());
		copy.setFirst(toCopy.isFirst());
	}
}
