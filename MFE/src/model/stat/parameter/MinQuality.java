package model.stat.parameter;

import model.World;

public class MinQuality extends DoubleParameter {

	public MinQuality() {
		super("min quality");
	}

	@Override
	public void applyToWorld(World w) {
		w.setMinQuality(this.getValue());
	}
	
	@Override
	public MinQuality clone(){
		MinQuality p = new MinQuality();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getMinQuality());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}
}
