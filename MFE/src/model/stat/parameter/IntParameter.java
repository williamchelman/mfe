package model.stat.parameter;

import java.util.ArrayList;

public abstract class IntParameter extends Parameter {
	private boolean first;
	private int min = 0;
	private int max = 1;
	private int step = 1;
	private int value;
	private ArrayList<Integer> valueList = new ArrayList<Integer>();
	private int valueSetIndex = 0;
	
	public IntParameter(String label, int min, int max) {
		super(label);
		first = true;
		this.min = min;
		this.max = max;
		this.value = this.min;
		
	}
	
	public IntParameter(String label){
		super(label);
		this.value = this.min;
	}
	
	public void addValue(int value){
		valueList.add(value);
	}
	
	public void emptyValueList(){
		valueList.clear();
	}
	
	@Override
	public String getStringValue(){
		return Double.toString(value);
	}
	
	@Override
	public void setValue(String value){
		this.setValue(Integer.valueOf(value));
	}
	
	@Override
	public int valueNumber(){
		double temp = max - min;
		temp = temp/step;
		return (int) (Math.round(temp) + 1);
	}
	
	@Override
	public void init(){
		first = true;
		setValue(min);
		
	}
	
	@Override
	public boolean nextValue(){
		if(first){
			setValue(min);
			first = false;
			return true;
		} else {
			if(value + step > max){
				return false;
			}
			setValue(value + step);
			return true;
		}
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public static <T extends IntParameter> void clone(T toCopy, T copy){
		copy.setLabel(toCopy.getLabel());
		copy.setValue(toCopy.getValue());
		copy.setMin(toCopy.getMin());
		copy.setMax(toCopy.getMax());
		copy.setStep(toCopy.getStep());
		copy.setValueSetIndex(toCopy.getValueSetIndex());
		copy.emptyValueList();
		for(int value : toCopy.getValueList()){
			copy.addValue(value);
		}
	}

	public ArrayList<Integer> getValueList() {
		return valueList;
	}

	public void setValueList(ArrayList<Integer> valueList) {
		this.valueList = valueList;
	}

	public int getValueSetIndex() {
		return valueSetIndex;
	}

	public void setValueSetIndex(int valueSetIndex) {
		this.valueSetIndex = valueSetIndex;
	}
}
