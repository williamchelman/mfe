package model.stat.parameter;

import model.World;

public class MarketRandomness extends DoubleParameter {

	public MarketRandomness() {
		super("market randomness");
	}

	@Override
	public void applyToWorld(World w) {
		w.setPartRandomMarket(this.getValue());
	}
	
	@Override
	public MarketRandomness clone(){
		MarketRandomness p = new MarketRandomness();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getPartRandomMarket());
	}

	@Override
	public boolean applyBeforeInit() {
		return false;
	}
}
