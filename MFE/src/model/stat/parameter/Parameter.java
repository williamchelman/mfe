package model.stat.parameter;

import model.World;

public abstract class Parameter implements Cloneable {
	
	protected String label;
	
	public Parameter(String label) {
		this.label = label;
	}
	
	public abstract void init();
	public abstract boolean nextValue();
	
	public abstract void applyToWorld(World w);
	
	@Override
	public String toString(){
		return label;
	}
	
	public abstract int valueNumber();

	public abstract String getStringValue();
	public abstract void setValue(String value);

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public abstract Parameter clone();
	
	public abstract String getValueFromWorld(World w);
	
	public abstract boolean applyBeforeInit();
}
