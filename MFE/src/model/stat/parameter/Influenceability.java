package model.stat.parameter;

import model.World;

public class Influenceability extends DoubleParameter {
	
	public Influenceability() {
		super("influenceability");
	}

	@Override
	public void applyToWorld(World w) {
		w.setInfluenceability(getValue());
	}
	
	@Override
	public Influenceability clone(){
		Influenceability p = new Influenceability();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getInfluenceability());
	}

	@Override
	public boolean applyBeforeInit() {
		return false;
	}

}
