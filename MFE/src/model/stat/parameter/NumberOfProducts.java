package model.stat.parameter;

import model.World;

public class NumberOfProducts extends IntParameter {

	public NumberOfProducts() {
		super("Number of products");
	}

	@Override
	public void applyToWorld(World w) {
		w.setNumberOfProducts(this.getValue());

	}

	@Override
	public Parameter clone() {
		NumberOfProducts p = new NumberOfProducts();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getNumberOfProducts());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
