package model.stat.parameter;

import java.util.ArrayList;

public abstract class DoubleParameter extends Parameter {
	public final static int INCREMENTAL_MODE = 0;
	public final static int LIST_MODE = 1;
	
	private boolean first = true;
	private double min = 0;
	private double max = 1;
	private double step = 0.05;
	private double value;
	private int precision = 1000;
	private ArrayList<Double> valueList = new ArrayList<Double>();
	private int valueSetIndex = 0;
	private int mode = INCREMENTAL_MODE;
	
	protected DoubleParameter(String label, double min, double max) {
		super(label);
		this.min = min;
		this.max = max;
		this.value = this.min;
		
	}
	
	protected DoubleParameter(String label){
		super(label);
		this.value = this.min;
	}
	
	public void addValue(double value){
		valueList.add(value);
	}
	
	public void emptyValueList(){
		valueList.clear();
	}
	
	@Override
	public String getStringValue(){
		return Double.toString(value);
	}
	
	@Override
	public void setValue(String value){
		this.setValue(Double.valueOf(value));
	}
	
	@Override
	public int valueNumber(){
		if(mode == DoubleParameter.LIST_MODE){
			return valueList.size();
		} else if(mode == DoubleParameter.INCREMENTAL_MODE){
			double temp = max - min;
			temp = temp/step;
			return (int) (Math.round(temp) + 1);
		}
		return 0;
	}
	
	@Override
	public void init(){
		if(mode == DoubleParameter.INCREMENTAL_MODE){
			first = true;
			setValue(min);
		} else if(mode == DoubleParameter.LIST_MODE){
			valueSetIndex = 1;
			setValue(valueList.get(0));
		}
		
	}
	
	@Override
	public boolean nextValue(){
		if(mode == DoubleParameter.LIST_MODE && valueList.size() > 0){
			if(valueSetIndex >= valueList.size()){
				return false;
			}
			setValue(valueList.get(valueSetIndex));
			++valueSetIndex;
			return true;
		} else if(mode == DoubleParameter.INCREMENTAL_MODE) {
			if(first){
				first = false;
				setValue(min);
				return true;
			} else {
				if(value + step > max){
					return false;
				}
				setValue(value + step);
				return true;
			}
		}
		return false;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getStep() {
		return step;
	}

	public void setStep(double step) {
		this.step = step;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = precision*value;
		this.value = Math.round(this.value);
		this.value /= precision;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		if(mode == DoubleParameter.LIST_MODE){
			this.mode = mode;
		} else {
			this.mode = DoubleParameter.INCREMENTAL_MODE;
		}
	}
	
	public static <T extends DoubleParameter> void clone(T toCopy, T copy){
		copy.setLabel(toCopy.getLabel());
		copy.setValue(toCopy.getValue());
		copy.setMin(toCopy.getMin());
		copy.setMax(toCopy.getMax());
		copy.setMode(toCopy.getMode());
		copy.setPrecision(toCopy.getPrecision());
		copy.setStep(toCopy.getStep());
		copy.setValueSetIndex(toCopy.getValueSetIndex());
		copy.emptyValueList();
		for(double value : toCopy.getValueList()){
			copy.addValue(value);
		}
	}

	public ArrayList<Double> getValueList() {
		return valueList;
	}

	public void setValueList(ArrayList<Double> valueList) {
		this.valueList = valueList;
	}

	public int getValueSetIndex() {
		return valueSetIndex;
	}

	public void setValueSetIndex(int valueSetIndex) {
		this.valueSetIndex = valueSetIndex;
	}
	
	
}
