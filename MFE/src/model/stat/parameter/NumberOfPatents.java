package model.stat.parameter;

import model.World;

public class NumberOfPatents extends IntParameter {

	public NumberOfPatents() {
		super("Number of patents");
	}

	@Override
	public void applyToWorld(World w) {
		w.setNumberOfPatents(this.getValue());

	}

	@Override
	public Parameter clone() {
		NumberOfPatents p = new NumberOfPatents();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getNumberOfPatents());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
