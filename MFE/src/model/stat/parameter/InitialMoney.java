package model.stat.parameter;

import model.World;

public class InitialMoney extends DoubleParameter {
	
	public InitialMoney(){
		super("Initial agent's money");
	}
	
	public InitialMoney(int min, int max) {
		super("Initial agent's money", min, max);
	}

	@Override
	public void applyToWorld(World w) {
		w.setInitialMoneyAgent(getValue());
	}
	
	@Override
	public InitialMoney clone(){
		InitialMoney p = new InitialMoney();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getInitialMoneyAgent());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}
}
