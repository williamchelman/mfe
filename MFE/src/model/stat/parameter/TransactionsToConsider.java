package model.stat.parameter;

import model.World;

public class TransactionsToConsider extends IntParameter {

	public TransactionsToConsider() {
		super("Last transactions to consider");
	}

	@Override
	public void applyToWorld(World w) {
		w.setNumberOfLastTransactions(this.getValue());
	}

	@Override
	public Parameter clone() {
		TransactionsToConsider p = new TransactionsToConsider();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getNumberOfLastTransactions());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
