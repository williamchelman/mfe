package model.stat.parameter;

import model.World;

public class RandomProduction extends DoubleParameter {

	public RandomProduction() {
		super("production randomness");
	}

	@Override
	public void applyToWorld(World w) {
		w.setPartRandomProduction(this.getValue());
	}
	
	@Override
	public RandomProduction clone(){
		RandomProduction p = new RandomProduction();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getPartRandomProduction());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}
}
