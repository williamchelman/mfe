package model.stat.parameter;

import model.World;

public class InitialProduction extends IntParameter {

	public InitialProduction() {
		super("Initial production");
	}

	@Override
	public void applyToWorld(World w) {
		w.setInitialProduction(this.getValue());
	}

	@Override
	public Parameter clone() {
		InitialProduction p = new InitialProduction();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getInitialProduction());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
