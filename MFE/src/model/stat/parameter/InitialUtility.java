package model.stat.parameter;

import model.World;

public class InitialUtility extends DoubleParameter {
	
	public InitialUtility(){
		super("Initial agent's utility");
	}
	
	public InitialUtility(int min, int max) {
		super("Initial agent's utility", min, max);
	}

	@Override
	public void applyToWorld(World w) {
		w.setInitialUtilityAgent(getValue());
	}
	
	@Override
	public InitialUtility clone(){
		InitialUtility p = new InitialUtility();
		DoubleParameter.clone(this, p);
		return p;
	}
	
	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getInitialUtilityAgent());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}
}
