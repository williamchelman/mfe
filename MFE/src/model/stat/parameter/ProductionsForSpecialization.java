package model.stat.parameter;

import model.World;

public class ProductionsForSpecialization extends IntParameter {

	public ProductionsForSpecialization() {
		super("Productions for Specialization");
	}

	@Override
	public void applyToWorld(World w) {
		w.setSpecializedProduction(this.getValue());
	}

	@Override
	public Parameter clone() {
		ProductionsForSpecialization p = new ProductionsForSpecialization();
		IntParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getSpecializedProduction());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}

}
