package model.stat.parameter;

import model.World;

public class MaximumBenefit extends DoubleParameter {

	public MaximumBenefit() {
		super("maximum benefit");
	}

	@Override
	public void applyToWorld(World w) {
		w.setMaxBenefit((getValue()));
	}
	
	@Override
	public MaximumBenefit clone(){
		MaximumBenefit p = new MaximumBenefit();
		DoubleParameter.clone(this, p);
		return p;
	}

	@Override
	public String getValueFromWorld(World w) {
		return String.valueOf(w.getMaxBenefit());
	}

	@Override
	public boolean applyBeforeInit() {
		return true;
	}
}
