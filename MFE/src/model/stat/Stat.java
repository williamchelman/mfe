package model.stat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import model.output.WorldOutputFactory;
import model.stat.parameter.Parameter;

public class Stat extends Observable implements Observer{
	private int maxIter;
	private int maxStep;
	private long start = 0;
	private int stepsNumber;
	private int totalSteps = -1;
	private int threadNumber;
	private Thread[] threads;
	private StatWorldRunnable[] swrs;
	private WorldOutputFactory wof;
	private ArrayList<Parameter> parameters;
	private boolean finished;
	private boolean stopAll = false;
	
	public Stat(int maxIter, int maxStep, int threadNumber,ArrayList<Parameter> parameters, WorldOutputFactory wof){
		this.maxIter = maxIter;
		this.maxStep = maxStep;
		this.threadNumber = threadNumber;
		this.parameters = parameters;
		this.wof = wof;
	}
	
	private void init(){
		finished = false;
		this.stepsNumber = 0;
		this.totalSteps = -1;
		this.start = Calendar.getInstance().getTimeInMillis();
		threads = new Thread[threadNumber];
		swrs = new StatWorldRunnable[threadNumber];
	}
	
	public void run() {
		init();
		runParameters(0);
		waitForLastThreads();
		finished = true;
		setChanged();
		notifyObservers();
	}
	
	private void waitForLastThreads(){
		while(!stopAll){
			boolean stop = true;
			for(int i = 0 ; i < threads.length ; ++i){
				if(threads[i] != null && threads[i].isAlive()){
					stop = false;
				}
				if(threads[i] != null && !threads[i].isAlive()){
					threads[i] = null;
					swrs[i] = null;
				}
			}
			if(stop){
				break;
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void runParameters(int index){
		Parameter p = parameters.get(index);
		p.init();
		while(p.nextValue()){
			if(index < parameters.size() - 1){
				runParameters(index+1);
			} else {
				this.runIter();
			}
		}
	}
	
	private void runIter() {
		if(this.countObservers() == 0){
			for(Parameter p : parameters){
				System.out.println(p.getLabel() + " : " + p.getStringValue());
			}
			long time = getEstimatedRemainingTime();
			int hours = (int) Math.floor(1.0*time/(1000*60*60));
			int minutes = (int) Math.floor(1.0*time/(1000*60) - 60*hours);
			int seconds = (int) Math.floor(1.0*time/1000 - 60*minutes - 60*60*hours);
			System.out.println("It should take " + hours + "h" + minutes + "m" + seconds + "s");
			System.out.println(getPercentageDone()+"%");
		}
		setChanged();
		notifyObservers();
		launchThread(parameters);
	}
	
	private void launchThread(ArrayList<Parameter> parameters){
		boolean stop = false;
		while(!stop && !stopAll){
			for(int i = 0 ; i < threads.length ; ++i){
				if(threads[i] != null && !stopAll){
					if(!threads[i].isAlive()){
						threads[i] = null;
						swrs[i] = null;
					}
				}
				
				if(threads[i]== null && !stopAll){
					StatWorldRunnable swr = new StatWorldRunnable(maxStep, maxIter,wof);
					swr.init(parameters);
					swr.addObserver(this);
					swrs[i] = swr;
					Thread t = new Thread(swr);
					threads[i] = t;
					t.start();
					stop = true;
					break;
				}
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private long getEllapsedTime(){
		return Calendar.getInstance().getTimeInMillis() - this.start;
	}
	
	public long getEstimatedRemainingTime(){
		long time = 0;
		if(this.stepsNumber != 0 && !finished)
			time = getEllapsedTime()*(getTotalSteps(parameters)-this.stepsNumber)/this.stepsNumber;
		return time;
	}
	
	public double getPercentageDone(){
		if(!finished){
			double res = 10000.0*this.stepsNumber/getTotalSteps(parameters);
			res = Math.round(res);
			res = res/100;
			return res;
		} else {
			return 100;
		}
	}
	
	
	private int getTotalSteps(ArrayList<Parameter> parameters){
		if(totalSteps == -1){
			totalSteps = this.maxIter*this.maxStep;
			for(Parameter p : parameters){
				totalSteps *= p.valueNumber();
			}
		}
		
		return this.totalSteps;
	}

	public boolean isFinished() {
		return finished;
	}

	public void stop() {
		stopAll = true;
		for(StatWorldRunnable runnable : swrs){
			if(runnable != null)
				runnable.setStop(true);
		}
		for(Thread t : threads){
			if(t != null)
				t.interrupt();
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg0 instanceof StatWorldRunnable){
			stepsNumber += maxStep;
			setChanged();
			notifyObservers();
		}	
	}
}
