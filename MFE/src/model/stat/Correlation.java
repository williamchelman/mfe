package model.stat;

import helper.Helper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.output.sql.MYSQL;

public class Correlation {

	public static void main(String[] args) throws SQLException {
		Connection connection = MYSQL.getConnection("nephidream.com", "3306", "tradeoff", "tradeoff", "tradeoffrediar1023");

		//for(int i = 1 ; i <= 20 ; ++i){
		double corr = computeCorrelation(connection);
			System.out.println(corr);
			System.out.println(corr*corr);
		//}
		connection.close();
	}
	
	private static double computeCorrelation(Connection connection) throws SQLException{
		String query = "SELECT ta.utility, ta.money FROM `test_Agents` ta";
		Statement s = connection.createStatement();
		s.execute(query);
		
		ResultSet results = s.getResultSet();
		ArrayList<Double> utilities = new ArrayList<Double>();
		ArrayList<Double> addedValues = new ArrayList<Double>();
		while(results.next()){
			utilities.add(results.getDouble(1));
			addedValues.add(results.getDouble(2));
		}
		results.close();
		
		double corr = computeCovariance(utilities, addedValues)/(getStd(utilities)*getStd(addedValues));
		
		return corr;
	}
	
	private static double computeCovariance(ArrayList<Double> a1, ArrayList<Double> a2){
		double res = 0;
		
		double meanA1 = getMean(a1);
		double meanA2 = getMean(a2);
		
		for(int i = 0 ; i < a1.size() ; ++i){
			res += (a1.get(i)-meanA1) * (a2.get(i)-meanA2);
		}
		
		return res/a1.size();
	}
	
	private static double getMean(ArrayList<Double> a){
		Double[] aArray = new Double[a.size()];
		for(int i = 0 ; i < a.size() ; ++i){
			aArray[i] = a.get(i);
		}
		return Helper.getMean(aArray);
	}
	
	private static double getStd(ArrayList<Double> a){
		Double[] aArray = new Double[a.size()];
		for(int i = 0 ; i < a.size() ; ++i){
			aArray[i] = a.get(i);
		}
		
		return Helper.getStdDev(aArray);
	}
}
