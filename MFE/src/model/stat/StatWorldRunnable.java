package model.stat;

import java.util.ArrayList;
import java.util.Observable;

import model.Agent;
import model.MarketFailingException;
import model.Transaction;
import model.World;
import model.output.WorldOutput;
import model.output.WorldOutputFactory;
import model.stat.parameter.Parameter;

public class StatWorldRunnable extends Observable implements Runnable {
	private int steps;
	private int iterations;
	private ArrayList<Parameter> parameters;
	private boolean stop = false;
	private WorldOutputFactory wof;
	//private static int number = 0;
	
	
	public StatWorldRunnable(int steps, int iterations, WorldOutputFactory wof) {
		super();
		this.steps = steps;
		this.iterations = iterations;
		this.parameters = new ArrayList<Parameter>();
		this.wof = wof;
	}
	
	public void init(ArrayList<Parameter> parameters){
		this.parameters.clear();
		for(Parameter p : parameters){
			this.parameters.add(p.clone());
			//System.out.println(p.getLabel() + " : "+ p.getStringValue());
		}
	}

	@Override
	public void run() {
		/*int myNumber = number;
		++number;*/
		ArrayList<World> worlds = new ArrayList<World>();
		for (int i = 0; i < iterations && !stop; i++) {
			World w = new World("World");
			
			for(Parameter p : parameters){
				if(p.applyBeforeInit())
					p.applyToWorld(w);
			}
			w.init();
			for(Parameter p : parameters){
				if(!p.applyBeforeInit())
					p.applyToWorld(w);
			}
			
			while (w.getCurrentStep() < steps && !stop) {
				try {
					w.run();
				} catch (MarketFailingException e) {
					e.printStackTrace();
					break;
				}
			}
			if(wof != null){
				try {
					WorldOutput wo = wof.createWorldOutput();
					wo.saveWorld(w, steps);
					wo.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			worlds.add(w);
			
			
			setChanged();
			notifyObservers();
		}
	}


	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}
}
