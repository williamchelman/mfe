package model.stat;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;

import model.MarketFailingException;
import model.output.WorldOutputFactory;
import model.output.sql.MYSQLFactory;
import model.stat.parameter.BoolParameter;
import model.stat.parameter.DiminishingMarginaleUtility;
import model.stat.parameter.DoubleParameter;
import model.stat.parameter.Influenceability;
import model.stat.parameter.InitialMoney;
import model.stat.parameter.IntParameter;
import model.stat.parameter.MarketRandomness;
import model.stat.parameter.MinQuality;
import model.stat.parameter.NumberOfAgents;
import model.stat.parameter.NumberOfLowcostAgents;
import model.stat.parameter.NumberOfPatents;
import model.stat.parameter.Parameter;
import model.stat.parameter.TransactionsToConsider;

public class Main {

	public static void main(String[] args) throws InterruptedException, FileNotFoundException, MarketFailingException {
		int iterations = 50;
		int steps = 50000;
		int threads = 6;
		boolean saveHistory = true;
		boolean saveLorentz = true;
		boolean saveAgents = true;
		boolean saveProducts = true;
		boolean saveAgentsProducts = true;
		boolean saveTransactions = true;
		boolean savePatents = true;
		String tableSuffix = "";
		WorldOutputFactory wof = new MYSQLFactory("nephidream.com", "3306", "tradeoff", "tradeoff", "tradeoffrediar1023",tableSuffix,saveHistory,saveLorentz,saveAgents,saveProducts,saveAgentsProducts,saveTransactions,savePatents);
		
		ArrayList<Parameter> parameters = new ArrayList<Parameter>();
		
		DoubleParameter p;
		
		p = new InitialMoney();
		p.setMin(500);
		p.setMax(500);
		p.setStep(480);
		//parameters.add(p);
		
		p = new Influenceability();
		p.setMin(1);
		p.setMax(1);
		p.setStep(1);
		parameters.add(p);
		
		p = new MarketRandomness();
		p.setStep(0.25);
		p.setMin(0.1);
		p.setMax(0.1);
		//parameters.add(p);
		
		IntParameter ip = new NumberOfAgents();
		ip.setMin(20);
		ip.setMax(20);
		ip.setStep(1);
		//parameters.add(ip);
		
		ip = new NumberOfPatents();
		ip.setMin(0);
		ip.setMax(10);
		ip.setStep(1);
		//parameters.add(ip);
		
		ip = new TransactionsToConsider();
		ip.setMin(1);
		ip.setMax(91);
		ip.setStep(10);
		//parameters.add(ip);
		
		ip = new NumberOfLowcostAgents();
		ip.setMin(0);
		ip.setMax(50);
		ip.setStep(5);
		//parameters.add(ip);
		
		p = new MinQuality();
		p.setMin(0.1);
		p.setMax(0.9);
		p.setStep(0.4);
		//parameters.add(p);
		
		BoolParameter bp = new DiminishingMarginaleUtility();
		bp.setValue(true);
		bp.setVariates(false);
		//parameters.add(bp);
		
		long start = Calendar.getInstance().getTimeInMillis();
		Stat s = new Stat(iterations,steps,threads,parameters,wof);
		s.run();
		long finish = Calendar.getInstance().getTimeInMillis();
		System.out.println(finish-start);
	}
}
