package model;

import java.util.ArrayList;

/**
 * The seller part of the agent
 * @author nicolasbernier
 *
 */
public abstract class Seller
{
    protected Agent myAgent;
    protected ArrayList<Integer> productProposed=new ArrayList<Integer>();
    protected int[] productProposedAttemps;

    /**
     * Constructor
     * @param myAgent - the parent agent
     */
    public Seller(Agent myAgent)
    {
        this.myAgent = myAgent;
        this.productProposedAttemps=new int[this.myAgent.getNumberOfProducts()];
        for(int i=0;i<this.productProposedAttemps.length;i++){
        	this.productProposedAttemps[i]=0;
        }
    }
    /**
     * Reinitialize the buyer before a transaction
     */
    public void reinit(){
    	this.productProposed.clear();
    	for(int i=0;i<this.productProposedAttemps.length;i++){
        	this.productProposedAttemps[i]=0;
        }
    }
    /**
     * Select the next product to sell
     * @return - the id of the product
     */
    public abstract int selectProductToSell();
    
    /**
     * Create a selling offer
     * @return - the offer
     */
    public abstract Offer sell();
	/**
	 * @return the productProposedAttemps
	 */
	public int[] getProductProposedAttemps() {
		return productProposedAttemps;
	}
	/**
	 * @return the productProposed
	 */
	public ArrayList<Integer> getProductProposed() {
		return productProposed;
	}
}
