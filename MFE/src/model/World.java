package model;
import helper.Helper;

import java.util.*;

import model.monopoly.PatentOffice;


/**
 * The main component of the model
 * @author nicolasbernier
 *
 */
public class World extends Observable
{   
    /*
     * Public static variables
     */
    public static int MAXNUMBEROFATTEMPTS = 1000; // Before signaling a market failure
    public static int NUM_REPETITION_PRODUCT_SELLER_BUYER=5;//
    public static Random ran = new Random();
    
    //determine the type of market
    private int market_type=1;//1=>Mixed,2=>Competitive,3=>Random
    /*
     * Private variables
     */
    private boolean uptadeView=true;
    private Integer currentStep;
    private Agent[] theAgents = null;
    private Market market = null;
    
    private String name;    
    private int numberOfAgents = 50;
    //for the market
    private int initialProduction = 10; // NUMBER OF INITIAL PRODUCTION STEPS TO BOOT THE MODEL
    private int numberOfProducts = 10;
    private Double maxBenefit = 5.0;
    private int numberOfLastTransactions = 1000; // The transactions on which are based the selection of the next product to produce
    
    //for the agents
    private Double initialMoneyAgent = 500.0; // INITIAL VALUE HB: 500
    private Double initialUtilityAgent = 0.0; // INITIAL VALUE HB: 0
    private int specializedProduction = 100;
    private Double partRandomProduction = 0.0; // 3 times out of 4, the product to produce is selected randomly = Almost pure random production (higher values lead to systematic exceptions in competitive market)
    private Boolean includeStocksInWealth = false;
    private Boolean decreasingUtility = false;
    private Boolean unequalMoney=false;
    
    
    /*
     * History part
     */
    private ArrayList<Double> historyUtility=new ArrayList<Double>();
    private ArrayList<Double> historyMoney=new ArrayList<Double>();
    private ArrayList<Double> historyAddedValue=new ArrayList<Double>();
    private ArrayList<Double> historyWealth=new ArrayList<Double>();
    private ArrayList<Double> historyProductsInStock=new ArrayList<Double>();
    private ArrayList<Double> historyValueProductsInStock=new ArrayList<Double>();
    private ArrayList<Double> historyShareAgentsWithNoMoney=new ArrayList<Double>();
    private ArrayList<Double> historyMarketFailure=new ArrayList<Double>();
    private ArrayList<Double> historyGiniUtility=new ArrayList<Double>();
    private ArrayList<Double> historyGiniAddedValue=new ArrayList<Double>();
    private ArrayList<Double> historyGiniMoney=new ArrayList<Double>();
    private ArrayList<Double> historyGiniWealth=new ArrayList<Double>();
    /*
     * Lorentz part
     */
    private Double[] lorentzWealth;
    private Double[] lorentzMoney;
    private Double[] lorentzAddedValue;
    private Double[] lorentzUtility;
    
    /*
     * Mimicry part
     */
    private Mimicry mimicry;
    
    /*
     * Monopoly part
     */
    private PatentOffice patentOffice;
    
    
    /*
     * Asymetry
     */
    private int numberOfLowcostAgents = 0;
    private double minQuality = 1;
    
    private HashMap<Integer,ArrayList<Double>> mappingIndexGraph=new HashMap<Integer,ArrayList<Double>>();

    //each world has a amount of step to displayed (default=all)
    /**
     * Constructor of the world
     * @param name - the name of the world
     */
    public World(String name)
    {
    	this.name=name;
    	this.currentStep=0;
		//mapping used for an eventual view
		mappingIndexGraph.put(1, historyUtility);
		mappingIndexGraph.put(2, historyMoney);
		mappingIndexGraph.put(3, historyAddedValue);
		mappingIndexGraph.put(4, historyWealth);
		mappingIndexGraph.put(5, historyProductsInStock);
		mappingIndexGraph.put(6, historyValueProductsInStock);
		mappingIndexGraph.put(7, historyShareAgentsWithNoMoney);
		mappingIndexGraph.put(8, historyMarketFailure);
		mappingIndexGraph.put(9, historyGiniUtility);
		mappingIndexGraph.put(10, historyGiniAddedValue);
		mappingIndexGraph.put(11, historyGiniMoney);
		mappingIndexGraph.put(12, historyGiniWealth);
		
		mimicry = new Mimicry();
		patentOffice = new PatentOffice();
    }
    /**
     * Initialize the world
     */
    
    public void init(){
    	this.createAgents();
    	this.initMimicry();
		this.initPatentOffice();
    	this.createMarket(this.market_type);
    }
    
	/**
     * Create the agents of the world
     */
	public void createAgents()
    {
        theAgents = new Agent[this.numberOfAgents];
        for (int i = 0; i < this.numberOfAgents; i++)
        {
            theAgents[i] = new Agent(this.initialMoneyAgent, this.initialUtilityAgent, this.numberOfProducts, this.specializedProduction, this.partRandomProduction, this.includeStocksInWealth,this.decreasingUtility,this.unequalMoney,i,patentOffice);
        }
        
        createLowcostAgents();
        
        this.lorentzUtility=new Double[this.numberOfAgents];
        this.lorentzAddedValue=new Double[this.numberOfAgents];
        this.lorentzMoney=new Double[this.numberOfAgents];
        this.lorentzWealth=new Double[this.numberOfAgents];
       
        
    }
	
	private void createLowcostAgents(){
		numberOfLowcostAgents = Math.min(numberOfAgents, numberOfLowcostAgents);
        int lowcostAgents = 0;
        
        while(lowcostAgents < numberOfLowcostAgents){
        	int index = ran.nextInt(numberOfAgents);
        	Producer p = theAgents[index].getMyProducer();
        	if(p.getQuality() == 1){
        		p.setQuality(minQuality);
        		++lowcostAgents;
        	}
        }
	}
	/**
	 * create the market according to the market 
	 * @param type
	 */
    public void createMarket(int type){
    	switch (type) {
		case 1:
			this.createMixedMarket();
			break;
		case 2:
			this.createCompetitiveMarket();
			break;
		case 3:
			this.createRandomMarket();
			break;

		default:
			this.createMixedMarket();
			break;
		}
    }
    /**
     * Create a purely competitive market
     */
    @SuppressWarnings("deprecation")
    public void createCompetitiveMarket()
    {
        market = new CompetitiveMarket(this.theAgents,this.initialProduction, this.numberOfLastTransactions, this.numberOfProducts,this.maxBenefit);
        for (int i = 0; i < this.numberOfAgents; i++)
        {
            theAgents[i].setTheMarket(market);
        }
        initialProduction();
        ((CompetitiveMarket)market).initialize();
    }
    /**
     * Create a purely random market
     */
    @SuppressWarnings("deprecation")
	public void createRandomMarket()
    {
        market = new RandomMarket(this.theAgents, this.initialProduction, this.numberOfLastTransactions, this.numberOfProducts,this.maxBenefit);
        for (int i = 0; i < this.numberOfAgents; i++)
        {
            theAgents[i].setTheMarket(market);
        }
        initialProduction();
    }
    /**
     * Create a market than can have both a random and a competitive behaviour
     */
    public void createMixedMarket()
    {
        market = new MixedMarket(this.theAgents,this.initialProduction, this.numberOfLastTransactions, this.numberOfProducts,this.maxBenefit);
        for (int i = 0; i < this.numberOfAgents; i++)
        {
            theAgents[i].setTheMarket(market);
        }
        initialProduction();
        ((MixedMarket)this.market).initialize();
    }
    /**
     * Compute the initial production of the market
     */
    public void initialProduction()
    {	
        for (int i = 0; i < this.initialProduction; i++)
        {
            market.onlyProduce();
        }
    }
    
    /**
     * Run one step
     * @throws MarketFailingException
     */
    public void run() throws MarketFailingException// RJ version
    {	
            market.tick(this.currentStep); // Execute one step of the market 
            this.computeGlobalUtility();
            this.computeGlobalMoney();
            this.computeGlobalAddedValue();
            this.computeGlobalWealth();
            this.nbProductsInStocks();
            this.computeValueOfProductsInStocks();
            this.shareAgentsWithNoMoney();
            this.computeMarketFailure();
            this.computeGiniUtility();
            this.computeGiniMoney();
            this.computeGiniAddedValue();
            this.computeGiniWealth();
            //Not displayed
            this.shareAgentsWithNoMoneyAndNoProduct();
            this.getNumberOfMarketFailures();
            this.getLastProduction();
            this.getLastBuyer();
            this.getLastSeller();
            this.getLastPrice();
            this.getLastProductSold();
            //Lorentz
            this.computeLorentzUtility();
            this.computeLorentzAddedValue();
            this.computeLorentzMoney();
            this.computeLorentzWealth();
            
            //Mimicry
            mimicry.tick();
            
            this.currentStep++;
            this.notifyView();
    }
    public void notifyView(){
    	if(this.uptadeView){
    		this.setChanged();
        	this.notifyObservers(this.currentStep);
    	}
    }
    public void notifyFail(){
    	this.setChanged();
    	this.notifyObservers("Market Failure");
    }
    /**
     * Reset the world
     */
    public void reset()
    {
    	this.currentStep=0;
    	
    	this.historyAddedValue.clear();
    	this.historyGiniAddedValue.clear();
    	this.historyGiniMoney.clear();
    	this.historyGiniUtility.clear();
    	this.historyGiniWealth.clear();
    	this.historyMoney.clear();
    	this.historyProductsInStock.clear();
    	this.historyShareAgentsWithNoMoney.clear();
    	this.historyMarketFailure.clear();
    	this.historyUtility.clear();
    	this.historyValueProductsInStock.clear();
    	this.historyWealth.clear();
    	//Lorentz curves
    	this.lorentzAddedValue=Helper.emptyArray(this.lorentzAddedValue);
    	this.lorentzMoney=Helper.emptyArray(this.lorentzMoney);
    	this.lorentzUtility=Helper.emptyArray(this.lorentzUtility);
    	this.lorentzWealth=Helper.emptyArray(this.lorentzWealth);
    	
        for (int i = 0; i < this.numberOfAgents; i++)
        {
            theAgents[i].agentReset();
        }
        //reset but keep the randomness chosses
        Double val=this.market.getRandomness();
		this.createMarket(this.market_type);
		this.market.setRandomness(val);
		this.notifyView();
    }
    /**
     * Compute the global utility of the market at a given step
     * @return the utility
     */
    public Double computeGlobalUtility()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            total += ag.getUtility();
        }
        this.historyUtility.add(total);
        return total;
    }
    /**
     * Compute the global money of the market at a given step
     * @return the money
     */
    public Double computeGlobalMoney()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            total += ag.getMoney();
        }
        this.historyMoney.add(total);
        return total;
    }
    /**
     * Compute the global value of the product in stocks of the market at a given step
     * @return the value of the product in stocks
     */
    public Double computeValueOfProductsInStocks()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            total += ag.getMyProducer().computeWealth();
        }
        this.historyValueProductsInStock.add(total);
        return total;
    }
    /**
     * Compute the global wealth of the market at a given step
     * @return the wealth
     */
    public Double computeGlobalWealth()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            ag.computeWealth();
            total += ag.getWealth();
        }
        this.historyWealth.add(total);
        return total;
    }
    /**
     * Compute the global added value of the market at a given step
     * @return the added value
     */
    public Double computeGlobalAddedValue()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            total += ag.getAddedValue();
        }
        this.historyAddedValue.add(total);
        return total;
    }
    
    /*
     * Gini key metrics
     */
    /**
     * Compute the Gini coefficient of utility of the market at a given step
     * @return the Gini coefficient
     */
    public Double computeGiniUtility()
    {
        Double gini = 0.0;
        //Double total = this.computeGlobalUtility();
        //get last total
        if(this.historyUtility.size()>0){
	        Double total = this.historyUtility.get(this.historyUtility.size()-1);
	        Agent[] agentCopy= Arrays.copyOf(this.theAgents, this.numberOfAgents);
	        Arrays.sort(agentCopy, Agent.sortUtilityAscending);
	
	        Double share = 1.0 / this.numberOfAgents;
	        Double cumul = 0.0;
	        Double a = 0.0;
	        Double prevCumul = 0.0;
	
	        for (Agent ag : agentCopy)
	        {
	            cumul += ag.getUtility() / total;
	            a = prevCumul + cumul;
	            prevCumul = cumul;
	            gini += a * share;
	        }
	        
	        gini=Math.max(gini,0);
	        gini=Math.min(gini,1);
	        Double value=Math.abs(1 - gini);
	        this.historyGiniUtility.add(value);
	        return value;
        }
        return 0.0;
    }
    /**
     * Compute the Gini coefficient of money of the market at a given step
     * @return the Gini coefficient
     */
    public Double computeGiniMoney()
    {
    	if(this.historyMoney.size()>0){
	        Double gini = 0.0;
	        //Double total = this.computeGlobalMoney();
	        Double total = this.historyMoney.get(this.historyMoney.size()-1);
	
	        Agent[] agentCopy= Arrays.copyOf(this.theAgents, this.numberOfAgents);
	        Arrays.sort(agentCopy, Agent.sortMoneyAscending);
	
	        Double share = 1.0 / this.numberOfAgents;
	        Double cumul = 0.0;
	        Double a = 0.0;
	        Double prevCumul = 0.0;
	
	        for (Agent ag : agentCopy)
	        {
	            cumul += ag.getMoney() / total;
	            a = prevCumul + cumul;
	            prevCumul = cumul;
	            gini += a * share;
	        }
	        gini=Math.max(gini,0);
	        gini=Math.min(gini,1);
	        Double value=Math.abs(1 - gini);
	        this.historyGiniMoney.add(value);
	        
	        return value;
    	}
    	return 0.0;

    }
    /**
     * Compute the Gini coefficient of added value of the market at a given step
     * @return the Gini coefficient
     */
    public Double computeGiniAddedValue()
    {
    	if(this.historyAddedValue.size()>0){
	        Double gini = 0.0;
	        //Double total = this.computeGlobalAddedValue();
	        Double total = this.historyAddedValue.get(this.historyAddedValue.size()-1);
	
	        Agent[] agentCopy= Arrays.copyOf(this.theAgents, this.numberOfAgents);
	
	        Arrays.sort(agentCopy, Agent.sortAddedValueAscending);
	
	        Double share = 1.0 / this.numberOfAgents;
	        Double cumul = 0.0;
	        Double a = 0.0;
	        Double prevCumul = 0.0;
	        
	        for (Agent ag : agentCopy)
	        {
	            cumul += ag.getAddedValue() / total;
	            a = prevCumul + cumul;
	            prevCumul = cumul;
	            gini += a * share;
	        }
	        gini=Math.max(gini,0);
	        gini=Math.min(gini,1);
	        Double value=Math.abs(1 - gini);
	        this.historyGiniAddedValue.add(value);
	        return value;
	    }
		return 0.0;
    }
    /**
     * Compute the Gini coefficient of wealth of the market at a given step
     * @return the Gini coefficient
     */
    public Double computeGiniWealth()
    {
        Double gini = 0.0;
        Double total = 0.0;
        Agent[] agentCopy= Arrays.copyOf(this.theAgents, this.numberOfAgents);

        Arrays.sort(agentCopy, Agent.sortWealthAscending);

        for (Agent ag : agentCopy)
        {
            ag.computeWealth();
            total += ag.getWealth();
        }

        Double share = 1.0 / this.numberOfAgents;
        Double cumul = 0.0;
        Double a = 0.0;
        Double prevCumul = 0.0;

        for (Agent ag : agentCopy)
        {
            cumul += ag.getWealth() / total;
            a = prevCumul + cumul;
            prevCumul = cumul;
            gini += a * share;
        }
        gini=Math.max(gini,0);
        gini=Math.min(gini,1);
        Double value=Math.abs(1 - gini);
        this.historyGiniWealth.add(value);
        return value;

    }
    /**
     * The the copy of the agents
     * @return
     */
    public Agent[] getAgentsCopy(){
    	return Arrays.copyOf(this.theAgents, this.numberOfAgents);
    }
    /*
     * Lorentz curves
     */
    /**
     * Compute the Lorentz curve of the distribution of the utility among the agents
     */
    public void computeLorentzUtility(){
    	Agent[] agents=this.getAgentsCopy();
    	Arrays.sort(agents, Agent.sortUtilityAscending);
    	double totalUtility=0.0;
    	for(int i=0;i<agents.length;i++){
    		totalUtility+=agents[i].getUtility();
    	}
    	double value=0.0;
    	for(int i=0;i<agents.length;i++){
    		value+=agents[i].getUtility();
    		this.lorentzUtility[i]=(value/totalUtility);
    	}
    }
    
    /**
     * Compute the Lorentz curve of the distribution of the Wealth among the agents
     */
    public void computeLorentzWealth(){
    	Agent[] agents=this.getAgentsCopy();
    	Arrays.sort(agents, Agent.sortWealthAscending);
    	double totalWealth=0.0;
    	for(int i=0;i<agents.length;i++){
    		totalWealth+=agents[i].getWealth();
    	}
    	double value=0.0;
    	for(int i=0;i<agents.length;i++){
    		value+=agents[i].getWealth();
    		this.lorentzWealth[i]=(value/totalWealth);
    	}
    }
    
    /**
     * Compute the Lorentz curve of the distribution of the added Value among the agents
     */
    public void computeLorentzAddedValue(){
    	Agent[] agents=this.getAgentsCopy();
    	Arrays.sort(agents, Agent.sortAddedValueAscending);
    	double totalAddedValue=0.0;
    	for(int i=0;i<agents.length;i++){
    		totalAddedValue+=agents[i].getAddedValue();
    	}
    	double value=0.0;
    	for(int i=0;i<agents.length;i++){
    		value+=agents[i].getAddedValue();
    		this.lorentzAddedValue[i]=(value/totalAddedValue);
    	}
    }
    
    /**
     * Compute the Lorentz curve of the distribution of the money among the agents
     */
    public void computeLorentzMoney(){
    	Agent[] agents=this.getAgentsCopy();
    	Arrays.sort(agents, Agent.sortMoneyAscending);
    	double totalMoney=0.0;
    	for(int i=0;i<agents.length;i++){
    		totalMoney+=agents[i].getMoney();
    	}
    	double value=0.0;
    	for(int i=0;i<agents.length;i++){
    		value+=agents[i].getMoney();
    		this.lorentzMoney[i]=(value/totalMoney);
    	}
    }
    /**
     * Return the Lorentz curve
     * @param index - the index of graph
     * @return the array of values corresponding to the index
     */
    public Double[] getLorentz(int index){
    	switch (index) {
		case 13:
			return this.lorentzUtility;
		case 14:
			return this.lorentzAddedValue;
		case 15:
			return this.lorentzMoney;
		case 16:
			return this.lorentzWealth;
		default:
			return this.lorentzUtility;
		}
    }
    /*
     * Other values
     */
    /**
     * Compute the proportion of agent with no money
     * @return the proportion
     */
    public Double shareAgentsWithNoMoney() // Proportion of broke agents (collapse agents)
    {
        Double total = 0.0;
        for (Agent ag : this.theAgents)

        {
            if (ag.iAmBroke())
                total++;
        }
        Double value=total / theAgents.length;
        this.historyShareAgentsWithNoMoney.add(value);
        return value;
    }
    /**
     * Compute the proportion of agent with no money and no product
     * @return the proportion
     */
    public Double shareAgentsWithNoMoneyAndNoProduct()
    {
        Double total = 0.0;
        for (Agent ag : this.theAgents)

        {
            if (ag.iAmBroke() & ag.getMyProducer().getNumberOfProducts() <= 0)
                total++;
        }
        Double value=total / theAgents.length;
        return value;
    }
    /**
     * Add the current number of market failure to the historic of market failure
     */
    public void computeMarketFailure(){
    	historyMarketFailure.add((double)this.market.getNumberOfMarketFailures());
    }
    /**
     * Compute the number of products in stocks
     * @return the number of products
     */
    public Double nbProductsInStocks()
    {
        Double total = 0.0;
        for (Agent ag : theAgents)
        {
            total += ag.getMyProducer().getNumberOfProducts();
        }
        this.historyProductsInStock.add(total);
        return total;
    }
    /**
     * Create a random value included between two bounds 
     * @param min - the lower bound
     * @param max - the upper bound
     * @return the random value
     */
    public static Double getRandomNumber(Double min, Double max)
    {
        return (ran.nextDouble() * (max - min) + min);
    }
    public void setUpdateView(boolean value){
    	this.uptadeView=value;
    }
    
    private void initMimicry(){
    	mimicry.init(theAgents);
    }
    
    private void initPatentOffice() {
		patentOffice.init(theAgents);
	}
    
    /*
     * Other getter/setter
     */
    /**
     * @return market_type
     */
    public int getMarket_type() {
		return market_type;
	}

    /**
     * Set the market type
     * @param market_type - the market type
     */
	public void setMarket_type(int market_type) {
		this.market_type = market_type;
	}


	/**
	 * @return theAgents
	 */
	public Agent[] getTheAgents() {
		return theAgents;
	}


	/**
     * Set the agents of the world
     * @param theAgents - the agents
     */
	public void setTheAgents(Agent[] theAgents) {
		this.theAgents = theAgents;
	}


	/**
	 * @return initialMoneyAgent
	 */
	public Double getInitialMoneyAgent() {
		return initialMoneyAgent;
	}


	/**
	 * Set the initial money of the agent
	 * @param initialMoneyAgent - the initial money
	 */
	public void setInitialMoneyAgent(Double initialMoneyAgent) {
		this.initialMoneyAgent = initialMoneyAgent;
	}


	/**
	 * @return initialUtilityAgent
	 */
	public Double getInitialUtilityAgent() {
		return initialUtilityAgent;
	}


	/**
	 * Set the initial utility of the agent
	 * @param initialUtilityAgent - the initial utility
	 */
	public void setInitialUtilityAgent(Double initialUtilityAgent) {
		this.initialUtilityAgent = initialUtilityAgent;
	}

	/**
	 * @return initialProduction
	 */
	public int getInitialProduction() {
		return initialProduction;
	}


	/**
	 * Set the amount initial productions of the market
	 * @param initialProduction - the amount of initial productions
	 */
	public void setInitialProduction(int initialProduction) {
		this.initialProduction = initialProduction;
	}


	/**
	 * @return numberOfProducts
	 */
	public int getNumberOfProducts() {
		return numberOfProducts;
	}


	/**
	 * Set the number of products in the market 
	 * @param numberOfProducts - the number of products
	 */
	public void setNumberOfProducts(int numberOfProducts) {
		this.numberOfProducts = numberOfProducts;
	}


	/**
	 * @return numberOfAgents
	 */
	public int getNumberOfAgents() {
		return numberOfAgents;
	}


	/**
	 * Set the number of agents in the market
	 * @param numberOfAgents - the number of agents
	 */
	public void setNumberOfAgents(int numberOfAgents) {
		this.numberOfAgents = numberOfAgents;
	}


	/**
	 * @return specializedProduction
	 */
	public int getSpecializedProduction() {
		return specializedProduction;
	}


	/**
	 * Set the amount of production for specialization
	 * @param specializedProduction - the amount of production
	 */
	public void setSpecializedProduction(int specializedProduction) {
		this.specializedProduction = specializedProduction;
	}

	/**
	 * @return the partRandomProduction
	 */
	public Double getPartRandomProduction() {
		return partRandomProduction;
	}

	/**
	 * Set the part of random production
	 * @param partRandomProduction - the part of random production
	 */
	public void setPartRandomProduction(Double partRandomProduction) {
		this.partRandomProduction = partRandomProduction;
	}

	/**
	 * @return numberOfLastTransactions
	 */
	public int getNumberOfLastTransactions() {
		return numberOfLastTransactions;
	}

	/**
	 * Set the number of last transactions to consider
	 * @param numberOfLastTransactions - the number of last transactions
	 */
	public void setNumberOfLastTransactions(int numberOfLastTransactions) {
	
		this.numberOfLastTransactions = numberOfLastTransactions;
	}

	/**
	 * @return the maximum possible benefit for a transaction
	 */
	public Double getMaxBenefit() {
		return maxBenefit;
	}
	
	/**
	 * Set the maximum possible 
	 * @param maxBenefit - the maximum possible benefit
	 */
	public void setMaxBenefit(Double maxBenefit) {
		this.maxBenefit = maxBenefit;
	}

	/**
	 * @return includeStocksInWealth
	 */
	public Boolean getIncludeStocksInWealth() {
		return includeStocksInWealth;
	}

	/**
	 * set if the stocks should be included in the wealth or not
	 * @param inclideStocksInWealth - the boolean controlling that
	 */
	public void setIncludeStocksInWealth(Boolean inclideStocksInWealth) {
		this.includeStocksInWealth = inclideStocksInWealth;
	}
		
	/**
	 * @return the currentStep
	 */
	public Integer getCurrentStep(){
		return this.currentStep;
	}

	/**
	 * set the current step
	 * @param currentStep - the current step
	 */
	public void setCurrentStep(Integer currentStep) {
		this.currentStep = currentStep;
	}
	

	/*
     * Market related getter/setter
     */
    public int getNumberOfMarketFailures()
    {
        return market.getNumberOfMarketFailures();
    }

    public int getNumberOfTransactions()
    {
        return market.getNumberOfTransactions();
    }

    public int getLastProduction()
    {
        return market.getLastProduction();
    }


    public int[] getProductions()
    {
        return market.getProductions();
    }

    public int[] getProductsSold()
    {
        return market.getProductsSold();
    }

    public int getLastBuyer()
    {
        return market.getLastBuyer();
    }

    public int getLastSeller()
    {
        return market.getLastSeller();
    }

    public Double getLastPrice()
    {
        return market.getLastPrice();
    }

    public int getLastProductSold()
    {
        return market.getLastProductSold();
    }
    /*
     * Part RandomMarket
     */
    public void setPartRandomMarket(Double value)
    {
        this.market.setRandomness(value);
    }
    public Double getPartRandomMarket()
    {
        return this.market.getRandomness();
    }



    /**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the decreasingUtility
	 */
	public Boolean getDecreasingUtility() {
		return decreasingUtility;
	}
	/**
	 * @param decreasingUtility the decreasingUtility to set
	 */
	public void setDecreasingUtility(Boolean decreasingUtility) {
		this.decreasingUtility = decreasingUtility;
	}
	/**
	 * @return the numStepToDisplay
	 */

	/*
	 * Getter/setter historical
	 */
	
	/**
	 * @return the historyUtility
	 */
	public ArrayList<Double> getHistoryUtility() {
		return historyUtility;
	}
	/**
	 * @param historyUtility the historyUtility to set
	 */
	public void setHistoryUtility(ArrayList<Double> historyUtility) {
		this.historyUtility = historyUtility;
	}
	/**
	 * @return the historyMoney
	 */
	public ArrayList<Double> getHistoryMoney() {
		return historyMoney;
	}
	/**
	 * @param historyMoney the historyMoney to set
	 */
	public void setHistoryMoney(ArrayList<Double> historyMoney) {
		this.historyMoney = historyMoney;
	}
	/**
	 * @return the historyAddedValue
	 */
	public ArrayList<Double> getHistoryAddedValue() {
		return historyAddedValue;
	}
	/**
	 * @param historyAddedValue the historyAddedValue to set
	 */
	public void setHistoryAddedValue(ArrayList<Double> historyAddedValue) {
		this.historyAddedValue = historyAddedValue;
	}
	/**
	 * @return the historyWealth
	 */
	public ArrayList<Double> getHistoryWealth() {
		return historyWealth;
	}
	/**
	 * @param historyWealth the historyWealth to set
	 */
	public void setHistoryWealth(ArrayList<Double> historyWealth) {
		this.historyWealth = historyWealth;
	}
	/**
	 * @return the historyProductsInStock
	 */
	public ArrayList<Double> getHistoryProductsInStock() {
		return historyProductsInStock;
	}
	/**
	 * @param historyProductsInStock the historyProductsInStock to set
	 */
	public void setHistoryProductsInStock(ArrayList<Double> historyProductsInStock) {
		this.historyProductsInStock = historyProductsInStock;
	}
	/**
	 * @return the historyValueProductsInStock
	 */
	public ArrayList<Double> getHistoryValueProductsInStock() {
		return historyValueProductsInStock;
	}
	/**
	 * @param historyValueProductsInStock the historyValueProductsInStock to set
	 */
	public void setHistoryValueProductsInStock(
			ArrayList<Double> historyValueProductsInStock) {
		this.historyValueProductsInStock = historyValueProductsInStock;
	}
	/**
	 * @return the historyShareAgentsWithNoMoney
	 */
	public ArrayList<Double> getHistoryShareAgentsWithNoMoney() {
		return historyShareAgentsWithNoMoney;
	}
	/**
	 * @param historyShareAgentsWithNoMoney the historyShareAgentsWithNoMoney to set
	 */
	public void setHistoryShareAgentsWithNoMoney(
			ArrayList<Double> historyShareAgentsWithNoMoney) {
		this.historyShareAgentsWithNoMoney = historyShareAgentsWithNoMoney;
	}
	/**
	 * @return the historyMarketFailure
	 */
	public ArrayList<Double> getHistoryMarketFailure() {
		return historyMarketFailure;
	}
	/**
	 * @param historyMarketFailure the historyMarketFailure to set
	 */
	public void setHistoryMarketFailure(ArrayList<Double> historyMarketFailure) {
		this.historyMarketFailure = historyMarketFailure;
	}
	/**
	 * @return the historyGiniUtility
	 */
	public ArrayList<Double> getHistoryGiniUtility() {
		return historyGiniUtility;
	}
	/**
	 * @param historyGiniUtility the historyGiniUtility to set
	 */
	public void setHistoryGiniUtility(ArrayList<Double> historyGiniUtility) {
		this.historyGiniUtility = historyGiniUtility;
	}
	/**
	 * @return the historyGiniAddedValue
	 */
	public ArrayList<Double> getHistoryGiniAddedValue() {
		return historyGiniAddedValue;
	}
	/**
	 * @param historyGiniAddedValue the historyGiniAddedValue to set
	 */
	public void setHistoryGiniAddedValue(ArrayList<Double> historyGiniAddedValue) {
		this.historyGiniAddedValue = historyGiniAddedValue;
	}
	/**
	 * @return the historyGiniMoney
	 */
	public ArrayList<Double> getHistoryGiniMoney() {
		return historyGiniMoney;
	}
	/**
	 * @param historyGiniMoney the historyGiniMoney to set
	 */
	public void setHistoryGiniMoney(ArrayList<Double> historyGiniMoney) {
		this.historyGiniMoney = historyGiniMoney;
	}
	/**
	 * @return the historyGiniWealth
	 */
	public ArrayList<Double> getHistoryGiniWealth() {
		return historyGiniWealth;
	}
	/**
	 * @param historyGiniWealth the historyGiniWealth to set
	 */
	public void setHistoryGiniWealth(ArrayList<Double> historyGiniWealth) {
		this.historyGiniWealth = historyGiniWealth;
	}
	/**
	 * @return the lorentzWealth
	 */
	public Double[] getLorentzWealth() {
		return lorentzWealth;
	}
	/**
	 * @param lorentzWealth the lorentzWealth to set
	 */
	public void setLorentzWealth(Double[] lorentzWealth) {
		this.lorentzWealth = lorentzWealth;
	}
	/**
	 * @return the lorentzMoney
	 */
	public Double[] getLorentzMoney() {
		return lorentzMoney;
	}
	/**
	 * @param lorentzMoney the lorentzMoney to set
	 */
	public void setLorentzMoney(Double[] lorentzMoney) {
		this.lorentzMoney = lorentzMoney;
	}
	/**
	 * @return the lorentzAddedValue
	 */
	public Double[] getLorentzAddedValue() {
		return lorentzAddedValue;
	}
	/**
	 * @param lorentzAddedValue the lorentzAddedValue to set
	 */
	public void setLorentzAddedValue(Double[] lorentzAddedValue) {
		this.lorentzAddedValue = lorentzAddedValue;
	}
	/**
	 * @return the lorentzUtility
	 */
	public Double[] getLorentzUtility() {
		return lorentzUtility;
	}
	/**
	 * @param lorentzUtility the lorentzUtility to set
	 */
	public void setLorentzUtility(Double[] lorentzUtility) {
		this.lorentzUtility = lorentzUtility;
	}
	/**
	 * @return the mappingIndexGraph
	 */
	public HashMap<Integer, ArrayList<Double>> getMappingIndexGraph() {
		return mappingIndexGraph;
	}
	/**
	 * @param mappingIndexGraph the mappingIndexGraph to set
	 */
	public void setMappingIndexGraph(
			HashMap<Integer, ArrayList<Double>> mappingIndexGraph) {
		this.mappingIndexGraph = mappingIndexGraph;
	}
	
	public double getInfluenceability(){
		return mimicry.getInfluenceability();
	}
	
	public void setInfluenceability(double influenceability){
		mimicry.setInfluenceability(influenceability);
	}
	
	public int getNumberOfPatents(){
		return patentOffice.getNumberOfPatents();
	}
	
	public void setNumberOfPatents(int numberOfPatents){
		patentOffice.setNumberOfPatents(numberOfPatents);
	}
	
	public Market getMarket() {
		return market;
	}
	
	public int getAgentIndex(Agent a){
		for(int i = 0 ; i < theAgents.length ; ++i){
			if(theAgents[i].equals(a)){
				return i;
			}
		}
		
		return -1;
	}
	public PatentOffice getPatentOffice() {
		return patentOffice;
	}
	public void setPatentOffice(PatentOffice patentOffice) {
		this.patentOffice = patentOffice;
	}
	
	public int getNumberOfLowcostAgents() {
		return numberOfLowcostAgents;
	}
	public void setNumberOfLowcostAgents(int numberOfLowcostAgents) {
		this.numberOfLowcostAgents = numberOfLowcostAgents;
	}
	public double getMinQuality() {
		return minQuality;
	}
	public void setMinQuality(double minQuality) {
		this.minQuality = minQuality;
	}
}
