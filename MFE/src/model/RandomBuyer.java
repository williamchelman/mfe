package model;


/**
 * The random buyer part of the agent
 * @author nicolasbernier
 *
 */
class RandomBuyer extends Buyer
{
	/**
	 * Constructor of the random buyer
	 * @param myAgent - the parent agent
	 */
    public RandomBuyer(Agent myAgent){
        super(myAgent);
    }

  
	@Override
	public Offer buy(Offer sellingOffer) {
		myAgent.incrementBuyingChances();
		double reservePrice=myAgent.getMyConsumer().getMyTastes()[sellingOffer.getTheProduct()] * myAgent.getMoney() * myAgent.getTheMarket().getTimeIndex();
        if (reservePrice >= sellingOffer.getPrice()) // Budget Constraint
        {
            double price = World.getRandomNumber(sellingOffer.getPrice(),reservePrice);
            return new Offer(sellingOffer.getTheProduct(), myAgent, price, Direction.buy);
        }
        else
        {
            myAgent.frustrate();
        }
        return null;
	}


	@Override
	public int selectProductToBuy() {
		return 0;
	}

}