package model;
import java.util.ArrayList;
import java.util.Comparator;

import model.monopoly.PatentOffice;

/**
 * Agent of the model
 * @author nicolasbernier
 *
 */
public class Agent
{
	private int index;
	
	private Double money;// The money decreasing with production and buying, increasing with selling
	private Double initialMoney;//Loging the initial value when resetting
	private Double maxMoney;
    private Double utility;  // The utility increasing with consumption
    private Double initialUtility; //Initial value of utility (used for reset)
    private int numberOfProducts;
    private int specializedProduction;
    private double partRandomProduction;
    private boolean includeStocksInWealth,decreasingUtility,unequalMoney;
    
    protected Double wealth; // the sum of all belongings: Stocks, moneys and utilities
    private Double addedValue = 0.0;
    private int frustration = 0;// +1 each time the agent can't enter an auction due to his budget constraint
    private int[] numberOfProductsSold;
    private int[] numberOfProductsBought;
    private int[] numberOfLowcostProductsBought;
    private int buyingChances = 0;// +1 each time the agent get a chance to buy a product
    private int sellingChances = 0;//+1 each time the agent get a chance to sell a product
    protected Consumer myConsumer;
    protected Producer myProducer;
    
    protected Buyer randomBuyer,competitiveBuyer;
    protected Seller randomSeller,competitiveSeller;
    
    protected Market theMarket;
    
    /**
     * Constructor
     * @param money - initial money
     * @param utility - initial utility
     * @param numberOfProducts - the number of products in the market
     * @param specializedProduction - the number of production for specialization
     * @param partRandomProduction - the part of random production
     * @param includeStocksInWealth - include stocks in wealth or not
     * @param decreasingUtility - use the principle of diminishing marginal utility or not
     * @param unequalMoney - use unequal money or not
     */
    public Agent(Double money,Double utility,int numberOfProducts,int specializedProduction,double partRandomProduction,boolean includeStocksInWealth,boolean decreasingUtility,boolean unequalMoney,int index, PatentOffice patentOffice)
    {
    	this.index = index;
    	this.unequalMoney=unequalMoney;
        this.maxMoney = money;
        this.money=computeInitialMoney();
        this.utility = utility; 
        this.initialUtility=utility;
        
        this.numberOfProducts=numberOfProducts;
        numberOfProductsSold = new int[numberOfProducts];
        numberOfProductsBought = new int[numberOfProducts];
        numberOfLowcostProductsBought = new int[numberOfProducts];
        this.specializedProduction=specializedProduction;
        this.partRandomProduction=partRandomProduction;
        this.includeStocksInWealth=includeStocksInWealth;
        this.decreasingUtility=decreasingUtility;
        //An agent is both a consumer and a producer
        myConsumer = new Consumer(this);
        myProducer = new Producer(this,patentOffice);
        this.initBuyerSeller();
    }
    /**
     * Reinitialize competitive seller and buyer
     */
    public void reinit(){
    	this.competitiveBuyer.reinit();
    	this.competitiveSeller.reinit();
    }
    /**
     * Compute initial money of the agent
     * @return
     */
    private Double computeInitialMoney() {
		if(this.unequalMoney)
			this.initialMoney=World.getRandomNumber(this.maxMoney/10, this.maxMoney); // initial random money
		else
			this.initialMoney=this.maxMoney;
		return this.initialMoney;
	}
    /**
     * Init the buyer and the seller part of the agent
     */
	private void initBuyerSeller() {
    	this.randomBuyer=new RandomBuyer(this);
        this.randomSeller=new RandomSeller(this);
        this.competitiveBuyer=new CompetitiveBuyer(this);
        this.competitiveSeller=new CompetitiveSeller(this);
	}
	/*
     * Sorting functions
     */
    /**
     * Sort Agent by Utility
     * @author nicolasbernier
     *
     */
    private static class sortUtilityAscending implements Comparator<Agent> // Sort the agents by utility
    {
        @Override
		public int compare(Agent a1, Agent a2) {
        	return a1.getUtility().compareTo(a2.getUtility());
		}
    }
    
    
    public static Comparator<Agent> sortUtilityAscending = new sortUtilityAscending(); // Sort the agents by utility
 	
    /**
     * Sort Agent by Money
     * @author nicolasbernier
     *
     */
    private static class sortMoneyAscending implements Comparator<Agent> // Sort the agents by utility
    {
        @Override
		public int compare(Agent a1, Agent a2) {
        	return a1.getMoney().compareTo(a2.getMoney());
		}
    }
    public static Comparator<Agent> sortMoneyAscending= new sortMoneyAscending(); // Sort the agents by utility

    /**
     * Sort Agent by Added Value
     * @author nicolasbernier
     *
     */
    private static class sortAddedValueAscending implements Comparator<Agent> // Sort the agents by utility
    {
        @Override
		public int compare(Agent a1, Agent a2) {
        	return a1.getAddedValue().compareTo(a2.getAddedValue());
		}
    }
    public static Comparator<Agent> sortAddedValueAscending=new sortAddedValueAscending(); // Sort the agents by utility

    /**
     * Sort Agent by Wealth
     * @author nicolasbernier
     *
     */
    private static class sortWealthAscending implements Comparator<Agent> // Sort the agents by utility
    {
        @Override
		public int compare(Agent a1, Agent a2) {
        	return a1.wealth.compareTo(a2.wealth);
		}
    }
    public static Comparator<Agent> sortWealthAscending= new sortWealthAscending(); // Sort the agents by utility

    /*
     * Other functions
     */
    /**
     * return if the agent is broken or not
     * @return
     */
    public Boolean iAmBroke()
    {
        Double minProdCost = 1.0;
        for (int i = 0; i < myProducer.getMySkills().length; i++)
        {
            if (myProducer.getMySkills()[i] < minProdCost)
                minProdCost = myProducer.getMySkills()[i];
        }
        if (money < minProdCost)
            return true;
        else
            return false; // The agent can't produce anymore. He is broken !!
    }
    /**
     * Compute the wealth of the agent  
     * @return
     */
	public Double computeWealth() // Global wealth: money + utility (+ stock)
    {
        if (this.includeStocksInWealth)
            wealth = money + utility + myProducer.computeWealth();
        else
            wealth = money + utility;
        return wealth;
    }
	/**
	 * Reset an agent
	 */
    public void agentReset()
    {
    	this.money = computeInitialMoney();
        this.utility = this.initialUtility;
        this.addedValue = 0.0;
        this.frustration = 0;
        for(int i = 0 ; i < getNumberOfProducts() ; ++i){
        	this.numberOfProductsBought[i] = 0;
        	this.numberOfProductsSold[i] = 0;
        	this.numberOfLowcostProductsBought[i] = 0;
        }
        this.buyingChances = 0;
        this.sellingChances = 0;
        this.initBuyerSeller();
        this.myProducer.reset();
        this.myConsumer.reset();
    }
    

    /**
     * Spend money
     * @param amount - the amount of money spent
     */
    public void spend(Double amount)
    {
        money -= amount;
    }
    /**
     * Earn money
     * @param amount - the amount of money earned
     */
    public void earn(Double amount)
    {
        money += amount;
    }
    
    /**
     * Increase the utility of the agent
     * @param amount - the amount of utility earned
     */
    public void increaseUtility(Double amount)
    {
        utility += amount;
    }
    /**
     * Try to produce a product
     * @return if the production occured or not
     */
    public Boolean actProduce()
    {
    	Boolean done = false;
        if (this.money > 0) // Production is possible if a minimum of money
        {
            done = myProducer.produce();
            this.computeWealth();
        }
        return done;
    }
    /**
     * Consume a product
     * @param product - the product consumed
     * @param quality 
     */
    public void actConsume(int product, double quality)
    {
    	if(quality != 1){
    		numberOfLowcostProductsBought[product]++;
    	}
        myConsumer.consume(product,quality);
    }
    /**
     * Compute added value of the product sold
     * @param productSold - the product sold
     * @param price - the price of the sale
     */
    public void computeAddedValue(int productSold, Double price)
    {
        addedValue += price - myProducer.getProductionCost(productSold);
    }
    /**
     * Increment selling chances
     */
    public void incrementSellingChances()
    {
        this.sellingChances++;
    }
    /**
     * Increment buying chances
     */
    public void incrementBuyingChances()
    {
        this.buyingChances++;
    }
    /**
     * Increment the frustration
     */
    public void frustrate()
    {
        frustration++;
    }
    /**
     * Increment the amount of product sold
     */
    public void sold(int product)
    {
        numberOfProductsSold[product]++;
    }
    /**
     * Increment the amount of product bought
     */
    public void bought(int product)
    {
        numberOfProductsBought[product]++;
    }

	/**
	 * @return the money
	 */
	public Double getMoney() {
		return money;
	}

	/**
	 * @param money the money to set
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

	/**
	 * @return the initialMoney
	 */
	public Double getInitialMoney() {
		return initialMoney;
	}

	/**
	 * @param initialMoney the initialMoney to set
	 */
	public void setInitialMoney(Double initialMoney) {
		this.initialMoney = initialMoney;
	}

	/**
	 * @return the utility
	 */
	public Double getUtility() {
		return utility;
	}

	/**
	 * @param utility the utility to set
	 */
	public void setUtility(Double utility) {
		this.utility = utility;
	}

	/**
	 * @return the wealth
	 */
	public Double getWealth() {
		return wealth;
	}

	/**
	 * @param wealth the wealth to set
	 */
	public void setWealth(Double wealth) {
		this.wealth = wealth;
	}

	/**
	 * @return the addedValue
	 */
	public Double getAddedValue() {
		return addedValue;
	}

	/**
	 * @param addedValue the addedValue to set
	 */
	public void setAddedValue(Double addedValue) {
		this.addedValue = addedValue;
	}

	/**
	 * @return the frustration
	 */
	public int getFrustration() {
		return frustration;
	}

	/**
	 * @param frustration the frustration to set
	 */
	public void setFrustration(int frustration) {
		this.frustration = frustration;
	}

	/**
	 * @return the numberOfProductsSold
	 */
	public int getTotalNumberOfProductsSold() {
		int total = 0;
		for(int i = 0 ; i < numberOfProductsSold.length ; ++i){
			total += numberOfProductsSold[i];
		}
		return total;
	}
	
	/**
	 * @return the numberOfProductsSold
	 */
	public int[] getNumberOfProductsSold() {
		return numberOfProductsSold;
	}

	/**
	 * @param numberOfProductsSold the numberOfProductsSold to set
	 */
	public void setNumberOfProductsSold(int[] numberOfProductsSold) {
		this.numberOfProductsSold = numberOfProductsSold;
	}

	/**
	 * @return the numberOfProductsBought
	 */
	public int getTotalNumberOfProductsBought() {
		int total = 0;
		for(int i = 0 ; i < numberOfProductsBought.length ; ++i){
			total += numberOfProductsBought[i];
		}
		return total;
	}
	
	/**
	 * @return the numberOfProductsBought
	 */
	public int[] getNumberOfProductsBought() {
		return numberOfProductsBought;
	}

	/**
	 * @param numberOfProductsBought the numberOfProductsBought to set
	 */
	public void setNumberOfProductsBought(int[] numberOfProductsBought) {
		this.numberOfProductsBought = numberOfProductsBought;
	}

	/**
	 * @return the buyingChances
	 */
	public int getBuyingChances() {
		return buyingChances;
	}

	/**
	 * @param buyingChances the buyingChances to set
	 */
	public void setBuyingChances(int buyingChances) {
		this.buyingChances = buyingChances;
	}

	/**
	 * @return the sellingChances
	 */
	public int getSellingChances() {
		return sellingChances;
	}

	/**
	 * @param sellingChances the sellingChances to set
	 */
	public void setSellingChances(int sellingChances) {
		this.sellingChances = sellingChances;
	}

	/**
	 * @return the myConsumer
	 */
	public Consumer getMyConsumer() {
		return myConsumer;
	}

	/**
	 * @param myConsumer the myConsumer to set
	 */
	public void setMyConsumer(Consumer myConsumer) {
		this.myConsumer = myConsumer;
	}

	/**
	 * @return the myProducer
	 */
	public Producer getMyProducer() {
		return myProducer;
	}

	/**
	 * @param myProducer the myProducer to set
	 */
	public void setMyProducer(Producer myProducer) {
		this.myProducer = myProducer;
	}

	/**
	 * @return the theMarket
	 */
	public Market getTheMarket() {
		return theMarket;
	}

	/**
	 * @param theMarket the theMarket to set
	 */
	public void setTheMarket(Market theMarket) {
		this.theMarket = theMarket;
	}

	/**
	 * @return the sortUtilityAscending
	 */
	public static Comparator<Agent> getSortUtilityAscending() {
		return sortUtilityAscending;
	}

	/**
	 * @param sortUtilityAscending the sortUtilityAscending to set
	 */
	public static void setSortUtilityAscending(
			Comparator<Agent> sortUtilityAscending) {
		Agent.sortUtilityAscending = sortUtilityAscending;
	}

	/**
	 * @return the sortMoneyAscending
	 */
	public static Comparator<Agent> getSortMoneyAscending() {
		return sortMoneyAscending;
	}

	/**
	 * @param sortMoneyAscending the sortMoneyAscending to set
	 */
	public static void setSortMoneyAscending(Comparator<Agent> sortMoneyAscending) {
		Agent.sortMoneyAscending = sortMoneyAscending;
	}

	/**
	 * @return the sortAddedValueAscending
	 */
	public static Comparator<Agent> getSortAddedValueAscending() {
		return sortAddedValueAscending;
	}

	/**
	 * @param sortAddedValueAscending the sortAddedValueAscending to set
	 */
	public static void setSortAddedValueAscending(
			Comparator<Agent> sortAddedValueAscending) {
		Agent.sortAddedValueAscending = sortAddedValueAscending;
	}

	/**
	 * @return the sortWealthAscending
	 */
	public static Comparator<Agent> getSortWealthAscending() {
		return sortWealthAscending;
	}

	/**
	 * @param sortWealthAscending the sortWealthAscending to set
	 */
	public static void setSortWealthAscending(Comparator<Agent> sortWealthAscending) {
		Agent.sortWealthAscending = sortWealthAscending;
	}
	/**
	 * @return the initialUtility
	 */
	public Double getInitialUtility() {
		return initialUtility;
	}
	/**
	 * @param initialUtility the initialUtility to set
	 */
	public void setInitialUtility(Double initialUtility) {
		this.initialUtility = initialUtility;
	}
	/**
	 * @return the numberOfProducts
	 */
	public int getNumberOfProducts() {
		return numberOfProducts;
	}
	/**
	 * @param numberOfProducts the numberOfProducts to set
	 */
	public void setNumberOfProducts(int numberOfProducts) {
		this.numberOfProducts = numberOfProducts;
	}
	/**
	 * @return the specializedProduction
	 */
	public int getSpecializedProduction() {
		return specializedProduction;
	}
	/**
	 * @param specializedProduction the specializedProduction to set
	 */
	public void setSpecializedProduction(int specializedProduction) {
		this.specializedProduction = specializedProduction;
	}
	/**
	 * @return the partRandomProduction
	 */
	public double getPartRandomProduction() {
		return partRandomProduction;
	}
	/**
	 * @param partRandomProduction the partRandomProduction to set
	 */
	public void setPartRandomProduction(double partRandomProduction) {
		this.partRandomProduction = partRandomProduction;
	}
	/**
	 * @return the includeStocksInWealth
	 */
	public boolean isIncludeStocksInWealth() {
		return includeStocksInWealth;
	}
	/**
	 * @param includeStocksInWealth the includeStocksInWealth to set
	 */
	public void setIncludeStocksInWealth(boolean inclideStocksInWealth) {
		this.includeStocksInWealth = inclideStocksInWealth;
	}
	/**
	 * @return the randomBuyer
	 */
	public Buyer getRandomBuyer() {
		return randomBuyer;
	}
	/**
	 * @param randomBuyer the randomBuyer to set
	 */
	public void setRandomBuyer(Buyer randomBuyer) {
		this.randomBuyer = randomBuyer;
	}
	/**
	 * @return the competitiveBuyer
	 */
	public Buyer getCompetitiveBuyer() {
		return competitiveBuyer;
	}
	/**
	 * @param competitiveBuyer the competitiveBuyer to set
	 */
	public void setCompetitiveBuyer(Buyer competitiveBuyer) {
		this.competitiveBuyer = competitiveBuyer;
	}
	/**
	 * @return the randomSeller
	 */
	public Seller getRandomSeller() {
		return randomSeller;
	}
	/**
	 * @param randomSeller the randomSeller to set
	 */
	public void setRandomSeller(Seller randomSeller) {
		this.randomSeller = randomSeller;
	}
	/**
	 * @return the competitiveSeller
	 */
	public Seller getCompetitiveSeller() {
		return competitiveSeller;
	}
	/**
	 * @param competitiveSeller the competitiveSeller to set
	 */
	public void setCompetitiveSeller(Seller competitiveSeller) {
		this.competitiveSeller = competitiveSeller;
	}
	/**
	 * @return the decreasingUtility
	 */
	public boolean isDecreasingUtility() {
		return decreasingUtility;
	}
	/**
	 * @param decreasingUtility the decreasingUtility to set
	 */
	public void setDecreasingUtility(boolean decreasingUtility) {
		this.decreasingUtility = decreasingUtility;
	}

	public int getIndex() {
		return index;
	}
	
	public int getTotalNumberOfLowcostProductsBought() {
		int total = 0;
		for(int quantity : numberOfLowcostProductsBought)
			total += quantity;
		return total;
	}
	
	
}
