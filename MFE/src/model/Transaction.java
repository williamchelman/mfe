package model;

import helper.Helper;
/**
 * Transactions of the model 
 * @author nicolasbernier
 *
 */
public class Transaction implements Comparable<Transaction>
{
    private Offer sellingOffer;
    private Offer buyingOffer;
    private Double price;
    private Agent buyerAgent;
    private Agent sellerAgent;
    private Market theMarket;
    
    /**
     * Constructor
     * @param sellingOffer - the selling offer
     * @param buyingOffer - the buying offer
     * @param theMarket - the market where the transaction is happening
     */
    public Transaction(Offer sellingOffer, Offer buyingOffer, Market theMarket)
    {
        this.sellingOffer = sellingOffer;
        this.buyingOffer = buyingOffer;
        this.theMarket = theMarket;
        price = 0.0;
    }

    /**
     * Execute the transaction
     * @param tick - the step in the market when the transaction is executed
     */
    public void executeTransaction(int tick)
    {
        price = buyingOffer.getPrice();//negotiatePrice();

        sellerAgent = sellingOffer.getTheAgent();
		boolean hasBeenSold=sellerAgent.getMyProducer().looseProduct(sellingOffer.getTheProduct());
		if(hasBeenSold){
	        sellerAgent.sold(sellingOffer.getTheProduct());
	        sellerAgent.earn(price);
	        sellerAgent.computeAddedValue(sellingOffer.getTheProduct(), price);
	        sellerAgent.computeWealth();
	        
		    buyerAgent = buyingOffer.getTheAgent();
		    buyerAgent.bought(sellingOffer.getTheProduct());
		    buyerAgent.spend(price);
		    buyerAgent.actConsume(sellingOffer.getTheProduct(),sellerAgent.getMyProducer().getQuality());
		    buyerAgent.computeWealth();
		}
		else{
			//Bad
			Helper.print("NO TRANSACTION AT STEP : "+tick);
		}
    }
    /**
     * Negotiate the price of the transaction
     * @return
     */
    public double negotiatePrice()
    {
        double price = 0.0;
        price = sellingOffer.getPrice() + (World.ran.nextDouble() * (buyingOffer.getPrice() - sellingOffer.getPrice()));
        return price;
    }

    @Override
	public int compareTo(Transaction t) {
    	
        return this.price.compareTo(t.getPrice());
    }
    /**
     * Get the product of the transactions
     * @return the product
     */
    public int getTheProduct()
    {
        return sellingOffer.getTheProduct();
    }

	/**
	 * @return the sellingOffer
	 */
	public Offer getSellingOffer() {
		return sellingOffer;
	}


	/**
	 * @param sellingOffer the sellingOffer to set
	 */
	public void setSellingOffer(Offer sellingOffer) {
		this.sellingOffer = sellingOffer;
	}


	/**
	 * @return the buyingOffer
	 */
	public Offer getBuyingOffer() {
		return buyingOffer;
	}


	/**
	 * @param buyingOffer the buyingOffer to set
	 */
	public void setBuyingOffer(Offer buyingOffer) {
		this.buyingOffer = buyingOffer;
	}


	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}


	/**
	 * @return the buyer
	 */
	public Agent getBuyer() {
		return buyerAgent;
	}


	/**
	 * @param buyer the buyer to set
	 */
	public void setBuyer(Agent buyer) {
		this.buyerAgent = buyer;
	}


	/**
	 * @return the seller
	 */
	public Agent getSeller() {
		return sellerAgent;
	}


	/**
	 * @param seller the seller to set
	 */
	public void setSeller(Agent seller) {
		this.sellerAgent = seller;
	}


	/**
	 * @return the theMarket
	 */
	public Market getTheMarket() {
		return theMarket;
	}


	/**
	 * @param theMarket the theMarket to set
	 */
	public void setTheMarket(Market theMarket) {
		this.theMarket = theMarket;
	}

}
