package model;

import java.util.*;

import model.monopoly.PatentOffice;

/**
 * The producer part of the agent
 * @author nicolasbernier
 *
 */
public class Producer
{
    private Agent myAgent;
    //skills for each product => a agent can be a better producer of a product
    private double[] mySkills;
    private ArrayList<Double[]> myHistoricSkills = new ArrayList<Double[]>(); // Utility modification for product i at each consumption of any product !
    //Quantity of each product
    private int[] products;
    private ArrayList<Integer> myProductions=new ArrayList<Integer>();
    private double wealth;
    private double lastProductionCost;
    private int lastProduction;
    private double quality = 1;
    
    private PatentOffice patentOffice;
    
    public Producer(Agent myAgent, PatentOffice patentOffice)
    {
        this.myAgent = myAgent;
        this.patentOffice = patentOffice;
        mySkills = new double[this.myAgent.getNumberOfProducts()];
        products = new int[this.myAgent.getNumberOfProducts()];
        this.initSkills();
        //Initially no product
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            products[i] = 0;
        }
    }
    /**
     * Initialize and normalize the skills of the producer
     */
    public void initSkills(){
        double totalSkill = 0;
    	for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            mySkills[i] = World.ran.nextDouble();
            totalSkill += mySkills[i];
        }
        //Skills normalization
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
             mySkills[i] = mySkills[i] / totalSkill;
        }
        this.logSkill();
    }
    /**
     * Save the skill of the producer at a given time
     */
    public void logSkill(){
    	Double[] d=new Double[this.myAgent.getNumberOfProducts()];
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            d[i] = mySkills[i];
        }
        this.myHistoricSkills.add(d);
    }
    public double computeWealth()
    {
        wealth = 0.0;
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            wealth += products[i] * myAgent.getTheMarket().getProductPrice()[i];
        }
        return wealth;
    }


    public void reset()
    {
        //delete all the product
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            products[i] = 0;
        }
        //reinit skills
        //this.initSkills();
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            mySkills[i] = this.myHistoricSkills.get(0)[i];
        }
        this.logSkill(); 
    }

    public Boolean produce() // RJ version
    {
        Boolean done = false;
        int numberOfAttempts = 0;
        int y = 0;
        this.lastProduction=-1;
        boolean canProduce = canProduce();
        while ((!done) && (numberOfAttempts < World.MAXNUMBEROFATTEMPTS) && canProduce)
        {
        	//select product to produce (according to the skill of the producer)
            int p = this.selectProduct();
            if(p == -1){
            	break;
            } else if (patentOffice.canProduce(myAgent, p)) {
                if (myAgent.getMoney() > getProductionCost(p)){
                	//produce product p
                    products[p]++;
                    //the product p cost its skills
                    myAgent.spend(getProductionCost(p));
                    lastProductionCost = getProductionCost(p);
                    lastProduction = p;
                    done = true;
                    myProductions.add(p);
                    //Specialization
                    
                    //if a product has been produce a lot=>decrease its skills 
                    for (int i : myProductions)
                    {
                        if (i == p)
                        {
                            y++;
                        }
                    }
                    if (y % this.myAgent.getSpecializedProduction() == 0 && y!=0)
                    {
                        double range = mySkills[p] * 0.1; 
                        double min = Math.max((mySkills[p] - range), 0.0);
                        mySkills[p] = World.getRandomNumber(min, mySkills[p]); // Decreased production cost with increased ability (skill cost lest)
                        double totalSkill = 0;
                        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
                        {
                            totalSkill += mySkills[i];
                        }
                        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
                        {
                           mySkills[i] = mySkills[i] / totalSkill;
                        }
                        this.logSkill();

                    }
                } else {
                    numberOfAttempts++;
                    if(myAgent.getPartRandomProduction() == 0 || patentOffice.getNumberOfPatents() >= myAgent.getNumberOfProducts()){
                    	System.out.println("nop produce");
                    	break;
                    }
                }
            }
            else
            {
                numberOfAttempts++;
            }
        }
        return done;
    }
    
    private boolean canProduce(){
    	boolean result = false;
    	for(int product = 0 ; product < mySkills.length ; ++product){
    		double cost = getProductionCost(product);
    		if(cost<=myAgent.getMoney() && patentOffice.canProduce(myAgent, product)){
    			result = true;
    			break;
    		}
    	}
    	return result;
    }

    public double getLastProductionCost()
    {
        return lastProductionCost;
    }

    public int getLastProduction()
    {
        return lastProduction;
    }
    
    public double getProductionCost(int productType){
    	return mySkills[productType]*quality;
    }
    
    public boolean looseProduct(int product)
    {
        if (products[product] == 0)
        {
        	//empty stocks
        	return false;
        }
        else
        {
            products[product]--;
            return true;
        }
    }

    public int getNumberOfProducts()
    {
        int res = 0;
        for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
        {
            res += products[i];
        }
        return res;
    }
    /**
     * Select a product to produce
     * @return the index of the product produced
     */
    public int selectProduct() // RJ version
    {
        int bestProduct = -1;
        //Select product randomly
        if (World.ran.nextDouble() < this.myAgent.getPartRandomProduction())
        {
            bestProduct = World.ran.nextInt(this.myAgent.getNumberOfProducts()); // Proportion of random productions
        }
        //Select most profitable product
        else
        {
            double[] AvgPrices = myAgent.getTheMarket().getAvgPrices();
            double[] expectedProfits = new double[this.myAgent.getNumberOfProducts()];
            for (int i = 0; i < this.myAgent.getNumberOfProducts(); i++)
            {
            	//Product not already produce produced in the case they will give a better added value. 
            	
                if (AvgPrices[i] == 0)
                {
                    expectedProfits[i] = 1 - getProductionCost(i);
                }
                else
                {
                    //temp[i] = 1 + AvgPrices[i] - mySkills[i];
                    expectedProfits[i] = AvgPrices[i] - getProductionCost(i);

                }
            }

            double bestProfit = 0;
            for (int product = 0; product < this.myAgent.getNumberOfProducts(); product++)
            {
                if (expectedProfits[product] > bestProfit && patentOffice.canProduce(myAgent, product) && myAgent.getMoney() >= mySkills[product])
                {
                    bestProfit = expectedProfits[product];
                    bestProduct = product; // Return the product with the best profit !!
                }
            }
        }
//        if(bestProduct < 0){
//        	System.out.println("nop");
//        }
        return bestProduct;
    }
    
    /*
     * Getter/Setter
     */
    
	/**
	 * @return the myAgent
	 */
	public Agent getMyAgent() {
		return myAgent;
	}


	/**
	 * @param myAgent the myAgent to set
	 */
	public void setMyAgent(Agent myAgent) {
		this.myAgent = myAgent;
	}


	/**
	 * @return the mySkills
	 */
	public double[] getMySkills() {
		return mySkills;
	}


	/**
	 * @param mySkills the mySkills to set
	 */
	public void setMySkills(double[] mySkills) {
		this.mySkills = mySkills;
	}


	/**
	 * @return the products
	 */
	public int[] getProducts() {
		return products;
	}


	/**
	 * @param products the products to set
	 */
	public void setProducts(int[] products) {
		this.products = products;
	}


	/**
	 * @return the myProductions
	 */
	public ArrayList<Integer> getMyProductions() {
		return myProductions;
	}


	/**
	 * @param myProductions the myProductions to set
	 */
	public void setMyProductions(ArrayList<Integer> myProductions) {
		this.myProductions = myProductions;
	}


	/**
	 * @return the wealth
	 */
	public double getWealth() {
		return wealth;
	}


	/**
	 * @param wealth the wealth to set
	 */
	public void setWealth(double wealth) {
		this.wealth = wealth;
	}


	/**
	 * @param productionCost the productionCost to set
	 */
	public void setProductionCost(double productionCost) {
		this.lastProductionCost = productionCost;
	}


	/**
	 * @param production the production to set
	 */
	public void setProduction(int production) {
		this.lastProduction = production;
	}
	public PatentOffice getPatentOffice() {
		return patentOffice;
	}
	public void setPatentOffice(PatentOffice patentOffice) {
		this.patentOffice = patentOffice;
	}
	public double getQuality() {
		return quality;
	}
	public void setQuality(double quality) {
		this.quality = quality;
	}

    public int[] getHistoryProduction(){
    	int[] productions = new int[myAgent.getNumberOfProducts()];
    	
    	for(int i = 0 ; i < productions.length ; ++i){
    		productions[i] = 0;
    	}
    	
    	for(int product : myProductions){
    		productions[product]++;
    	}
    	
    	return productions;
    }
	
}
