package model;

/**
 * Market failure exeption
 * @author nicolasbernier
 *
 */
public class MarketFailingException extends Exception
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MarketFailingException(int i)
    {
    	System.out.println("NO POSSIBLE TRANSACTION AT TICK # " + i);
    }
}
