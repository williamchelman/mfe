package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * About dialog used only to display information about the author of the program
 * @author nicolasbernier
 *
 */
public class AboutDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Font text,textBig;
	private Color c=new Color(240,240,240);
	/**
	 * Constructor of the about dialog
	 * @param window - parent window
	 * @param title - title of the dialog
	 */
	public AboutDialog(Window window, String title){
	    super(window, title);
	    this.setResizable(false);
	    this.textBig=new Font(MainView.FONT,Font.BOLD,30);
	    this.text=new Font(MainView.FONT,Font.PLAIN, 14);
		this.initComponent();
	}
	/**
	 * Initialize the components of the about dialog
	 */
	public void initComponent(){
		JPanel inner=new JPanel();
		inner.setLayout(new BoxLayout(inner, BoxLayout.PAGE_AXIS));
		this.setBackground(MainView.GRAPH_COLOR);
		inner.setBackground(MainView.GRAPH_COLOR);
		inner.setBorder(BorderFactory.createEmptyBorder(20, 60, 20, 60));
		JLabel txt1,txt2,txt3,txt4;
		txt1=new JLabel("Market Comparator");
		txt1.setFont(this.textBig);
		txt1.setForeground(this.c);
		txt1.setAlignmentX(CENTER_ALIGNMENT);
		txt1.setBorder(BorderFactory.createEmptyBorder(40, 0, 40, 0));
		
		txt2=new JLabel("2013-2014");
		txt2.setForeground(this.c);
		txt2.setAlignmentX(CENTER_ALIGNMENT);
		txt2.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		txt2.setFont(this.text);
		
		txt3=new JLabel("<html>"
				+ "<p style='text-align:center'>"
				+ "<u>For further informations, pleaser refer to:</u>"
				+ "</p>"
				+ "<p>"
				+ "<table>"
				+ "<tr>"
				+ "<td>[1]</td>"
				+ "<td>"
				+ "<div>Bersini, Hugues & van Zeebroeck, Nicolas. <i>A Stylized Software Model to Explore the Free Market</i></div>"
				+ "<div><i>Equality/Efficiency Tradeoff</i>, Advances in Intelligent Systems and Computing Volume 221, 2013, pp 1-8.</div>"
				+ "</td>"
				+ "</tr><tr>"
				+ "<td>[2]</td>"
				+ "<td>"
				+ "<div>Bernier, Nicolas. <i>Comparison between a competitive and a redistributive market economy using</i></div> "
				+ "<div><i>computer simulation</i>, Master thesis in Computer Science and Engineering, under the direction of</div>"
				+ "<div>Hugues Bersini, Brussels, Université Libre de Bruxelles (ULB), 2014, 61 p.</div>"
				+ "</td>"
				+ "</tr>"
				+ "</p>"
				+ "</html>");
		txt3.setForeground(this.c);
		txt3.setAlignmentX(CENTER_ALIGNMENT);
		txt3.setBorder(BorderFactory.createEmptyBorder(0, 0, 30, 0));
		txt3.setFont(this.text);
		
		txt4=new JLabel("Interface realized by Nicolas Bernier");
		txt4.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
		txt4.setAlignmentX(CENTER_ALIGNMENT);
		txt4.setForeground(this.c);
		txt4.setFont(this.text);
		
		inner.add(txt1);
		inner.add(txt3);
		inner.add(txt2);
		inner.add(txt4);
		
		this.add(inner);
	}
	/**
	 * Show the dialog
	 */
	public void showDialog(){
		this.pack();
		this.setLocation ((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
		this.setVisible(true);
		requestFocus();     
	}
}
