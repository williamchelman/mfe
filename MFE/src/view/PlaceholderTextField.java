package view;

import java.awt.*;

import javax.swing.*;
import javax.swing.text.Document;
/**
 * Custom text field allowing to display a placeholder
 * @author nicolasbernier
 *
 */
public class PlaceholderTextField extends JTextField {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String placeholder;
	/*
	 * Basics constructors of a textfield overridden
	 */
    public PlaceholderTextField() {
    	super();
    	this.setDisabledTextColor(new Color(0.3F,0.3F,0.3F));
    }
    public PlaceholderTextField(
        final Document pDoc,
        final String pText,
        final int pColumns)
    {
        super(pDoc, pText, pColumns);
    	this.setDisabledTextColor(new Color(0.3F,0.3F,0.3F));

    }

    public PlaceholderTextField(final int pColumns) {
        super(pColumns);
    	this.setDisabledTextColor(new Color(0.3F,0.3F,0.3F));

    }

    public PlaceholderTextField(final String pText) {
        super(pText);
    	this.setDisabledTextColor(new Color(0.3F,0.3F,0.3F));

    }

    public PlaceholderTextField(final String pText, final int pColumns) {
        super(pText, pColumns);
    	this.setDisabledTextColor(new Color(0.3F,0.3F,0.3F));

    }
    /**
     * Get the placeholder value
     * @return - placeholder value
     */
    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);

        if (placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        g.setFont(this.getFont());
        g.drawString(placeholder, getInsets().left, pG.getFontMetrics()
            .getMaxAscent() + getInsets().top);
    }
    /**
     * Set placeholder value
     * @param s - the value
     */
    public void setPlaceholder(final String s) {
        placeholder = s;
    }

}