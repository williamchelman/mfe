package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

public class InfoComponent extends JComponent implements MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isClicked=false;
	@SuppressWarnings("unused")
	private boolean isHover=false;
	private String message;
	private Container parent;
	
	public InfoComponent(String message,Container dialog){
		this.parent=dialog;
		this.message=message;
		this.setPreferredSize(new Dimension(16,16));
	}
	@Override
    protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//2) fill the content
	    Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	    int min=(int) Math.min(getWidth(), getHeight());
	    if(!this.isClicked)
	    	g2d.setColor(new Color(50,50,50));
	    else
	    	g2d.setColor(new Color(70,70,70));
	    g.fillRect(getWidth()-min,0,min,min);
	    
	    FontMetrics fm;
        Font f;
        int fontSize=12;
        f=new Font(MainView.FONT,Font.PLAIN,fontSize);
       	fm=this.getFontMetrics(f);
       	g2d.setFont(f);
       	String content="?";
       	Rectangle2D rect=fm.getStringBounds(content, g2d);
       	int textHeight = (int)(rect.getHeight()); 
       	int textWidth  = (int)(rect.getWidth());
       	g2d.setColor(new Color(250,250,250));
     // Center text horizontally and vertically
    	int x = (getWidth()-min)+(min  - textWidth)  / 2;
    	int y = (getHeight() - textHeight) / 2  + fm.getAscent();
       	g2d.drawString(content,x,y);

    }

	@Override
	public void mouseClicked(MouseEvent e) {
		JOptionPane.showMessageDialog(null,message,"Info", JOptionPane.INFORMATION_MESSAGE);
		this.parent.requestFocus();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		this.isHover=true;
		this.setCursor(new Cursor(Cursor.HAND_CURSOR));
		this.repaint();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		this.isHover=false;
		this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		this.repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		this.isClicked=true;
		this.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		this.isClicked=false;
		this.repaint();
	}
	
	

}
