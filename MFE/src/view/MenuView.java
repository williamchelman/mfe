package view;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import controller.Mode;
import controller.UserGlobalController;
/**
 * Menu of the application
 * @author nicolasbernier
 *
 */
public class MenuView extends JMenuBar {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String MODE_1="Single World";
	private static final String MODE_2="Comparison";
	private static final String MODE_3="Stat";
	
	private Mode mode;
	
	private JMenu worlds = new JMenu("Worlds");
	private JMenu worldSelected = new JMenu("Selected World");
	private JMenu worldsEdit = new JMenu("Edit world");
	private JMenuItem worldsCreate = new JMenuItem("Create new world");
	private JMenu worldsDelete = new JMenu("Delete word");
	//to select
	private ArrayList<JCheckBoxMenuItem> listWorlds=new ArrayList<JCheckBoxMenuItem>();
	private ArrayList<JCheckBoxMenuItem> worldChecked=new ArrayList<JCheckBoxMenuItem>();
	//to edit
	private ArrayList<JMenuItem> listWorldsUpdate=new ArrayList<JMenuItem>();
	//to delete
	private ArrayList<JMenuItem> listWorldsDelete=new ArrayList<JMenuItem>();

	private JMenu graphs = new JMenu("Graphs");
	private JMenu graphsSelected = new JMenu("Graphs displayed");
	public final static String GRAPH_NAMES[]={"Utility","Money","Added Value","Wealth","Product In Stock (PIS)","Value PIS","% Agents Broke","Market Failure","Gini Utility","Gini Added Value","Gini Money","Gini Wealth","Lorentz Utility","Lorentz Added Value","Lorentz Money","Lorentz Wealth"};
	
	private ArrayList<JCheckBoxMenuItem> potentialGraphs=new ArrayList<JCheckBoxMenuItem>();	
	
	private JMenu modes = new JMenu("Modes");
	private JRadioButtonMenuItem mode1=new JRadioButtonMenuItem(MenuView.MODE_1);
	private JRadioButtonMenuItem mode2=new JRadioButtonMenuItem(MenuView.MODE_2);
	private JRadioButtonMenuItem mode3=new JRadioButtonMenuItem(MenuView.MODE_3);
	/*
	//TODO:eventually add export mode
	private JMenu exports = new JMenu("Export");
	private JMenu exportSelect=new JMenu("Export csv data");
	*/
	private JMenu about = new JMenu("About");
	private JMenuItem aboutElem= new JMenuItem("?");
	private ArrayList<JCheckBoxMenuItem> graphCheckedElem=new ArrayList<JCheckBoxMenuItem>();
	private ArrayList<Integer> graphChecked=new ArrayList<Integer>();
	
	private WorldDialog wd;
	private AboutDialog ad;
	
	private UserGlobalController controller;
	
	//parent view
	private MainView parent;
	/**
	 * Constructor of the menu
	 * @param parent - the parent view
	 */
	public MenuView(MainView parent){
		this.parent=parent;
		this.mode=Mode.single;
		//world menu
		this.worlds.add(this.worldSelected);
		this.worlds.add(this.worldsEdit);
		this.worlds.add(this.worldsCreate);
		this.worlds.addSeparator();
		this.worlds.add(this.worldsDelete);
		
		//graph menu
		this.graphs.add(this.graphsSelected);
		
		for(int i=0;i<MenuView.GRAPH_NAMES.length;i++){
			this.potentialGraphs.add(new JCheckBoxMenuItem(MenuView.GRAPH_NAMES[i]));
		}
		int selection[]={9,10,0,8};
		for(int i:selection){
			JCheckBoxMenuItem elem=this.potentialGraphs.get(i);
			elem.setSelected(true);
			this.graphCheckedElem.add(elem);
		}
		for(int i=0;i<MenuView.GRAPH_NAMES.length;i++){
			JCheckBoxMenuItem elem=this.potentialGraphs.get(i);
			elem.setName(String.valueOf(i));
			this.graphsSelected.add(elem);
		}
		//add listener to Graph
		this.computeGraphChecked();
		
		//Mode
		ButtonGroup bg=new ButtonGroup();
		bg.add(this.mode1);
		bg.add(this.mode2);
		bg.add(this.mode3);
		this.modes.add(this.mode1);
		this.modes.add(this.mode2);
		this.modes.add(this.mode3);
		this.mode1.setSelected(true);
		//about
		this.about.add(this.aboutElem);
		
		//add to menuBar
		this.add(this.worlds);
		this.add(this.graphs);
		this.add(this.modes);
		this.add(this.about);
		//JFrame f =(JFrame)SwingUtilities.getAncestorOfClass(JFrame.class,this.parent);
		
		this.wd=new WorldDialog(SwingUtilities.windowForComponent(this),"");
		this.ad=new AboutDialog(SwingUtilities.windowForComponent(this),"ABOUT");
	}
	/**
	 * Tell the view programmatically that a world has to be checked
	 * @param name - the concerned world
	 */
	public void setSelectedWord(String name){
		for(JCheckBoxMenuItem r:this.listWorlds){
			if(r.getText().equals(name)){
				r.setSelected(true);
				this.worldChecked.add(r);
				break;
			}
		}
	}
	/**
	 * Reset the world checked
	 */
	public void resetWorldChecked(){
		for(JCheckBoxMenuItem r:this.listWorlds){
			r.setSelected(false);
		}
		this.worldChecked.clear();
	}
	/**
	 * Add a world to the list of the worlds (3 lists)
	 * @param name - the name of the new world
	 */
	public void addWorld(String name){
		//Select world
		JCheckBoxMenuItem elem=new JCheckBoxMenuItem(name);
		elem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JCheckBoxMenuItem item=(JCheckBoxMenuItem)arg0.getSource();
				handleSelectWorld(item);
			}
		});
		this.listWorlds.add(elem);
		//Delete world
		JMenuItem elemDelete=new JMenuItem(name);
		elemDelete.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JMenuItem m=(JMenuItem)arg0.getSource();
				handleDelete(m);
			}
		});
		this.listWorldsDelete.add(elemDelete);
		//Update world
		JMenuItem elemUpdate=new JMenuItem(name);
		elemUpdate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JMenuItem m=(JMenuItem)arg0.getSource();
				handleUpdate(m);
			}
		});
		this.listWorldsUpdate.add(elemUpdate);
		
		this.resetButton();
	}
	/**
	 * Handle the selection of a World
	 * @param c - the component representing the world selected (or de-selected)
	 */
	public void handleSelectWorld(JCheckBoxMenuItem c){
		if(this.mode==Mode.single){
			if(!c.isSelected()){
				//has selected the same world than previously=>cannot have a world with no world selected
				c.setSelected(true);
			}
			else{
				if(worldChecked.size()!=0){
					JCheckBoxMenuItem item=worldChecked.get(0);
					item.setSelected(false);
					worldChecked.remove(item);
				}
				worldChecked.add(c);
				this.controller.selectWorld(c.getText());
			}
		}
		else if(this.mode==Mode.comparison){
			if(!c.isSelected()){
				//has selected the same world than previously=>cannot uncheck a world selected
				c.setSelected(true);
			}
			else{
				if(worldChecked.size()==0){
					//should not happen
				}
				else if(worldChecked.size()>0){
					if(worldChecked.size()>1){
						//should always happen
						JCheckBoxMenuItem item=worldChecked.remove(0);
						item.setSelected(false);
					}
					worldChecked.add(c);
					this.controller.selectWorld(worldChecked.get(0).getText(),worldChecked.get(1).getText());
				}
			}
		}
	}
	/**
	 * Handle the deletion of a world
	 * @param m - the component representing the world 
	 */
	public void handleDelete(JMenuItem m){
		String name=m.getText(); 
		UIManager.put("OptionPane.yesButtonText", "YES");  
		UIManager.put("OptionPane.noButtonText", "NO");  

		int option = JOptionPane.showConfirmDialog(null, "ARE YOU SURE ?", "WORLD DELETE", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		
		if(option == JOptionPane.OK_OPTION){
			this.controller.deleteWorld(name);
		}
		//else => same as close
	}
	/**
	 * Handle the update of a world
	 * @param m - the component representing the world 
	 */
	public void handleUpdate(JMenuItem m){
		//1)Dialog with different fields (same than the one use to create)
		//2)controller set value to these fields
		//3)when saving, tell the controller to get the value and set the value to the world
		this.wd.setTitle("World Edition");
		this.wd.setIsEdit(true);
		String name=m.getText();
		this.wd.setOldNameWorld(name);
		this.controller.setWorldValues(this.wd,name);
		this.wd.showDialog();
	}
	/**
	 * Remove a world for all the list
	 * @param name - the world to delete
	 */
	public void removeWorld(String name){
		for(JCheckBoxMenuItem r:this.listWorlds){
			if(r.getText().equals(name)){
				this.listWorlds.remove(r);
				break;
			}
		}
		for(JMenuItem r:this.listWorldsUpdate){
			if(r.getText().equals(name)){
				this.listWorldsUpdate.remove(r);
				break;
			}
		}
		for(JMenuItem r:this.listWorldsDelete){
			if(r.getText().equals(name)){
				this.listWorldsDelete.remove(r);
				break;
			}
		}
		this.resetButton();
	}
	/**
	 * Reset the world list displayed to the user
	 */
	private void resetButton(){
		this.worldSelected.removeAll();
		this.worldsEdit.removeAll();
		this.worldsDelete.removeAll();
		
		for(JCheckBoxMenuItem r:this.listWorlds){
			this.worldSelected.add(r);
		}
		for(JMenuItem r:this.listWorldsDelete){
			this.worldsDelete.add(r);
		}
		for(JMenuItem r:this.listWorldsUpdate){
			this.worldsEdit.add(r);
		}
	}
	/*
	 * Actions
	 */
	/**
	 * Add a listener to the button world creation
	 */
	public void addCreationListener(){
		this.worldsCreate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				wd.setTitle("World Creation");
				if(wd.getIsEdit()){
					wd.reinitialize();
				}
				wd.setIsEdit(false);
				wd.showDialog();
			}
		});
	}
	/**
	 * Add a listener to the about (?) button
	 */
	public void addAboutListener(){
		this.aboutElem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ad.showDialog();
			}
		});
	}
	/*
	 * Graph listener function
	 */
	/**
	 * Add listener to the graph buttons 
	 */
	public void addGraphListener(){
		for(int i=0;i<MenuView.GRAPH_NAMES.length;i++){
			this.addGraphListenerElem(this.potentialGraphs.get(i));
		}
	}
	private void addGraphListenerElem(JCheckBoxMenuItem elem){
		elem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBoxMenuItem item=(JCheckBoxMenuItem)e.getSource();
				boolean state=item.isSelected();
				//Integer index=Integer.getInteger(item.getName());
				if(state){
					//elem is checked
					int limit=0;
					if(mode==Mode.single)
						limit=GraphView.NUM_GRAPH;
					else if(mode==Mode.comparison)
						limit=GraphView.NUM_GRAPH/2;
					if(graphCheckedElem.size()>=limit){
						//remove first elem
						JCheckBoxMenuItem itemToRemove=graphCheckedElem.get(0);
						itemToRemove.setSelected(false);
						graphCheckedElem.remove(itemToRemove);
					}
					graphCheckedElem.add(item);
				}
				else{
					graphCheckedElem.remove(item);
				}
				computeGraphChecked();
				controller.getWorldController().changeGraph();
			}
		});
	}
	/*
	 * Mode related functions
	 */
	/**
	 * Add events to the two mode buttons
	 */
	public void addModeEvents(){
		//Mode listener
		ActionListener a=new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JRadioButtonMenuItem item=(JRadioButtonMenuItem)arg0.getSource();
				String txt=item.getText();
				boolean hasChanged=false;
				if(txt.equals(MenuView.MODE_1) && mode!=Mode.single){
					hasChanged=handleSingleMode();
				}
				else if(txt.equals(MenuView.MODE_2) && mode!=Mode.comparison){
					hasChanged=handleComparisonMode();
				}
				else if(txt.equals(MenuView.MODE_3) && mode!=Mode.stat){
					hasChanged=handleStatMode();
				}
				if(hasChanged){
					computeGraphChecked();
					controller.setMode(mode);
				}

			}
		};
		this.mode1.addActionListener(a);
		this.mode2.addActionListener(a);
		this.mode3.addActionListener(a);
	}
	/**
	 * Check if it is allowed to change mode to comparison mode
	 * @return - true of false
	 */
	public boolean handleComparisonMode(){
		if(controller.checkCanChangeMode()){
			mode=Mode.comparison;
			if(graphCheckedElem.size()>2){
				while(graphCheckedElem.size()>2){
					JCheckBoxMenuItem itemToRemove=graphCheckedElem.get(0);
					itemToRemove.setSelected(false);
					graphCheckedElem.remove(itemToRemove);
				}
			}
			mode2.setSelected(true);
			return true;
		}
		else{
			mode1.setSelected(true);
			parent.showPopupError(new String("You must have at least two worlds to choose this mode").toUpperCase(),"ERROR MODE");
			return false;
		}
	}
	/**
	 * Set in single mode
	 * @return - true of false
	 */
	public boolean handleSingleMode(){
		mode=Mode.single;
		if(graphCheckedElem.size()<4){
			for(JCheckBoxMenuItem g:potentialGraphs){
				if(!g.isSelected()){
					g.setSelected(true);
					graphCheckedElem.add(g);
				}
				if(graphCheckedElem.size()>=4){
					break;
				}
			}
		}
		if(worldChecked.size()>1){
			while(worldChecked.size()>1){
				JCheckBoxMenuItem itemToRemove=worldChecked.get(worldChecked.size()-1);
				itemToRemove.setSelected(false);
				worldChecked.remove(itemToRemove);
			}
		}
		this.mode1.setSelected(true);
		return true;
	}
	/**
	 * Set in stat mode
	 * @return - true of false
	 */
	public boolean handleStatMode(){
		mode=Mode.stat;
		/*if(graphCheckedElem.size()<4){
			for(JCheckBoxMenuItem g:potentialGraphs){
				if(!g.isSelected()){
					g.setSelected(true);
					graphCheckedElem.add(g);
				}
				if(graphCheckedElem.size()>=4){
					break;
				}
			}
		}*/
		if(worldChecked.size()>1){
			while(worldChecked.size()>1){
				JCheckBoxMenuItem itemToRemove=worldChecked.get(worldChecked.size()-1);
				itemToRemove.setSelected(false);
				worldChecked.remove(itemToRemove);
			}
		}
		this.mode3.setSelected(true);
		return true;
	}
	/**
	 * Compute which graph have been checked
	 */
	public void computeGraphChecked(){
		this.graphChecked.clear();
		for(JCheckBoxMenuItem elem: this.graphCheckedElem){
			if(elem.isSelected()){
				this.graphChecked.add(Integer.valueOf(elem.getName()));
			}
		}
	}
	/**
	 * Get the graph Checked
	 */
	public ArrayList<Integer> getGraphChecked(){
		return this.graphChecked;
	}
	/**
	 * Attach a controller to this view
	 * @param controller - the controller
	 */
	public void setController(UserGlobalController controller) {
		this.controller=controller;
		this.wd.attachController(controller);
	}
	
	@Override
	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		worlds.setEnabled(enabled);
		graphs.setEnabled(enabled);
		modes.setEnabled(enabled);
	}
}
