package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import view.stat.StatView;
import controller.Mode;
import controller.StatController;
import controller.UserGlobalController;

/**
 * The main window of the application, implemented as an Applet
 * @author nicolasbernier
 *
 */
public class MainView extends JApplet implements Observer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String FONT="Tahoma";
	public static final int WIDTH=1000;
	public static final int HEIGHT=650;
	private static final float PERCENTAGE_WIDTH_GRAPH=0.7F;
	public static final Color CONTROL_COLOR=new Color(155,155,155);
	public static final Color GRAPH_COLOR=new Color(50,50,50);
	public static final Color COLOR_DISABLED=new Color(175,175,175);
	public static final Color COLOR_INPUT=new Color(188,188,188);
	public static final Color COLOR_TEXT=new Color(40,40,40);
	public static final Color COLOR_BUTTON_SELECTED=new Color(235,235,235);
	public static final Font FONT_DIALOG=new Font(MainView.FONT,Font.PLAIN,15);
	private Mode mode;
	
	private JPanel mainPanel;
	//Header of the app (contains a menu)
	private MenuView menuView;
	//body of the app (contains a graphic panel and a control panel)
	private GraphView graphPanel;
	private ControlView controlPanel;
	private StatController statController;
	
	private UserGlobalController controller;
	/**
	 * Constructor of the MainView - Since it is an Applet it also represent the constructor of the program
	 */
	public MainView(){ 
		this.initFrame();
		//Constructor
		UserGlobalController controller=new UserGlobalController(this);
        this.addAllListener(controller);
        controller.init();
	}
	/**
	 * Init the Frame components
	 */
	public void initFrame(){
		//global settings
		UIManager.put("MenuBar.background",new Color(240,240,240));
		UIManager.put("Button.select",MainView.COLOR_BUTTON_SELECTED);

		this.setSize(WIDTH,HEIGHT);
				
		//Organize the frame
		this.mainPanel=new JPanel();
		this.mainPanel.setBackground(new Color(0.906F,0.906F,0.906F));
		this.mainPanel.setLayout(new BorderLayout());
		//organization of the frame
						
		//Header
			
		this.menuView=new MenuView(this);
		this.menuView.setPreferredSize(new Dimension(WIDTH,27));		

		//Body
		//1) Graph Panel
		this.graphPanel=new GraphView(this);
		this.graphPanel.setBackground(new Color(50,50,50));
		this.graphPanel.setSize(new Dimension(Math.round(PERCENTAGE_WIDTH_GRAPH*WIDTH),HEIGHT));
		//2) Control Panel
		this.controlPanel=new ControlView(this);
		this.controlPanel.setBackground(MainView.CONTROL_COLOR);
		this.controlPanel.setSize(new Dimension(Math.round((1-PERCENTAGE_WIDTH_GRAPH)*WIDTH),HEIGHT));
						
		this.mainPanel.add(this.graphPanel,BorderLayout.CENTER);
		this.mainPanel.add(this.controlPanel,BorderLayout.EAST);
		this.start();
	}
	/**
	 * Continuation of the initialization that start the Window
	 */
	public void start(){
		this.setJMenuBar(this.menuView);
		
		this.setContentPane(this.mainPanel);
		this.showView();
		this.setSize(WIDTH,HEIGHT);
	}
	/**
	 * Add the different listener to its components
	 * @param controller
	 */
	public void addAllListener(UserGlobalController controller){
		this.setController(controller);
		this.addControlListener();
		this.addMenuListener();
	}
	/**
	 * Set the global controller of this view
	 * @param controller
	 */
	public void setController(UserGlobalController controller){
		this.controller=controller;
		this.controlPanel.setController(this.controller.getWorldController());
		this.menuView.setController(this.controller);
	}
	/**
	 * Add listener to the control panel
	 */
	public void addControlListener(){
		//Control Panel is referring to ONE world
		this.controlPanel.addStartListener();
		this.controlPanel.addResetListener();
		this.controlPanel.addPauseListener();
	}
	/**
	 * Add listener to the menu
	 */
	public void addMenuListener(){
		//Menu View
		//this.menuView.addDeleteListener();
		this.menuView.addGraphListener();
		this.menuView.addModeEvents();
		this.menuView.addCreationListener();
		this.menuView.addAboutListener();
	}
	/**
	 * Change the mode of the view (same mode than in the controller)
	 * @param m - the Mode
	 */
	public void setMode(Mode m){
		this.mode=m;
		this.mainPanel.removeAll();
		if(this.mode == Mode.single || this.mode == Mode.comparison){
			this.graphPanel.constructPanel(this.mode);
			this.controlPanel.constructPanel(this.mode);
			this.mainPanel.add(this.graphPanel,BorderLayout.CENTER);
			this.mainPanel.add(this.controlPanel,BorderLayout.EAST);
			this.graphPanel.repaint();
			this.controlPanel.repaint();
		} else if(this.mode == Mode.stat){
			StatView statPanel = new StatView(this);
			statController = new StatController(statPanel);
			statPanel.attachController(statController);
			this.mainPanel.add(statPanel, BorderLayout.CENTER);
		}
		// Because repaint() didn't work for some reason in Mode.stat mode
		this.validate();
		this.addControlListener();
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof Integer){
			this.graphPanel.updateGraph();
			this.controlPanel.updatePanel((Integer)arg1);
		}
		//Message part (could be used)
		else if(arg1 instanceof String){
			//...
		}
	}
	/**
	 * Show the view
	 */
	public void showView() {
		this.setVisible(true);
		requestFocus();
	}
	/**
	 * Show popup message
	 * @param message - the message
	 * @param title - the message title
	 */
	public void showPopupError(String message,String title){
		JOptionPane.showMessageDialog(null,message,title, JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Show popup message
	 * @param message - the message
	 * @param title - the message title
	 */
	public void showPopupInfo(String message,String title){
		JOptionPane.showMessageDialog(null,message,title, JOptionPane.INFORMATION_MESSAGE);
	}
	/**
	 * Return its controller
	 * @return the controller
	 */
	public UserGlobalController getController(){
		return this.controller;
	}
	/*
	 * Getter of the sub views
	 */
	public MenuView getMenuView() {
		return menuView;
	}
	public GraphView getGraphPanel() {
		return graphPanel;
	}
	public ControlView getControlPanel() {
		return controlPanel;
	}
}
