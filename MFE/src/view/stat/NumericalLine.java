package view.stat;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class NumericalLine {
	public static final int INT_TYPE = 0;
	public static final int DOUBLE_TYPE = 1;
	
	private int type;
	private NumericalLine self = this;
	
	public JLabel title;
	public JTextField valueTF;
	public JTextField minTF;
	public JTextField maxTF;
	public JTextField stepTF;
	
	public JComboBox<String> variability;
	
	private boolean positive = false;
	private boolean strictlyPositive = false;
	private boolean lowerOrEqualToOne = false;

	private static final Color errorColor = Color.RED;
	private static final Color noErrorColor = Color.WHITE;
	
	public NumericalLine(String name, String help,ParametersPanel pointer, String defaultValue, int type) {
		super();
		title = new JLabel(name+":",JLabel.LEFT);
		title.setToolTipText(help);
		
		valueTF = new JTextField(5);
		valueTF.setText(defaultValue);
		valueTF.getDocument().addDocumentListener(new BasicDocumentListener(valueTF));
		
		minTF = new JTextField(5);
		minTF.setText(defaultValue);
		minTF.getDocument().addDocumentListener(new MinMaxDocumentListener(minTF));
		
		maxTF = new JTextField(5);
		maxTF.setText(defaultValue);
		maxTF.getDocument().addDocumentListener(new MinMaxDocumentListener(maxTF));
		
		stepTF = new JTextField(5);
		stepTF.setText("1");
		stepTF.getDocument().addDocumentListener(new StepDocumentListener(stepTF));
		
		variability = new JComboBox<String>();
		variability.addItem("fixed");
		variability.addItem("variates");
		variability.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pointer.placeElements();
				pointer.repaint();
				pointer.validate();
			}
		});
		
		
		
		this.type = type;
	}
	
	class BasicDocumentListener implements DocumentListener {
		protected JTextField tf;
	
		public BasicDocumentListener(JTextField tf) {
			super();
			this.tf = tf;
		}

		protected boolean detectError(){
			try{
				double value;
				if(self.type == INT_TYPE){
					value = Integer.valueOf(tf.getText());
				} else {
					value = Double.valueOf(tf.getText());
				}
				String errorMessage = null;
				if(self.positive && value < 0){
					errorMessage = "Should be positive";
				} else if(self.strictlyPositive && value <= 0){
					errorMessage = "Should be strictly positive";
				} else if(self.lowerOrEqualToOne && value > 1){
					errorMessage = "Should be lower or equal to 1";
				}
				if(errorMessage == null){
					noError(tf);
					return false;
				} else {
					error(tf,errorMessage);
					return true;
				}
			} catch(Exception e){
				if(self.type == INT_TYPE){
					error(tf,"Should be an integer");
					return true;
				} else if(self.type == DOUBLE_TYPE){
					error(tf,"Should be a double");
					return true;
				}
			}
			return false;
		}
		

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			detectError();
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			detectError();
		}
		
		@Override
		public void changedUpdate(DocumentEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class MinMaxDocumentListener extends BasicDocumentListener{

		public MinMaxDocumentListener(JTextField tf) {
			super(tf);
		}
		
		@Override
		protected boolean detectError(){
			if(super.detectError()){
				return true;
			} else {
				try{
					if(Double.valueOf(minTF.getText()) > Double.valueOf(maxTF.getText())){
						if(tf.equals(minTF))
							self.error(minTF, "min should be lower or equal to max");
						else if(tf.equals(maxTF))
							self.error(maxTF, "max should be greater or equal to min");
						return true;
					}
				}catch(Exception e){
					
				}
				return false;
			}
		}
		
	}
	
	class StepDocumentListener extends BasicDocumentListener{

		public StepDocumentListener(JTextField tf) {
			super(tf);
		}
		
		@Override
		protected boolean detectError(){
			if(super.detectError()){
				return true;
			} else {
				try{
					if(Double.valueOf(stepTF.getText()) <= 0){
						self.error(stepTF, "Should be strictly positive");
						return true;
					}
				}catch(Exception e){
					
				}
				return false;
			}
		}
	}
	
	private void error(JTextField tf, String message){
		tf.setBackground(errorColor);
		tf.setToolTipText(message);
	}
	
	private void noError(JTextField tf){
		tf.setBackground(noErrorColor);
		tf.setToolTipText("");
	}
	
	public boolean isFixed(){
		return variability.getSelectedItem().equals("fixed");
	}

	public NumericalLine setPositive(boolean positive) {
		this.positive = positive;
		return this;
	}

	public NumericalLine setStrictlyPositive(boolean strictlyPositive) {
		this.strictlyPositive = strictlyPositive;
		return this;
	}

	public NumericalLine setLowerOrEqualToOne(boolean lowerOrEqualToOne) {
		this.lowerOrEqualToOne = lowerOrEqualToOne;
		return this;
	}
	
	public String getMin(){
		return minTF.getText();
	}
	
	public String getMax(){
		return maxTF.getText();
	}
	
	public String getStep(){
		return stepTF.getText();
	}
	
	public String getValue(){
		return valueTF.getText();
	}
	
	public Boolean isGoodToGo(){
		if(isFixed()){
			return valueTF.getBackground().equals(noErrorColor);
		} else {
			return minTF.getBackground().equals(noErrorColor) && maxTF.getBackground().equals(noErrorColor) && stepTF.getBackground().equals(noErrorColor);
		}
	}
	
	public void setEnabled(boolean enabled){
		valueTF.setEnabled(enabled);
		minTF.setEnabled(enabled);
		maxTF.setEnabled(enabled);
		stepTF.setEnabled(enabled);
		variability.setEnabled(enabled);
	}
}
