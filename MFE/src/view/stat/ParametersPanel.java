package view.stat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class ParametersPanel extends JPanel {
	private static final Insets WEST_INSETS = new Insets(5, 0, 5, 5);
	private static final Insets EAST_INSETS = new Insets(5, 5, 5, 0);
	
	private Map<String, NumericalLine> numericalParameters = new HashMap<String, NumericalLine>();
	private Map<String, BooleanLine> booleanParameters = new HashMap<String, BooleanLine>();
	private ArrayList<Object> allParameters = new ArrayList<Object>();
	
	private JPanel statView;
	
	public ParametersPanel(JPanel statView,String title){
		this.statView = statView;
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(title),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
	}
	
	public ParametersPanel(JPanel statView){
		this.statView = statView;
		setLayout(new GridBagLayout());
	}
	
	private GridBagConstraints createGbc(int x, int y) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		
		gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
		gbc.fill = (x == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;
		
		gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
		gbc.weightx = (x == 0) ? 0.1 : 1.0;
		gbc.weighty = 1.0;
		return gbc;
   }
	
	public NumericalLine addDoubleParameterLine(String name, String help, String defaultValue){
		NumericalLine line = new NumericalLine(name,help, this, defaultValue, NumericalLine.DOUBLE_TYPE);
		numericalParameters.put(name, line);
		allParameters.add(line);
		placeElements();
		return line;
	}
	
	public NumericalLine addIntParameterLine(String name, String help, String defaultValue){
		NumericalLine line = new NumericalLine(name,help, this, defaultValue, NumericalLine.INT_TYPE);
		numericalParameters.put(name, line);
		allParameters.add(line);
		placeElements();
		return line;
	}
	
	public BooleanLine addBooleanParameterLine(String name, String help, boolean defaultValue){
		BooleanLine line = new BooleanLine(name, help, this,defaultValue);
		booleanParameters.put(name, line);
		allParameters.add(line);
		placeElements();
		return line;
	}
	
	public void placeElements(){
		this.removeAll();
		int y = 0;
		for(Object o : allParameters){
			if(o instanceof NumericalLine){
				NumericalLine line = (NumericalLine)o;
				
				GridBagConstraints gbc = createGbc(0, y);
				this.add(line.title,gbc);
				
				gbc = createGbc(1, y);
				this.add(line.variability,gbc);
				
				if(line.isFixed()){
					gbc = createGbc(3, y);
					this.add(line.valueTF,gbc);
				} else {
					int x = 2;
					gbc = createGbc(x, y);
					this.add(new JLabel("min:",JLabel.LEFT),gbc);
					++x;
					gbc = createGbc(x, y);
					this.add(line.minTF,gbc);
					++x;
					gbc = createGbc(x, y);
					this.add(new JLabel("max:",JLabel.LEFT),gbc);
					++x;
					gbc = createGbc(x, y);
					this.add(line.maxTF,gbc);
					++x;
					gbc = createGbc(x, y);
					this.add(new JLabel("step:",JLabel.LEFT),gbc);
					++x;
					gbc = createGbc(x, y);
					this.add(line.stepTF,gbc);
					++x;
				}
				++y;
			} else if(o instanceof BooleanLine){
				BooleanLine line = (BooleanLine) o;
				
				GridBagConstraints gbc = createGbc(0, y);
				this.add(line.title,gbc);
				
				gbc = createGbc(1, y);
				this.add(line.variability,gbc);
				
				if(line.isFixed()){
					gbc = createGbc(3, y);
					this.add(line.valueTF,gbc);
				}
				
				++y;
			}
		}
	}
	
	public NumericalLine getNumericalLine(String name){
		return numericalParameters.get(name);
	}
	
	public BooleanLine getBooleanLine(String name){
		return booleanParameters.get(name);
	}
	
	public boolean areParametersValid(){
		Set<String> names = numericalParameters.keySet();
		for(String name : names){
			NumericalLine nl = numericalParameters.get(name);
			if(!nl.isGoodToGo()){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void setEnabled(boolean enabled){
		//super.setEnabled(enabled);
		
		Set<String> names = numericalParameters.keySet();
		for(String name : names){
			NumericalLine nl = numericalParameters.get(name);
			nl.setEnabled(enabled);
		}
		
		names = booleanParameters.keySet();
		for(String name : names){
			BooleanLine bl = booleanParameters.get(name);
			bl.setEnabled(enabled);
		}
	}

}
