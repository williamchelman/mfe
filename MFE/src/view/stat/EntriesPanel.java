package view.stat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


@SuppressWarnings("serial")
public class EntriesPanel extends JPanel {
	private static final Insets WEST_INSETS = new Insets(5, 0, 5, 5);
	private static final Insets EAST_INSETS = new Insets(5, 5, 5, 0);
	
	private Map<String, Object> entries = new HashMap<String, Object>();
	private int currentY = 0;
	
	public EntriesPanel(String title){
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createCompoundBorder(
				BorderFactory.createTitledBorder(title),
				BorderFactory.createEmptyBorder(5, 5, 5, 5)));
	}
	
	public EntriesPanel(){
		setLayout(new GridBagLayout());
	}
	
	public void addTextField(String name, String toolTip){
		addTextField(name, toolTip, "");
	}
	
	public void addTextField(String name, String toolTip, String text){
		JLabel title = new JLabel(name+":",JLabel.LEFT);
		GridBagConstraints gbc = createGbc(0,currentY);
		this.add(title, gbc);
		
		JTextField tf = new JTextField(10);
		tf.setText(text);
		tf.setToolTipText(toolTip);
		gbc = createGbc(1,currentY);
		this.add(tf, gbc);
		entries.put(name, tf);
		++currentY;
	}
	
	public void addPasswordField(String name){
		JLabel title = new JLabel(name+":",JLabel.LEFT);
		GridBagConstraints gbc = createGbc(0,currentY);
		this.add(title, gbc);
		
		JTextField tf = new JPasswordField(10);
		gbc = createGbc(1,currentY);
		this.add(tf, gbc);
		entries.put(name, tf);
		++currentY;
	}
	
	public void addComboBox(String name,String[] choices, String tooltip){
		JLabel title = new JLabel(name+":",JLabel.LEFT);
		GridBagConstraints gbc = createGbc(0,currentY);
		this.add(title, gbc);
		
		JComboBox<String> cb = new JComboBox<String>();
		for(String choice : choices){
			cb.addItem(choice);
		}
		gbc = createGbc(1,currentY);
		this.add(cb, gbc);
		cb.setToolTipText(tooltip);
		entries.put(name, cb);
		
		++currentY;
	}
	
	public void addCheckBox(String name, String tooltip){
		JLabel title = new JLabel(name+":",JLabel.LEFT);
		GridBagConstraints gbc = createGbc(0,currentY);
		this.add(title, gbc);
		
		JCheckBox cb = new JCheckBox();
		
		gbc = createGbc(1,currentY);
		this.add(cb, gbc);
		cb.setToolTipText(tooltip);
		entries.put(name, cb);
		
		++currentY;
	}
	
	private GridBagConstraints createGbc(int x, int y) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = 1;
		gbc.gridheight = 1;
		
		gbc.anchor = (x == 0) ? GridBagConstraints.WEST : GridBagConstraints.EAST;
		gbc.fill = (x == 0) ? GridBagConstraints.BOTH : GridBagConstraints.HORIZONTAL;
		
		gbc.insets = (x == 0) ? WEST_INSETS : EAST_INSETS;
		gbc.weightx = (x == 0) ? 0.1 : 1.0;
		gbc.weighty = 1.0;
		return gbc;
   }
	
	public String getEntryValue(String name){
		String result = null;
		Object o = entries.get(name);
		if(o instanceof JTextField){
			JTextField field = (JTextField) o;
			result = field.getText();
		} else if(o instanceof JComboBox<?>){
			JComboBox<?> cb = (JComboBox<?>) o;
			result = cb.getSelectedItem().toString();
		} else if(o instanceof JCheckBox){
			JCheckBox cb = (JCheckBox)o;
			result = String.valueOf(cb.isSelected());
		}
		
		return result;
	}
	
	@Override
	public void setEnabled(boolean enabled){
		super.setEnabled(enabled);
		Set<String> names = entries.keySet();
		for(String name : names){
			Object o = entries.get(name);
			if(o instanceof JTextField){
				((JTextField)o).setEnabled(enabled);
			} else if(o instanceof JComboBox){
				((JComboBox<?>)o).setEnabled(enabled);
			}
		}
	}
}
