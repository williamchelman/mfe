package view.stat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class BooleanLine {
	public JLabel title;
	public JComboBox<String> valueTF;
	public JComboBox<String> variability;

	public BooleanLine(String name, String help,ParametersPanel pointer, boolean defaultValue) {
		super();
		title = new JLabel(name+":",JLabel.LEFT);
		title.setToolTipText(help);
		valueTF = new JComboBox<String>();
		valueTF.addItem("no");
		valueTF.addItem("yes");
		if(defaultValue){
			valueTF.setSelectedItem("yes");
		}
		variability = new JComboBox<String>();
		variability.addItem("fixed");
		variability.addItem("variates");
		variability.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pointer.placeElements();
				pointer.repaint();
				pointer.validate();
			}
		});
	}
	
	public boolean isFixed(){
		return variability.getSelectedItem().equals("fixed");
	}
	
	public boolean getValue(){
		if(valueTF.getSelectedItem().equals("no")){
			return false;
		} else {
			return true;
		}
	}
	
	public void setEnabled(boolean enabled){
		valueTF.setEnabled(enabled);
		variability.setEnabled(enabled);
	}
}
