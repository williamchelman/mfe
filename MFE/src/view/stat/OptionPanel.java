package view.stat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import controller.StatController;
import custom.CustomButton;

public class OptionPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public static final String MYSQLOutput = "MYSQL";
	
	private CustomButton startButton;
	private CustomButton stopButton;
	private StatController sc;
	private StatView sv;
	private OptionPanel self = this;
	
	private JPanel outputChoicePanel;
	private MYSQLPanel mysqlPanel;
	private IterationParameterPanel statParametersPanel;
	
	JComboBox<String> outputType;
	
	public OptionPanel(StatView sv){
		super(new GridBagLayout());
		this.sv = sv;
		
		outputChoicePanel = createOutputChoice();
		mysqlPanel = new MYSQLPanel();
		startButton = createStartButton();
		stopButton = createStopButton();
		statParametersPanel = new IterationParameterPanel();
		
		replace();
	}
	
	private CustomButton createStartButton(){
		CustomButton button = new CustomButton("Start");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!sv.showErrors()){
					sc.run();
				}
			}
		});
		return button;
	}
	
	private CustomButton createStopButton(){
		CustomButton button = new CustomButton("Stop");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sc.stop();
			}
		});
		button.setEnabled(false);
		return button;
	}
	
	private JPanel createOutputChoice(){
		JPanel panel = new JPanel();
		outputType = new JComboBox<String>();
		outputType.addItem(MYSQLOutput);
		
		outputType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				self.replace();
				self.repaint();
				sv.repaint();
				sv.validate();
			}
		});
		
		panel.add(outputType);
		return panel;
	}
	
	public String getOutputType(){
		return outputType.getSelectedItem().toString();
	}
	
	private void replace(){
		this.removeAll();
		JPanel outputPanel = new JPanel();
		outputPanel.setBorder(BorderFactory.createCompoundBorder(
	            BorderFactory.createTitledBorder("Output Parameters"),
	            BorderFactory.createEmptyBorder(5, 5, 5, 5)));
		outputPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		outputPanel.add(outputChoicePanel,c);
		
		if(outputType.getSelectedItem().equals(MYSQLOutput)){
			++c.gridy;
			outputPanel.add(mysqlPanel,c);
		} 
		
		c.gridx = 0;
		c.gridy = 0;
		this.add(outputPanel);
		
		++c.gridy;
		this.add(statParametersPanel,c);
		
		++c.gridy;
		this.add(startButton,c);
		
		++c.gridy;
		this.add(stopButton,c);
	}
	
	@SuppressWarnings("serial")
	class MYSQLPanel extends EntriesPanel {
		private final static String url = "url";
		private final static String port = "port";
		private final static String db = "Database";
		private final static String user = "Username";
		private final static String psw = "Password";
		private final static String tableSuffix = "Table suffix";
		private final static String saveHistory = "Save history";
		private final static String saveLorentz = "Save lorentz curves";
		private final static String saveAgents = "Save agents";
		private final static String saveProducts = "Save products";
		private final static String saveAgentsProducts = "Save agents products";
		private final static String saveTransactions = "Save transactions";
		private final static String savePatents = "Save patents";
		
		public MYSQLPanel() {
			super();
			this.addTextField(url, "The address of your server, for example \"localhost\" or \"example.com\" or even \"127.0.0.1\"","nephidream.com");
			this.addTextField(port, "The port number of your MYSQL server, default is 3306","3306");
			this.addTextField(db, "The name of the database you wish to push the results in","tradeoff");
			this.addTextField(user, "The name of the user that have access to the desired DB","tradeoff");
			this.addPasswordField(psw);
			this.addTextField(tableSuffix, "The suffix that will be put behind every table name","test_");
			this.addCheckBox(saveHistory, "Check if you want to save the history of the different metrics");
			this.addCheckBox(saveLorentz, "Check if you want to save the Lorentz curves for each iterations");
			this.addCheckBox(saveAgents, "Check if you want to save information about all agents for each iterations");
			this.addCheckBox(saveProducts, "Check if you want to save information about all products for each iterations");
			this.addCheckBox(saveAgentsProducts, "Check if you want to save information about all agents' relation to products (taste, skill, number sold and number bought) for each iterations");
			this.addCheckBox(saveTransactions, "Check if tou want to save all transactions on all iterations (~2.5 MB by iteration of 50.000 step)");
			this.addCheckBox(savePatents, "Check if tou want to save all patents information on all iterations");
		}
		
		public String getUrl(){
			return getEntryValue(url);
		}
		public String getPort(){
			return getEntryValue(port);
		}
		
		public String getDB(){
			return getEntryValue(db);
		}
		public String getUser(){
			return getEntryValue(user);
		}
		public String getPassword(){
			return getEntryValue(psw);
		}
		public String getTableSuffix(){
			return getEntryValue(tableSuffix);
		}
		public boolean saveHistory(){
			return Boolean.valueOf(getEntryValue(saveHistory));
		}
		public boolean saveLorentz(){
			return Boolean.valueOf(getEntryValue(saveLorentz));
		}
		public boolean saveAgents(){
			return Boolean.valueOf(getEntryValue(saveAgents));
		}
		public boolean saveProducts(){
			return Boolean.valueOf(getEntryValue(saveProducts));
		}
		public boolean saveAgentsProducts(){
			return Boolean.valueOf(getEntryValue(saveAgentsProducts));
		}
		public boolean saveTransactions(){
			return Boolean.valueOf(getEntryValue(saveTransactions));
		}
		public boolean savePatents(){
			return Boolean.valueOf(getEntryValue(savePatents));
		}
	}
	
	@SuppressWarnings("serial")
	class IterationParameterPanel extends EntriesPanel {
		private final static String steps = "Steps";
		private final static String iterations = "Iterations";
		private final static String threads = "Threads";

		public IterationParameterPanel() {
			super("Iterations parameters");
			this.addTextField(steps, "The maximum number of step the worlds will run","50000");
			this.addTextField(iterations, "The number of times a given set of parameters will be run on a world","20");
			this.addTextField(threads, "The number of threads that will be created to run worlds","2");
		}
		
		public int getThreadsNumber(){
			return Integer.valueOf(getEntryValue("Threads"));
		}
		
		public int getStepsNumber(){
			return Integer.valueOf(getEntryValue("Steps"));
		}
		
		public int getIterationNumber(){
			return Integer.valueOf(getEntryValue("Iterations"));
		}
	}
	
	
	public void setController(StatController sc){
		this.sc = sc;
	}
	
	public int getThreadsNumber(){
		return statParametersPanel.getThreadsNumber();
	}
	
	public int getStepsNumber(){
		return statParametersPanel.getStepsNumber();
	}
	
	public int getIterationNumber(){
		return statParametersPanel.getIterationNumber();
	}
	
	public String getMYSQLUrl(){
		return mysqlPanel.getUrl();
	}
	public String getMYSQLPort(){
		return mysqlPanel.getPort();
	}
	
	public String getMYSQLDB(){
		return mysqlPanel.getDB();
	}
	public String getMYSQLUser(){
		return mysqlPanel.getUser();
	}
	public String getMYSQLPassword(){
		return mysqlPanel.getPassword();
	}
	public String getMYSQLTableSuffix() {
		return mysqlPanel.getTableSuffix();
	}
	public boolean getMYSQLSaveHistory(){
		return mysqlPanel.saveHistory();
	}
	public boolean getMYSQLSaveLorentz(){
		return mysqlPanel.saveLorentz();
	}
	public boolean getMYSQLSaveAgents(){
		return mysqlPanel.saveAgents();
	}
	public boolean getMYSQLSaveProducts(){
		return mysqlPanel.saveProducts();
	}
	public boolean getMYSQLSaveAgentsProducts(){
		return mysqlPanel.saveAgentsProducts();
	}
	public boolean getMYSQLSaveTransactions(){
		return mysqlPanel.saveTransactions();
	}
	public boolean getMYSQLSavePatents(){
		return mysqlPanel.savePatents();
	}
	
	@Override
	public void setEnabled(boolean enabled){
		//super.setEnabled(enabled);
		startButton.setEnabled(enabled);
		outputChoicePanel.setEnabled(enabled);
		mysqlPanel.setEnabled(enabled);
		statParametersPanel.setEnabled(enabled);
		outputType.setEnabled(enabled);
	}
	
	public void handleStart(){
		stopButton.setEnabled(true);
		
		startButton.setEnabled(false);
		outputChoicePanel.setEnabled(false);
		mysqlPanel.setEnabled(false);
		statParametersPanel.setEnabled(false);
		outputType.setEnabled(false);
	}

	
	public void handleStop() {
		stopButton.setEnabled(false);
		
		startButton.setEnabled(true);
		outputChoicePanel.setEnabled(true);
		mysqlPanel.setEnabled(true);
		statParametersPanel.setEnabled(true);
		outputType.setEnabled(true);
		
	}

	
}
