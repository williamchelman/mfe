package view.stat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import model.stat.Stat;
import view.MainView;
import controller.StatController;

@SuppressWarnings("serial")
public class StatView extends JPanel implements Observer {
	
	private final static String numberOfProducts = "Number of products";
	private final static String numberOfAgents = "Number of agents";
	private final static String initialMoney = "Initial agent's money";
	private final static String initialUtility = "Initial agent's utility";
	private final static String initialProduction = "#Initial production";
	private final static String maxBenefits = "Maximum possible benefit";
	private final static String lastTransactions = "#Last transactions to consider";
	private final static String randomProduction = "Part of random production";
	private final static String marketRandomness = "Part of randomness of the market";
	private final static String specProducts = "#Productions for specialization";
	private final static String diminishingMarginalUtility = "Diminishing marginal utility ?";
	private final static String influenceability = "Influenceability of the agents";
	private final static String numberOfPatents = "Number of patents";
	private final static String numberOfLowcosts = "Number of lowcost agents";
	private final static String LowcostQuality = "Lowcost quality";
	
	private ParametersPanel parametersPanel;
	private OptionPanel optionPanel;
	private EvolutionPanel evolutionPanel;
	
	private Chronometer chrono = new Chronometer();
	
	private MainView mainView;
	
	public StatView(MainView mainView){
		this.initComponent();
		this.mainView = mainView;
	}
	
	
	public void initComponent(){
		optionPanel = new OptionPanel(this);
		parametersPanel = createParametersPanel();
		evolutionPanel = new EvolutionPanel();
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill=GridBagConstraints.HORIZONTAL;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(parametersPanel,gbc);
		
		gbc.gridx = 1;
		this.add(optionPanel,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 2;
		this.add(evolutionPanel,gbc);
	}
	
	private ParametersPanel createParametersPanel(){
		ParametersPanel panel = new ParametersPanel(this,"World Parameters");
		panel.addIntParameterLine(numberOfProducts, "The number of products per agent","10").setStrictlyPositive(true);
		panel.addIntParameterLine(numberOfAgents, "The number of agents in the market","50").setStrictlyPositive(true);
		panel.addDoubleParameterLine(initialMoney, "The initial amount of money an agent has when the simulation starts","500.0").setStrictlyPositive(true);
		panel.addDoubleParameterLine(initialUtility, "The initial value of utility an agent has when the simulation starts","0.0").setPositive(true);
		
		String help = ""
				+ "<html>"
				+ "The number of initial productions to do, for each agent, before the simulation<br/>"
				+ "starts (without loosing money)"
				+ "</html>";
		panel.addIntParameterLine(initialProduction, help,"10").setPositive(true);
		panel.addDoubleParameterLine(maxBenefits, "The maximum benefit of a competitive transaction","5.0").setPositive(true);
		panel.addIntParameterLine(lastTransactions, "The number of last transactions on which the next production is based","1000").setPositive(true);
		panel.addDoubleParameterLine(randomProduction, "The percentage of productions that have to be realized randomly","0.0").setPositive(true).setLowerOrEqualToOne(true);
		panel.addDoubleParameterLine(marketRandomness, "Percentage representing how much the market is random","0.0").setPositive(true).setLowerOrEqualToOne(true);
		
		help = ""
				+ "<html>"
				+ "The amount of products an agent must produce to specialize<br/>"
				+ "himself in the production of this product."
				+ "</html>";
		panel.addIntParameterLine(specProducts, help,"100").setPositive(true);
		panel.addBooleanParameterLine(diminishingMarginalUtility, "Apply the principe of diminishing marginal utility or not",false);
		panel.addDoubleParameterLine(influenceability, "The percentage representative of how much an agent's tastes are modified by his neighbours' tastes","0.0").setPositive(true).setLowerOrEqualToOne(true);
		panel.addIntParameterLine(numberOfPatents, "The number of patents that are distributed at the beginning of the simulation", "0").setPositive(true);
		panel.addIntParameterLine(numberOfLowcosts, "The number of agents that produce low quality products", "0").setPositive(true);
		panel.addDoubleParameterLine(LowcostQuality, "The quality of the low cost products", "1").setStrictlyPositive(true).setLowerOrEqualToOne(true);
		return panel;
	}
	
	private NumericalLine getNumericalParameter(String name){
		return parametersPanel.getNumericalLine(name);
	}
	
	private BooleanLine getBooleanParameter(String name){
		return parametersPanel.getBooleanLine(name);
	}
	
	public boolean showErrors(){
		if(!parametersPanel.areParametersValid()){
			String mess=new String("Some fields are invalid").toUpperCase();
			JOptionPane.showMessageDialog(this,mess,"ERROR", JOptionPane.ERROR_MESSAGE);
			return true;
		}
		return false;
	}
	
	class EvolutionPanel extends JPanel{
		private JProgressBar completion;
		private JLabel estimatedRemainingTimeTitle;
		private JLabel estimatedRemainingTime;
		
		public EvolutionPanel(){
			completion = new JProgressBar(0,100);
			completion.setStringPainted(true);
			
			estimatedRemainingTimeTitle = new JLabel("Estimated remaining time:");
			estimatedRemainingTime = new JLabel();
			
			replace();
		}
		
		public void setRemainingTime(long remaingTime){
			if(remaingTime >= 0){
				int hours = (int) Math.floor(1.0*remaingTime/(1000*60*60));
				int minutes = (int) Math.floor(1.0*remaingTime/(1000*60) - 60*hours);
				int seconds = (int) Math.floor(1.0*remaingTime/1000 - 60*minutes - 60*60*hours);
				estimatedRemainingTime.setText(hours + "h" + minutes + "m" + seconds + "s");
			} else {
				estimatedRemainingTime.setText("Estimated after the first iteration");
			}
			
		}
		
		public void setProgression(double percentageDone){
			completion.setValue((int)Math.round(percentageDone));
			completion.setString(percentageDone+"%");
		}
		
		
		
		public void replace(){
			this.removeAll();
			GridBagConstraints gbc = new GridBagConstraints();
			
			gbc.gridwidth = 2;
			gbc.gridx= 0;
			gbc.gridy = 0;
			this.add(completion,gbc);
			
			gbc.gridwidth = 1;
			gbc.gridx= 0;
			gbc.gridy = 1;
			this.add(estimatedRemainingTimeTitle,gbc);
			
			gbc.gridwidth = 1;
			gbc.gridx= 1;
			gbc.gridy = 1;
			this.add(estimatedRemainingTime,gbc);
		}
	}
	
	public void attachController(StatController sc){
		optionPanel.setController(sc);
	}

	public NumericalLine getNumberOfProducts() {
		return getNumericalParameter(numberOfProducts);
	}


	public NumericalLine getNumberOfAgents() {
		return getNumericalParameter(numberOfAgents);
	}


	public NumericalLine getNumberOfLastTransactions() {
		return getNumericalParameter(lastTransactions);
	}


	public NumericalLine getInfluenceability() {
		return getNumericalParameter(influenceability);
	}
	
	public NumericalLine getPatentNumber() {
		return getNumericalParameter(numberOfPatents);
	}
	
	public NumericalLine getLowCostNumber() {
		return getNumericalParameter(numberOfLowcosts);
	}
	
	public NumericalLine getLowCostQuality() {
		return getNumericalParameter(LowcostQuality);
	}

	public NumericalLine getInitialMoneyAgent() {
		return getNumericalParameter(initialMoney);
	}


	public NumericalLine getInitialProduction() {
		return getNumericalParameter(initialProduction);
	}


	public NumericalLine getInitialUtilityAgent() {
		return getNumericalParameter(initialUtility);
	}

	public NumericalLine getMarketRandomness() {
		return getNumericalParameter(marketRandomness);
	}


	public NumericalLine getMaxBenefit() {
		return getNumericalParameter(maxBenefits);
	}


	public NumericalLine getSpecializedProduction() {
		return getNumericalParameter(specProducts);
	}


	public NumericalLine getPartRandomProduction() {
		return getNumericalParameter(randomProduction);
	}


	public BooleanLine getDiminishingMarginalUtility() {
		return getBooleanParameter(diminishingMarginalUtility);
	}
	
	public OptionPanel getOptionPanel(){
		return optionPanel;
	}
	
	@Override
	public void setEnabled(boolean enabled){
		//super.setEnabled(enabled);
		mainView.getMenuView().setEnabled(enabled);
		parametersPanel.setEnabled(enabled);
		optionPanel.setEnabled(enabled);
	}


	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg0 instanceof Stat){
			Stat s = (Stat)arg0;
			evolutionPanel.setProgression(s.getPercentageDone());
			chrono.remainingTime = s.getEstimatedRemainingTime();
			if(s.isFinished()){
				handleStop();
			}
			
			this.repaint();
			this.validate();
		}
		
	}

	public void error(Exception e){
		String mess=new String(e.getMessage());
		String title = "Error";
		if(e instanceof SQLException){
			title = "MYSQL Error";
		}
		JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
		this.requestFocus();
	}
	
	public void handleStart(){
		mainView.getMenuView().setEnabled(false);
		parametersPanel.setEnabled(false);
		optionPanel.handleStart();
		chrono = new Chronometer();
		chrono.start();
	}
	
	public void handleStop() {
		mainView.getMenuView().setEnabled(true);
		parametersPanel.setEnabled(true);
		optionPanel.handleStop();
		chrono.stop = true;
	}
	
	class Chronometer extends Thread{
		public int refreashTime = 1000;
		public long remainingTime = 0;
		public boolean stop = false;
		
		@Override
		public void run(){
			remainingTime = 0;
			while(!stop){
				try {
					Thread.sleep(refreashTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				remainingTime -= refreashTime;
				evolutionPanel.setRemainingTime(remainingTime);
				
			}
		}
	}
}
