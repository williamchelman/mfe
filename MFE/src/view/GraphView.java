package view;


import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
//import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import controller.Mode;
/**
 * The view displaying several graphs
 * @author nicolasbernier
 *
 */
public class GraphView extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int NUM_GRAPH=4;
	private JLabel title1,title2;
	//List of graphics
	private ArrayList<Graph> graphs=new ArrayList<Graph>();
	//The parent view
	private MainView view;
	//12 Colors
	public static final Color[] COLORS={
		new Color(0x0165C7),
		new Color(0x177230),
		new Color(0xF6492C),
		new Color(114,89,174),
		new Color(0x1CA9F9),
		new Color(0x39CDA8),
		new Color(0xFF7E00),
		new Color(0xCD6090),
		new Color(0x00D2FF),
		new Color(0x64D438),
		new Color(0xFECB1B),
		new Color(0xE7007D),
		new Color(0x0229DF),
		new Color(0x44A438),
		new Color(0xFFED3B),
		new Color(0xA2007D)
	};
	/**
	 * Controller
	 * @param view - parent view
	 */
	public GraphView(MainView view){
		this.view=view;
		this.constructSingle();
	}
	/**
	 * Construct the view in single mode
	 */
	public void constructSingle(){
		this.setLayout(new GridLayout(GraphView.NUM_GRAPH/2,2,10,10));
		this.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		for(int i=0;i<GraphView.NUM_GRAPH;i++){
			Graph graph=new Graph(this);
			graph.addMouseMotionListener(graph);
			graph.addMouseListener(graph);
			this.graphs.add(graph);
			this.add(graph);
			
		}
	}
	/**
	 * Construct the view in comparison mode
	 */
	public void constructComparison(){
		JPanel titles=new JPanel(new GridLayout(1,2,10,10));
		this.title1=new JLabel("Word 1");
		Font f=new Font(MainView.FONT, Font.BOLD, 20);
		title1.setFont(f);
		title1.setForeground(new Color(230,230,230));
		title1.setHorizontalAlignment(SwingConstants.CENTER);
		this.title2=new JLabel("Word 2");
		title2.setForeground(new Color(230,230,230));
		title2.setFont(f);
		title2.setHorizontalAlignment(SwingConstants.CENTER);
		titles.add(title1);
		titles.add(title2);

		JPanel graphsPanel=new JPanel(new GridLayout(GraphView.NUM_GRAPH/2,2,10,10));
		graphsPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		for(int i=0;i<GraphView.NUM_GRAPH;i++){
			Graph graph=new Graph(this);
			graph.addMouseMotionListener(graph);
			graph.addMouseListener(graph);
			this.graphs.add(graph);
			graphsPanel.add(graph);
		}
		titles.setBackground(MainView.GRAPH_COLOR);
		graphsPanel.setBackground(MainView.GRAPH_COLOR);

		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.add(titles);
		this.add(graphsPanel);
	}
	
	
	/**
	 * Construct the view depending on the mode
	 */
	public void constructPanel(Mode mode){
		this.removeAll();
		this.graphs.clear();
		if(mode == Mode.comparison){
			this.constructComparison();
		} else if(mode == Mode.single){
			this.constructSingle();
		} 
	}
	/**
	 * Get the graphs of this view
	 * @return the graphs
	 */
	public ArrayList<Graph> getGraphs(){
		return graphs;
	}
	/**
	 * Set the titles of the two worlds compared in comparison mode
	 * @param world1 - the first World
	 * @param world2 - the second World
	 */
	public void setTitles(String world1,String world2){
		this.title1.setText(world1.toUpperCase());
		this.title2.setText(world2.toUpperCase());
	}
	/**
	 * Update the graphs when world changes
	 */
	public void updateGraph(){
		this.repaintGraphs();
		this.changeSliderGraduation();
	}
	/*
	 * Specific function to repaint the graph
	 */
	private void repaintGraphs(){
		
		for(Graph graph:this.graphs){
			graph.updateView();
		}
		
	}
	/**
	 * When the graph are painted, the control panel has to adapt its slider graduation
	 */
	public void notifyRepaint(){
		this.changeSliderGraduation();
	}
	/**
	 * Change the slider graduation (eventually) depending on the graph size
	 */
	public void changeSliderGraduation(){
		for(int i=0;i<this.graphs.size();i++){
			if(!this.graphs.get(i).isAllGraduation()){
				Integer numGraduation=this.graphs.get(i).getNumGraduation();
				Integer size=this.graphs.get(i).getSizeDatas();
				
				this.view.getControlPanel().addSliderGraduation(numGraduation,size,Graph.NUM_STEP_TO_DISPLAY);
				break;
			}
		}
	}
	/**
	 * Tell the amount of step wanted to be displayed 
	 * @param numStep - the number of steps
	 */
	public void setNumStepToDisplay(Integer numStep){
		Graph.NUM_STEP_TO_DISPLAY=numStep;
		this.repaintGraphs();
	}
	/**
	 * Get graphs colors
	 * @return
	 */
	public HashMap<String, Color> getGraphColors() {
		HashMap<String, Color> colors=new HashMap<String, Color>();
		for(Graph g:this.graphs){
			if(g.getColor()!=null && g.getTitle()!=null){
				colors.put(g.getTitle(), g.getColor());
			}
		}
		return colors;
	}
}
