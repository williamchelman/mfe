package view;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Dictionary;
import java.util.EventObject;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controller.Mode;
import controller.WorldController;
import custom.CustomButton;
import custom.CustomSlider;
/**
 * Control view that allows to control the main characteristics of the world(s) currently running
 * @author nicolasbernier
 *
 */
public class ControlView extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String SLIDER_LABEL="Market randomness (%)";
	private JSlider slider,slider2,sliderGraduation;
	private static final int MAX=100;
	private static final int MIN=0;
	private static final int DEFAULT=0;
	
	private GridBagConstraints c;
	private JPanel grid;
	private JFormattedTextField numberSteps;
	private CustomButton buttonStart,buttonPause,buttonReset;
	private int gridy=0;
	private ControlView self;
	private JLabel worldTitle,worldTitle2;
	private JLabel currentStep,labelSliderGraduation;
	private Boolean hasStarted=false;
	private MainView parent;
	private WorldController controller;
	/**
	 * Constructor
	 * @param parent - parent View 
	 */
	public ControlView(MainView parent){
		
		this.parent=parent;
		//initiate the layout
		this.self=this;
		this.setLayout(new BorderLayout());
		this.constructSingle();	
	}
	/**
	 * Construct the view depending on the mode
	 * @param comparison - tell if the construction has to be made in comparison mode or not
	 */
	public void constructPanel(Mode mode){
		this.removeAll();
		if(mode == Mode.comparison){
			this.constructComparison();
		} else if (mode == Mode.single){
			this.constructSingle();
		} 
	}
	/**
	 * Construction mode single
	 */
	private void constructSingle(){
		this.construct(Mode.single);
	}
	/**
	 * Construction mode comparison
	 */
	private void constructComparison(){
		this.construct(Mode.comparison);
	}
	
	/**
	 * Construction of the panel
	 * @param comparison
	 */
	public void construct(Mode mode){
		this.c=new GridBagConstraints();
		this.grid=new JPanel(new GridBagLayout());
		grid.setBackground(MainView.CONTROL_COLOR);
		c.anchor=GridBagConstraints.NORTH;
		this.setBorder(new EmptyBorder(36, 20, 50, 20 ));
		Font fontTitle=null,fontSubTitle=null,fontText = null,fontTextBold=null;
		JLabel labelTitle=null,labelSlider=null;
		//Construction in comparison mode
		if(mode == Mode.comparison){
			//Fonts
			fontTitle = new Font(MainView.FONT, Font.BOLD, 21);
			fontSubTitle = new Font(MainView.FONT, Font.PLAIN, 19);
			fontText = new Font(MainView.FONT, Font.PLAIN, 17);
			fontTextBold = new Font(MainView.FONT, Font.BOLD, 17);
			
			labelTitle=new JLabel("Simulation Parameters");
			labelTitle.setFont(fontTitle);
			labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
			labelTitle.setBorder(new EmptyBorder(0,0,25,0));
			
			//Slider
			labelSlider=new JLabel(SLIDER_LABEL);
			labelSlider.setFont(fontSubTitle);
			labelSlider.setForeground(MainView.COLOR_TEXT);
			labelSlider.setAlignmentX(Component.CENTER_ALIGNMENT);	
			labelSlider.setBorder(new EmptyBorder(0,0,20,0));
			
			this.worldTitle=new JLabel();
			this.worldTitle.setForeground(MainView.COLOR_TEXT);
			this.worldTitle.setFont(fontTextBold);
			this.worldTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.worldTitle.setBorder(new EmptyBorder(0,5,0,5));
			
			this.slider=new CustomSlider(JSlider.HORIZONTAL,MIN, MAX, DEFAULT);
			this.slider.setMajorTickSpacing(25);
			this.slider.setMinorTickSpacing(5);
			this.slider.setBorder(new EmptyBorder(0, 0, 10, 0));
			
			this.worldTitle2=new JLabel();
			this.worldTitle2.setForeground(MainView.COLOR_TEXT);
			this.worldTitle2.setFont(fontTextBold);
			this.worldTitle2.setAlignmentX(Component.CENTER_ALIGNMENT);
			this.worldTitle2.setBorder(new EmptyBorder(0,5,0,5));
	
			this.slider2=new CustomSlider(JSlider.HORIZONTAL,MIN, MAX, DEFAULT);
			this.slider2.setMajorTickSpacing(25);
			this.slider2.setMinorTickSpacing(5);
			this.slider2.setBorder(new EmptyBorder(0, 0, 10, 0));
			
			this.addSliderListener(this.slider,1);
			this.addSliderListener(this.slider2,2);
		}
		//Construction in single mode
		else if(mode == Mode.single){
			
			//Fonts
			Font fontWorld = new Font(MainView.FONT, Font.PLAIN, 30);
			fontTitle = new Font(MainView.FONT, Font.BOLD, 21);
			fontText = new Font(MainView.FONT, Font.PLAIN, 18);
			fontTextBold = new Font(MainView.FONT, Font.BOLD, 18);

			//World Title
			this.worldTitle=new JLabel();
			this.worldTitle.setForeground(new Color(230,230,230));
			this.worldTitle.setFont(fontWorld);
			this.worldTitle.setHorizontalAlignment(SwingConstants.CENTER);
			this.worldTitle.setBorder(new EmptyBorder(0, 6, 35, 6));
			//add other buttons
			labelTitle=new JLabel("Simulation Parameters");
			labelTitle.setFont(fontTitle);
			labelTitle.setForeground(MainView.COLOR_TEXT);
			labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
			labelTitle.setBorder(new EmptyBorder(0,0,20,0));
			//Slider
			labelSlider=new JLabel(SLIDER_LABEL);
			labelSlider.setFont(fontText);
			labelSlider.setForeground(MainView.COLOR_TEXT);
			labelSlider.setAlignmentX(Component.CENTER_ALIGNMENT);		
			
			this.slider=new CustomSlider(JSlider.HORIZONTAL,MIN, MAX, DEFAULT);
			this.slider.setMajorTickSpacing(25);
			this.slider.setMinorTickSpacing(5);
			this.slider.setBorder(new EmptyBorder(10, 0, 10, 0));
			
			this.addSliderListener(this.slider,1);
		}
		//Other buttons
		
		//input
		JLabel labelSteps=new JLabel("Simulation speed");
		labelSteps.setFont(fontText);
		labelSteps.setForeground(MainView.COLOR_TEXT);
		labelSteps.setHorizontalAlignment(SwingConstants.RIGHT);
		this.numberSteps = new JFormattedTextField(NumberFormat.getNumberInstance());
		this.numberSteps.setValue(0);
		this.numberSteps.setColumns(4);
		this.numberSteps.setFont(fontText);
		this.numberSteps.setForeground(MainView.COLOR_TEXT);
		this.numberSteps.setBorder(BorderFactory.createMatteBorder(2,0,0, 0,MainView.COLOR_INPUT));
		this.numberSteps.setPreferredSize(new Dimension(80,25));
		this.numberSteps.setHorizontalAlignment(JFormattedTextField.CENTER);
		InfoComponent info=new InfoComponent("<html>"
				+ "This number represents the <b>speed</b> of the simulation. Enter a value between <b>20</b> (slowest) and <b>999</b> (fastest).<br/>"
				+ "This speed can decrease in some market configurations. Furthermore, in comparison mode, due to the<br/> "
				+ "synchronization of the two markets, the execution of one market can be delayed with the aim to be<br/>"
				+ "resynchronized with the other one."
				+ "<html>", this.parent);
		info.addMouseListener(info);
		//Control Buttons

		//PAUSE RESET
		this.buttonPause=new CustomButton("PAUSE");
		this.buttonPause.setPreferredSize(new Dimension(80,35));
		
		this.buttonReset=new CustomButton("RESET");
		this.buttonReset.setPreferredSize(new Dimension(80,35));

		//START
		this.buttonStart=new CustomButton("START");
		this.buttonStart.setPreferredSize(new Dimension(200,35));
		
		JLabel labelCurrentStep=new JLabel("Current step:");
		labelCurrentStep.setFont(fontText);
		labelCurrentStep.setHorizontalAlignment(SwingConstants.RIGHT);
		labelCurrentStep.setBorder(new EmptyBorder(10,0,10,0));
		this.currentStep=new JLabel("0");
		this.currentStep.setForeground(MainView.COLOR_TEXT);
		this.currentStep.setFont(fontTextBold);
		this.currentStep.setHorizontalAlignment(SwingConstants.CENTER);
		this.currentStep.setBorder(new EmptyBorder(10,0,10,0));
		//
		//Final slider
		//Slider
		this.labelSliderGraduation=new JLabel("Number steps on graphs");
		this.labelSliderGraduation.setFont(fontTextBold);
		this.labelSliderGraduation.setForeground(MainView.COLOR_TEXT);
		this.labelSliderGraduation.setAlignmentX(Component.CENTER_ALIGNMENT);		
		this.labelSliderGraduation.setForeground(MainView.COLOR_DISABLED);
		
		this.sliderGraduation=new CustomSlider(JSlider.HORIZONTAL);//,MIN, MAX, DEFAULT);
		this.sliderGraduation.setValue(0);
		this.sliderGraduation.setBorder(new EmptyBorder(10, 0, 10, 0));
		
		this.addSliderGraduationListener();
		//2) Place the elements
		c.gridx = 0;
		
		c.gridy = this.gridy;
		c.gridwidth=3;
		if(mode == Mode.comparison){
			grid.add(labelTitle,c);
			
			this.gridy++;
			c.gridy = this.gridy;
			grid.add(labelSlider,c);
			this.gridy++;
			
			c.gridy = this.gridy;
			grid.add(this.worldTitle,c);
			this.gridy++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridy = this.gridy;
			grid.add(this.slider, c);
			this.gridy++;
			
			c.fill=GridBagConstraints.NONE;
			c.gridy = this.gridy;
			grid.add(this.worldTitle2,c);
			this.gridy++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridy = this.gridy;
			grid.add(this.slider2, c);
			this.gridy++;
		}
		else if(mode == Mode.single){
			grid.add(worldTitle,c);
			this.gridy++;
			
			c.gridy = this.gridy;
			grid.add(labelTitle,c);
			
			this.gridy++;
			c.gridy = this.gridy;
			grid.add(labelSlider,c);
			this.gridy++;
			
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridy = this.gridy;
			grid.add(this.slider, c);
			this.gridy++;
		}
		
		//Other button
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor=GridBagConstraints.CENTER;
		c.weightx=0.4;
		c.gridy = this.gridy;
		c.gridwidth=1;
		grid.add(labelSteps,c);
		c.gridx=1;
		c.insets=new Insets(0,10,0,5);
		grid.add(this.numberSteps,c);
		c.gridx=2;
		grid.add(info,c);
		this.gridy++;
		c.insets=new Insets(20,0,0,3);
		c.gridy = this.gridy;
		c.gridx=0;
		grid.add(this.buttonPause,c);
		c.insets=new Insets(20,5,0,0);
		c.gridwidth=2;
		c.gridx=1;
		grid.add(this.buttonReset,c);
		this.gridy++;
		c.insets=new Insets(10,0,0,0);
		
		c.gridy = this.gridy;
		c.gridx=0;
		c.gridwidth=3;
		grid.add(this.buttonStart,c);
		this.gridy++;
		
		c.weightx=1;
		c.gridwidth=1;
		c.gridy=this.gridy;
		c.anchor=GridBagConstraints.CENTER;
		c.fill=GridBagConstraints.BOTH;
		grid.add(labelCurrentStep,c);
		c.gridx=1;
		c.gridwidth=2;
		grid.add(this.currentStep,c);
		this.gridy++;
		c.gridwidth=3;
		c.gridx=0;
		c.gridy = this.gridy;
		c.fill=GridBagConstraints.NONE;
		c.anchor=GridBagConstraints.CENTER;
		grid.add(this.labelSliderGraduation,c);
		this.gridy++;
		//============
		this.add(grid,BorderLayout.NORTH);
	}
	/**
	 * Update/Add a slider for the amount of the data displayed in the graphs
	 * @param graduation - the number of graduations displayed 
	 * @param size - the total amount of data in the graphs 
	 */
	public void addSliderGraduation(Integer graduation,Integer size,int numStepDisplayed){
		if(size<=graduation){
			this.resetGraduation();
			return;
		}
		if(!this.hasStarted){
			this.hasStarted=true;
			//Final slider
			c.fill=GridBagConstraints.HORIZONTAL;
			c.gridy = this.gridy;
			grid.add(this.sliderGraduation, c);
			grid.repaint();
			this.gridy++;
			this.labelSliderGraduation.setForeground(MainView.COLOR_TEXT);
		}
		//Compute min/max and trick
		int min=(int) Math.ceil(graduation/25.0)*25;
		int max=(int) Math.round(size*1.5/100.0)*100;
		int prevMin=this.sliderGraduation.getMinimum();
		int prevMax=this.sliderGraduation.getMaximum();
		if(prevMin!=min || prevMax!=max){
			int majorTick=(int) Math.ceil((max-min)/(3.0*25))*25;
			int minorTick=(int) Math.ceil((max-min)/(3.0*4.0*25))*25;
			this.sliderGraduation.setMinimum(min);
			this.sliderGraduation.setMaximum(max);
			//this.sliderGraduation.setMajorTickSpacing(majorTick);
			this.sliderGraduation.setMinorTickSpacing(minorTick);
			//set the dictionary of labels
			Dictionary<Integer, JLabel> graduations = new Hashtable<Integer, JLabel>();
	
			graduations.put(Math.round(min), new JLabel(String.valueOf(min)));
			graduations.put(Math.round(min+majorTick), new JLabel(String.valueOf(min+majorTick)));
			graduations.put(Math.round(max-majorTick), new JLabel(String.valueOf(max-majorTick)));
			graduations.put(Math.round(max), new JLabel(String.valueOf(max)));
			this.sliderGraduation.setLabelTable(graduations);
		}
	}
	/**
	 * Action called when the value of the slider has changed
	 * @param e
	 */
	public void activeSlider(EventObject e){
		JSlider source = (JSlider)e.getSource();
		//if(!source.getValueIsAdjusting()){
			int value = (int)source.getValue();	
        	self.parent.getGraphPanel().setNumStepToDisplay(value); 
		//}
	}
	/**
	 * Add a listener to the slider graduation
	 */
	public void addSliderGraduationListener(){
		/*this.mouseListener=new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				activeSlider(e);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				activeSlider(e);
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				activeSlider(e);
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {
				//... 
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				activeSlider(e);
			}
		};
		this.sliderGraduation.addMouseListener(this.mouseListener);*/
		
		this.sliderGraduation.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				self.activeSlider(e);
			}
		});
	}
	/**
	 * Set the number of steps to display
	 * @param numStep - the number of steps
	 */
	public void setNumStepToDisplay(Integer numStep){
		this.hasStarted=false;
		if(this.sliderGraduation.getMinimum()<numStep || numStep<this.sliderGraduation.getMaximum()){
			this.sliderGraduation.setValue(numStep);
			/*MouseEvent m = new MouseEvent(this.sliderGraduation, // which
				    MouseEvent.MOUSE_CLICKED, // what
				    System.currentTimeMillis(), // when
				    0, // no modifiers
				    this.sliderGraduation.getLocation().x,this.sliderGraduation.getLocation().y, // where: at (10, 10}
				    1, // only 1 click 
				    false); // not a popup trigger
			this.sliderGraduation.dispatchEvent(m);//*/
		}
		this.parent.getGraphPanel().setNumStepToDisplay(numStep);
	}
	/**
	 * Add a listener a given slider controlling the world randomness 
	 * @param s -  the slider concerned
	 * @param worldTargetted - the world targeted (1 or 2)
	 */
	public void addSliderListener(JSlider s,final Integer worldTargetted){
		s.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider)e.getSource();
		        if (!source.getValueIsAdjusting()) {
		            int value = (int)source.getValue();	
		            self.controller.handleCursor(value,worldTargetted);
		        }   
			}
		});
	}
	/**
	 * Add listener on the start button
	 */
	public void addStartListener(){		
		this.buttonStart.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer val=((Number)self.numberSteps.getValue()).intValue();
				if(!self.controllValue(val)){
					self.showErrorSteps();
				}
				else{
					controller.handleStart(val);
				}
			}
		});
	}
	/**
	 * Add listener on the reset button
	 */
	public void addResetListener(){
		this.buttonReset.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//handle 
				Integer val=((Number)self.numberSteps.getValue()).intValue();
				boolean control=false;
				if(control && !self.controllValue(val)){
					self.showErrorSteps();
				}
				else{
					self.controller.handleReset(val,control);
				}
			}
		});
	}
	/**
	 * Add listener on the pause button
	 */
	public void addPauseListener(){
		this.buttonPause.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				//handle 
				controller.handlePause();
			}
		});
	}
	/**
	 * remove the graduation slider when it does not have to be displayed
	 */
	public void resetGraduation(){
		this.grid.remove(this.sliderGraduation);
		this.grid.repaint();
		this.labelSliderGraduation.setForeground(MainView.COLOR_DISABLED);
		this.hasStarted=false;
	}
	/**
	 * Show a error message when the number of steps entered is too high
	 */
	public void showErrorSteps(){
		String mess=new String("Your entered a invalid number of steps. This should be value between 20 and 999").toUpperCase();
		JOptionPane.showMessageDialog(this.parent,mess,"ERROR IN NUMBER OF STEPS", JOptionPane.ERROR_MESSAGE);
	}
	/**
	 * Control if a certain value is in the range [20,1000]
	 * @param value - the value to control
	 * @return true or false
	 */
	public boolean controllValue(Integer value){
		if(value>=1000 || value<20){
			return false;
		}
		return true;
	}
	/**
	 * Reinitialize some parameters of the control panel: the market randomness, the title and the current step
	 * @param percent
	 * @param name
	 * @param currentStep
	 */
	public void reinit(Double percent,String name,int currentStep){
		
		this.setMainRandomness(percent);
		this.setTitle(name);
		this.setCurrentStep(currentStep);
		//other initialization have to be added here
		//...
	}
	/**
	 * Set the title of the main world running in single mode
	 * @param world1 - the title
	 */
	public void setTitle(String world1){
		this.worldTitle.setText(world1.toUpperCase());
	}
	/**
	 * Set the titles of the worlds running in comparison mode
	 * @param world1 - the title of the first world
	 * @param world2 - the title of the second world
	 */
	public void setTitles(String world1,String world2){
		this.setTitle(world1);
		this.worldTitle2.setText(world2.toUpperCase());
	}
	/**
	 * Set the market randomness of the main world running in single mode
	 * @param d - the randomness value
	 */
	public void setMainRandomness(Double d){
		d*=100;
		this.slider.setValue(d.intValue());
	}
	/**
	 * Set the market randomness of the worlds running in comparison mode
	 * @param d1 - the randomness value of the first world
	 * @param d2 - the randomness value of the second world
	 */
	public void setRandomness(Double d1,Double d2){
		this.setMainRandomness(d1);
		d2*=100;
		this.slider2.setValue(d2.intValue());
	}
	/**
	 * Change text of the button start (not used currently) 
	 * @param txt
	 */
	public void changeContentStrat(String txt){
		this.buttonStart.setText(txt);
	}
	/**
	 * Set the current step value
	 * @param currentStep - the current step
	 */
	public void setCurrentStep(int currentStep){
		this.currentStep.setText(String.valueOf(currentStep));
	}
	/**
	 * Attach a controller to this view
	 * @param worldController - the controller
	 */
	public void setController(WorldController worldController) {
		this.controller=worldController;
	}
	/**
	 * Update the current step value, this function is called by its parent when the World running notifies the view
	 * @param value
	 */
	public void updatePanel(Integer value){
		this.setCurrentStep(value);
	}
}
