package view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.UserGlobalController;
import custom.CustomButton;
/**
 * Dialog used to edit/create a world
 * @author nicolasbernier
 *
 */
public class WorldDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		
	//The different fields used to edit/create a world
	private JLabel numberOfProducts
		,numberOfAgents
		,initialProduction
		,maxBenefit
		,numberOfLastTransactions
		,initialMoneyAgent
		,initialUtilityAgent
		,specializedProduction
		,partRandomProduction
		,decreasingUtility
		,influenceability
		,patentNumber;
	private PlaceholderTextField nameTF;
	private JFormattedTextField numberOfProductsTF
		,numberOfAgentsTF
		,initialProductionTF
		,maxBenefitTF
		,numberOfLastTransactionsTF
		,initialMoneyAgentTF
		,initialUtilityAgentTF
		,specializedProductionTF
		,partRandomProductionTF
		,influenceabilityTF
		,patentNumberTF;
	private JCheckBox decreasingUtilityCB;

	private String oldName="";
	
	private CustomButton save,cancel;
	
	private Font text,input,inputTitle;
	
	private int gridy=0;
		
	private JPanel grid;
	private GridBagConstraints c;
	
	private UserGlobalController controller;
	
	private boolean isEdit=false;
	
	private WorldDialog self;
	/**
	 * Constructor
	 * @param window - parent window
	 * @param title - title of the dialog
	 */
	public WorldDialog(Window window, String title){
	    super(window, title);
	    this.self=this;
	    this.setResizable(false);
	    this.text=new Font(MainView.FONT,Font.BOLD, 16);
		this.input=new Font(MainView.FONT,Font.PLAIN, 16);
		this.inputTitle=new Font(MainView.FONT,Font.BOLD, 20);
		this.initComponent();
	}
	/**
	 * Attach a controller to the dialog
	 * @param controller - the given controller
	 * @return this
	 */
	public WorldDialog attachController(UserGlobalController controller){
		this.controller=controller;
		return this;
	}
	/**
	 * Create a label for an input 
	 * @param label - the JLabel 
	 * @param name - the name of the label
	 * @return the label created
	 */
	public JLabel createLabelTitle(JLabel label,String name){
		label=new JLabel(name);
		label.setFont(text);
		label.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 20));
		return label;
	}
	/**
	 * Create an input having a placeholder
	 * @param textfield - the given input
	 * @param name - the placeholder value
	 * @return the created input
	 */
	public PlaceholderTextField createInput(PlaceholderTextField textfield,String name){
		textfield=new PlaceholderTextField();
		textfield.setPlaceholder(name);
		textfield.setBorder(BorderFactory.createMatteBorder(2, 0,0,0,MainView.COLOR_INPUT));
		textfield.setFont(this.inputTitle);
		textfield.setColumns(15);
		textfield.setHorizontalAlignment(JFormattedTextField.CENTER);
		return textfield;
	}
	/**
	 * Create an formatted input
	 * @param input - the input
	 * @return the formatted input
	 */
	public JFormattedTextField createFormattedInput(JFormattedTextField input){
		input=new JFormattedTextField(NumberFormat.getNumberInstance());
		input.setFont(this.input);
		input.setBorder(BorderFactory.createMatteBorder(2,0,0,0,MainView.COLOR_INPUT));
		input.setHorizontalAlignment(JFormattedTextField.CENTER);
		input.setColumns(5);
		return input;
	}
	/**
	 * Initialize the dialog view
	 */
	public void initComponent(){
		
		//1) Create all the elements
		Color bg=new Color(215,215,215);
		InfoComponent numbProductsInfo,numbAgentsInfo,initialMoneyAgentInfo,initialUtilityInfo,
		initialProductionInfo,maxBenefitInfo,numberOfLastTransactionsInfo,partRandomInfo,specializeProductionInfo,decreasingUtilityInfo,
		influenceabilityInfo,patentNumberInfo,patentLifetimeInfo;
		//this.name=this.createLabelTitle(this.name,"World name:");
		this.nameTF=this.createInput(this.nameTF,"World Name...");

		this.numberOfProducts=this.createLabelTitle(this.numberOfProducts,"Number of products:");
		numbProductsInfo=new InfoComponent("The number of products per agent",this);
		this.numberOfProductsTF=this.createFormattedInput(this.numberOfProductsTF);
		
		this.numberOfAgents=this.createLabelTitle(this.numberOfAgents,"Number of agents:");
		numbAgentsInfo=new InfoComponent("The number of agents in the market",this);
		this.numberOfAgentsTF=this.createFormattedInput(this.numberOfAgentsTF);
		
		this.initialMoneyAgent=this.createLabelTitle(this.initialMoneyAgent,"Initial agent's money:");
		initialMoneyAgentInfo=new InfoComponent("The initial amount of money an agent has when the simulation starts",this);
		this.initialMoneyAgentTF=this.createFormattedInput(this.initialMoneyAgentTF);
		
		this.initialUtilityAgent=this.createLabelTitle(this.initialUtilityAgent,"Initial agent's utility:");
		initialUtilityInfo=new InfoComponent("The initial value of utility an agent has when the simulation starts",this);
		this.initialUtilityAgentTF=this.createFormattedInput(this.initialUtilityAgentTF);
		
		this.initialProduction=this.createLabelTitle(this.initialProduction,"#Initial production:");
		initialProductionInfo=new InfoComponent(""
				+ "<html>"
				+ "The number of initial productions to do, for each agent, before the simulation<br/>"
				+ "starts (without loosing money)"
				+ "</html>",this);
		this.initialProductionTF=this.createFormattedInput(this.initialProductionTF);
		
		this.maxBenefit=this.createLabelTitle(this.maxBenefit,"Maximum possible benefit:");
		maxBenefitInfo=new InfoComponent("The maximum benefit of a competitive transaction",this);
		this.maxBenefitTF=this.createFormattedInput(this.maxBenefitTF);
		
		this.numberOfLastTransactions=this.createLabelTitle(this.numberOfLastTransactions,"#Last transactions to consider:");
		numberOfLastTransactionsInfo=new InfoComponent("The number of last transactions on which the next production is based",this);
		this.numberOfLastTransactionsTF=this.createFormattedInput(this.numberOfLastTransactionsTF);
		
		this.partRandomProduction=this.createLabelTitle(this.partRandomProduction,"Part of random production:");
		partRandomInfo=new InfoComponent("The percentage of productions that have to be realized randomnly",this);
		this.partRandomProductionTF=this.createFormattedInput(this.partRandomProductionTF);
		
		this.specializedProduction=this.createLabelTitle(this.specializedProduction,"#Productions for specialization:");
		specializeProductionInfo=new InfoComponent(""
				+ "<html>"
				+ "The amount of products an agent must produce to specialize<br/>"
				+ "himself in the production of this product."
				+ "</html>",this);
		this.specializedProductionTF=this.createFormattedInput(this.specializedProductionTF);
		
		this.decreasingUtility=this.createLabelTitle(this.decreasingUtility,"Diminishing marginal utility ?");
		decreasingUtilityInfo=new InfoComponent("Apply the principe of diminishing marginal utility or not",this);
		this.decreasingUtilityCB=new JCheckBox();
		this.decreasingUtilityCB.setBackground(bg);
		this.decreasingUtilityCB.setSelected(false);
		this.decreasingUtilityCB.setHorizontalAlignment(JFormattedTextField.CENTER);
		
		this.influenceability=this.createLabelTitle(this.influenceability,"Influenceability:");
		influenceabilityInfo=new InfoComponent("The percentage representative of how much an agent's tastes are modified by his neighbours' tastes",this);
		this.influenceabilityTF=this.createFormattedInput(this.influenceabilityTF);
		
		this.patentNumber=this.createLabelTitle(this.patentNumber,"Number of patents:");
		patentNumberInfo=new InfoComponent("The number of patents that are distributed at the beginning of the simulation",this);
		this.patentNumberTF=this.createFormattedInput(this.patentNumberTF);
		
		this.save=new CustomButton("SAVE");
		this.cancel=new CustomButton("  CANCEL  ");
		this.cancel.setPreferredSize(new Dimension(110,30));
		this.save.setPreferredSize(this.cancel.getPreferredSize());
		
		//Organization
		this.setLayout(new BorderLayout());
		
		//this.grid=new JPanel(new GridLayout(8,2,10,10));
		this.grid=new JPanel(new GridBagLayout());
		this.c=new GridBagConstraints();
		c.fill=GridBagConstraints.HORIZONTAL;
		this.simpleGridPlacement(this.numberOfProducts,numbProductsInfo,this.numberOfProductsTF);
		this.simpleGridPlacement(this.numberOfAgents,numbAgentsInfo ,this.numberOfAgentsTF);
		this.simpleGridPlacement(this.initialMoneyAgent,initialMoneyAgentInfo,this.initialMoneyAgentTF);
		this.simpleGridPlacement(this.initialUtilityAgent,initialUtilityInfo, this.initialUtilityAgentTF);
		this.simpleGridPlacement(this.initialProduction,initialProductionInfo, this.initialProductionTF);
		this.simpleGridPlacement(this.maxBenefit,maxBenefitInfo,this.maxBenefitTF);
		this.simpleGridPlacement(this.numberOfLastTransactions,numberOfLastTransactionsInfo, this.numberOfLastTransactionsTF);
		this.simpleGridPlacement(this.partRandomProduction,partRandomInfo,this.partRandomProductionTF);
		this.simpleGridPlacement(this.specializedProduction,specializeProductionInfo, this.specializedProductionTF);
		this.simpleGridPlacement(this.decreasingUtility,decreasingUtilityInfo,this.decreasingUtilityCB);
		this.simpleGridPlacement(this.influenceability,influenceabilityInfo, this.influenceabilityTF);
		this.simpleGridPlacement(this.patentNumber,patentNumberInfo, this.patentNumberTF);
		
		JPanel title=new JPanel();
		title.add(this.nameTF);
		title.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
		
		JPanel control=new JPanel();
		control.setBackground(MainView.CONTROL_COLOR);
		control.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		control.add(save);
		control.add(cancel);
		
		this.setBackground(bg);
		this.grid.setBackground(bg);
		this.grid.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
		this.add(title,BorderLayout.NORTH);
		this.add(this.grid,BorderLayout.CENTER);
		this.add(control,BorderLayout.SOUTH);
		
		this.addListener();
	}
	/**
	 * Place two elements in a row
	 * @param c1 - component 1
	 * @param c2 - component 2
	 */
	public void simpleGridPlacement(JComponent c1,InfoComponent c2,JComponent c3){
		this.c.gridy=this.gridy;
		this.c.gridx=0;
		this.c.weightx=0.5;
		this.grid.add(c1,this.c);
		this.c.gridx=1;
		this.c.weightx=0.3;
		this.grid.add(c3,this.c);
		this.gridy++;
		this.c.gridx=2;
		this.c.weightx=0.2;
		c.insets=new Insets(0, 10, 0, 0);
		this.grid.add(c2,this.c);
		c.insets=new Insets(0, 0, 0, 0);
		c2.addMouseListener(c2);
	}
	/**
	 * Add listener to the buttons of the dialog
	 */
	public void addListener(){
		/*
		 * Add listener to the cancel button
		 */
		this.cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		/*
		 * Add listener to the add button
		 */
		this.save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//1) Control of the value
				//1.1) Get all the values
				Integer numProd=getNumberOfProducts();
				Integer numAg=getNumberOfAgents();
				Integer numLastTrans=getNumberOfLastTransactions();
				Double partRand=getPartRandomProduction();
				Double maxBen=getMaxBenefit();
				String name=getNameWorld();
				Double money=getInitialMoneyAgent();
				Double util=getInitialUtilityAgent();
				Integer initialProd=getInitialProduction();
				Integer specProd=getSpecializedProduction();
				Double influenceability=getInfluenceability();
				Integer patentNumber = getPatentNumber();
				String title="ERROR";
				//1.2) Control that all the required value have been entered
				if(numProd!=null && numAg!=null && numLastTrans!=null && partRand!=null && maxBen !=null && !name.equals("") && money!=null && util!=null && initialProd!=null && specProd !=null && influenceability!=null && patentNumber != null){
					//1.3) Check all the different values
					if(numProd<=0){
						showErroGreaterZero("number of products",title);
						return;
					}
					if(numAg<=0){
						showErroGreaterZero("number of agents",title);
						return;
					}
					else if(numAg>200){
						String mess=new String("The number of agents should be inferior or equal to 200.").toUpperCase();
						JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
						requestFocus();
						return;
					}
					if(money<=0){
						showErroGreaterZero("Initial money of the agents",title);
						return;
					}
					if(influenceability<0 || influenceability>1){
						showErroPercent("influenceability",title);
						return;
					} 
					if(patentNumber <0){
						showErroGreaterEqualsZero("Number of patent",title);
						return;
					}
					if(util<0){
						showErroGreaterEqualsZero("initial utility of the agents",title);
						return;
					}
					if(initialProd<0){
						showErroGreaterEqualsZero("amount of initial production(s)",title);
						return;
					}
					if(maxBen<=0){				
						showErroGreaterZero("maximum benefit",title);
						return;
					}
					if(numLastTrans<0){
						showErroGreaterEqualsZero("number of last transactions considered",title);
						return;
					}
					if(partRand<0 || partRand>1){
						showErroPercent("part of randomn production",title);
						return;
					}
					if(specProd<0){
						showErroGreaterEqualsZero("amount of product production needed for a producer to specialize himself in this product",title);
						return;
					}
					if(self.controller.checkName(name,self.oldName)){
						//Name already taken
						String mess=new String("The Name you have chosen for your world is already the name of another world, please choose another one").toUpperCase();
						JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
						requestFocus();
						return;
					}
					//here everything is OK !
					if(self.isEdit){
						self.controller.editWorld(self.oldName,self);
						self.setVisible(false);
						JOptionPane.showMessageDialog(null,new String("World successfully updated").toUpperCase(),"SUCCESS", JOptionPane.INFORMATION_MESSAGE);
						requestFocus();
					}
					else{
						self.controller.saveWorld(self);
						self.setVisible(false);
						JOptionPane.showMessageDialog(null,new String("World successfully created").toUpperCase(),"SUCCESS", JOptionPane.INFORMATION_MESSAGE);
						requestFocus();
					}
				}
				else{
					String mess=new String("Please fill each input").toUpperCase();
					JOptionPane.showMessageDialog(null,mess,"ERROR", JOptionPane.ERROR_MESSAGE);
					requestFocus();
				}
			}
		});
	}
	/*
	 * Show different error types
	 */
	/**
	 * Show an error if a value smaller than zero has been entered
	 * @param field -  the field name
	 * @param title - the title of error message
	 */
	public void showErroGreaterEqualsZero(String field,String title){
		String mess=new String("The "+field+" must be greater or equal to 0").toUpperCase();
		JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
		this.requestFocus();
	}
	/**
	 * Show an error if a value smaller or equal to zero has been entered
	 * @param field -  the field name
	 * @param title - the title of error message
	 */
	public void showErroGreaterZero(String field,String title){
		String mess=new String("The "+field+" must be greater than 0").toUpperCase();
		JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
		this.requestFocus();
	}
	
	/**
	 * Show an error if a value smaller than zero or greater than 1  has been entered
	 * @param field -  the field name
	 * @param title - the title of error message
	 */
	public void showErroPercent(String field,String title){
		String mess=new String("The "+field+" must be between 0 and 1").toUpperCase();
		JOptionPane.showMessageDialog(null,mess,title, JOptionPane.ERROR_MESSAGE);
		this.requestFocus();
	}
	/**
	 * Show the dialog
	 */
	public void showDialog(){
		this.pack();
		this.setLocation ((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
		this.setVisible(true);
		requestFocus();     
	}
	/**
	 * Reinitialize all the inputs
	 */
	public void reinitialize(){
		this.nameTF.setText("");
		this.numberOfProductsTF.setValue(null);
		this.numberOfAgentsTF.setValue(null);
		this.initialMoneyAgentTF.setValue(null);
		this.initialUtilityAgentTF.setValue(null);
		this.initialProductionTF.setValue(null);
		this.maxBenefitTF.setValue(null);
		this.numberOfLastTransactionsTF.setValue(null);
		this.partRandomProductionTF.setValue(null);
		this.specializedProductionTF.setValue(null);
		this.decreasingUtilityCB.setSelected(false);
		this.influenceabilityTF.setValue(null);
	}
	
	/*
	 * All getter setter
	 */
	public void setIsEdit(boolean edit){
		this.isEdit=edit;
	}
	public boolean getIsEdit(){
		return this.isEdit;
	}
	//Old Name (for edit)
	public String getOldNameWorld() {
		return this.oldName;
	}
		
	public WorldDialog setOldNameWorld(String name){
		this.oldName=name;
		return this;
	}
	//Name
	public String getNameWorld() {
		return this.nameTF.getText();
	}
	
	public WorldDialog setNameWorld(String name){
		this.nameTF.setText(name);
		return this;
	}
	
	//Money
	public Double getInitialMoneyAgent() {
		Object tmp=this.initialMoneyAgentTF.getValue();
		if(tmp!=null){
			Double val=((Number)this.initialMoneyAgentTF.getValue()).doubleValue();
			return val;
		}
		return null;
	}

	public WorldDialog setInitialMoneyAgent(Double initialMoneyAgent) {
		this.initialMoneyAgentTF.setValue(initialMoneyAgent);
		return this;
	}
	
	//Influenceability
	public Double getInfluenceability() {
		Object tmp=this.influenceabilityTF.getValue();
		if(tmp!=null){
			Double val=((Number)this.influenceabilityTF.getValue()).doubleValue();
			return val;
		}
		return null;
	}

	public WorldDialog setInfluenceability(Double influenceability) {
		this.influenceabilityTF.setValue(influenceability);
		return this;
	}
	//Patent number
	public Integer getPatentNumber() {
		Object tmp=this.patentNumberTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.patentNumberTF.getValue()).intValue();
			return val;
		}
		return null;
	}

	public WorldDialog setPatentNumber(int patentNumber) {
		this.patentNumberTF.setValue(patentNumber);
		return this;
	}
	//Utility
	public Double getInitialUtilityAgent() {
		Object tmp=this.initialUtilityAgentTF.getValue();
		if(tmp!=null){
			Double val=((Number)this.initialUtilityAgentTF.getValue()).doubleValue();
			return val;
		}
		return null;
	}

	public WorldDialog setInitialUtilityAgent(Double initialUtilityAgent) {
		this.initialUtilityAgentTF.setValue(initialUtilityAgent);
		return this;
	}

	//Initial production
	public Integer getInitialProduction() {
		Object tmp=this.initialProductionTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.initialProductionTF.getValue()).intValue();
			return val;
		}
		return null;
	}

	public WorldDialog setInitialProduction(int initialProduction) {
		this.initialProductionTF.setValue(initialProduction);
		return this;
	}
	
	//Number of products
	public Integer getNumberOfProducts() {
		Object tmp=this.numberOfProductsTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.numberOfProductsTF.getValue()).intValue();
			return val;
		}
		return null;
	}

	public WorldDialog setNumberOfProducts(int numberOfProducts) {
		this.numberOfProductsTF.setValue(numberOfProducts);
		return this;
	}
	
	//Number of Agents
	public Integer getNumberOfAgents() {
		Object tmp=this.numberOfAgentsTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.numberOfAgentsTF.getValue()).intValue();
			return val;
		}
		return null;
	}

	public WorldDialog setNumberOfAgents(int numberOfAgents) {
		this.numberOfAgentsTF.setValue(numberOfAgents);
		return this;
	}

	//Specialized Production
	public Integer getSpecializedProduction() {
		Object tmp=this.specializedProductionTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.specializedProductionTF.getValue()).intValue();
			return val;
		}
		return null;
	}

	public WorldDialog setSpecializedProduction(int specializedProduction) {
		this.specializedProductionTF.setValue(specializedProduction);
		return this;
	}

	//Part random production
	public Double getPartRandomProduction() {
		Object tmp=this.partRandomProductionTF.getValue();
		if(tmp!=null){
			Double val=((Number)this.partRandomProductionTF.getValue()).doubleValue();
			return val;
		}
		return null;
	}

	public WorldDialog setPartRandomProduction(Double partRandomProduction) {
		this.partRandomProductionTF.setValue(partRandomProduction);
		return this;
	}

	//Number of last transactions
	public Integer getNumberOfLastTransactions() {
		Object tmp=this.numberOfLastTransactionsTF.getValue();
		if(tmp!=null){
			Integer val=((Number)this.numberOfLastTransactionsTF.getValue()).intValue();
			return val;
		}
		System.out.println("Num last trans "+tmp);
		return null;
	}

	public WorldDialog setNumberOfLastTransactions(int numberOfLastTransactions) {
		this.numberOfLastTransactionsTF.setValue(numberOfLastTransactions);
		return this;
	}

	//Max Benefit
	public Double getMaxBenefit() {
		Object tmp=this.maxBenefitTF.getValue();
		if(tmp!=null){
			Double val=((Number)this.maxBenefitTF.getValue()).doubleValue();
			return val;
		}
		return null;
	}

	public WorldDialog setMaxBenefit(Double maxBenefit) {
		this.maxBenefitTF.setValue(maxBenefit);
		return this;
	}

	//Decreasing Utility
	public Boolean getDecreasingUtility() {
		Boolean val=this.decreasingUtilityCB.isSelected();
		return val;
	}

	public WorldDialog setDecreasingUtility(Boolean decreasingUtility) {
		this.decreasingUtilityCB.setSelected(decreasingUtility);
		return this;
	}
	

}
