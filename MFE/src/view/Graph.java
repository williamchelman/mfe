package view;

import helper.Helper;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.JPanel;
/**
 * A custom graph element
 * @author nicolasbernier
 *
 */
public class Graph extends JPanel implements MouseMotionListener,MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Double> datas=new ArrayList<Double>();
	private Double[] datasAllGrad;

	private ArrayList<ValueGraph> values=new ArrayList<ValueGraph>();
	private String title;
	private boolean hasData=false;
	private Color color;
	private boolean isPercentage=true;
	private boolean allGraduation=false;
	private int maxNumGrad,size,min_grad;
	private double compression;
	private Integer widthBar;
	//private 
	private boolean displayMessage=false;
	private Message m;
	/**
	 * Private class that represent the tuple value and height on the graph
	 * @author nicolasbernier
	 *
	 */
	class ValueGraph{
		public Double value;
		public Integer height;
		public String valueS;
		public Integer step;
		public ValueGraph(Double value,Integer height,String valueS,Integer step){
			this.value=value;
			this.height=height;
			this.valueS=valueS;
			this.step=step;
		}
	}
	/**
	 * Private class that represent the tuple message posX, posY of the value
	 * @author nicolasbernier
	 *
	 */
	class Message{
		//public String message,step;
		public Integer grad,posX,poxY;
		public Message(Integer grad,Integer posX,Integer posY){
			//this.message=message;
			this.grad=grad;
			this.posX=posX;
			this.poxY=posY;
		}
	}
	//PARAMETERS OF THE GRAPH
	private static int SPACE_BORDER_X=70;
	private static int SPACE_BORDER_Y=15;
	private static int SPACE_BORDER_Y_BOTTOM=35;
	private static boolean DISPLAY_HORIZONTAL_LINE=false;
	private static boolean DISPLAY_VERTICAL_LINE=false;
    //Arrows
	private static int WIDTH_ARROW=12;
	private static int HEIGHT_ARROW=16;
	//Graduation Y
	private static int FONT_HEIGHT_GRADUATION=12;
	private static int WITH_GRADUATION=8;
	private static int SPACE_GRADUATION_TEXT=2;
	private static int SPACE_GRADUATION_ARROW=7;
	//All graduation parameter
	private static String TXT_GRADUATION_X="Agents";
	private static Boolean ADD_LINE_X_EQUAL_Y_ALL=true;

	//Graduation X
	private static int DEFAULT_WIDTH_BAR=3;
	
	public static int NUM_STEP_TO_DISPLAY=-1;
	public GraphView parent;
	/**
	 * The constructor 
	 * @param parent - the GraphView containing this graph
	 */
	public Graph(GraphView parent){
		this.parent=parent;
		this.setBackground(null);
		this.setPreferredSize(new Dimension(300,200));
		//Random r=new Random();
		//this.color=GraphView.COLORS[r.nextInt(GraphView.COLORS.length)];
	}
	/**
	 * Give data to be displayed
	 * @param datas - the given data
	 */
	public void setDatas(ArrayList<Double> datas){
		this.datas=datas;
		this.allGraduation=false;
		if(!this.hasData)
			this.hasData=true;
	}
	public void setDatas(Double[] datas){
		this.datasAllGrad=datas;
		this.allGraduation=true;
		if(!this.hasData)
			this.hasData=true;
	}
	/**
	 * Set a color to the graph
	 * @param c - the Color
	 */
	public void setColor(Color c){
		this.color=c;
	}
	/**
	 * Get the color of the graph
	 * @return the color
	 */
	public Color getColor(){
		return this.color;
	}
	/**
	 * Set a title to the graph and check the type of graph
	 * @param title
	 */
	public void setTitle(String title){
		this.title=title;
		if(title!=null){
			Integer index=Helper.getIndexInArray(title,MenuView.GRAPH_NAMES);
			if((index<=5 && index>=0) || index==7){
				this.isPercentage=false;
			}
			else{
				this.isPercentage=true;
			}
		}
	}
	/**
	 * Get the title of the graph
	 * @return the title
	 */
	public String getTitle(){
		return this.title;
	}
	/**
	 * Repaint the graph (if values have changed)
	 */
	public void updateView(){
		this.repaint();
	}
	@Override
    protected void paintComponent(Graphics g)
    {
		super.paintComponent(g);
		//1) Reinit the graph
		this.reinitGraph();
		//2) fill the content
		if(this.title!=null){
	        Graphics2D g2d = (Graphics2D) g;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

	        g2d.setColor(new Color(140,140,140));
	      //  g2d.fillRect(0,0, getWidth(), getHeight());
	        g2d.setColor(new Color(230,230,230));
	        //first => draw lines
	        int sizeX=getWidth();
	        int sizeY=getHeight();
	        //horizontal
	        int x1,x2,y1,y2;
	        x1=Graph.SPACE_BORDER_X;
	        x2=sizeX;
	        y1=Graph.SPACE_BORDER_Y;
	        y2=sizeY-SPACE_BORDER_Y_BOTTOM;
	        g2d.drawLine(x1,y2,x2-2,y2);
	        //vertical
	        g2d.drawLine(x1,y1+2,x1,y2);
	        //Second => Arrows
	        int decal=0;
	        int xA[]={sizeX-Graph.HEIGHT_ARROW+decal,sizeX-Graph.HEIGHT_ARROW+decal,sizeX+decal};
	        int yA[]={y2+Graph.WIDTH_ARROW/2,y2-Graph.WIDTH_ARROW/2,y2};
	        //vertical
	        g2d.fillPolygon(xA,yA,3);
	        int x2A[]={x1-Graph.WIDTH_ARROW/2,x1+Graph.WIDTH_ARROW/2,Graph.SPACE_BORDER_X};
	        int y2A[]={Graph.HEIGHT_ARROW+Graph.SPACE_BORDER_Y-decal,Graph.HEIGHT_ARROW+Graph.SPACE_BORDER_Y-decal,Graph.SPACE_BORDER_Y-decal};
	        //horizontal
	        g2d.fillPolygon(x2A,y2A,3);
	        //title
	        FontMetrics fm;
	        Font f;
	        
	        int heightY=y2-y1-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW;
	        int widthX=x2-x1-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW;
	        int baseY=Graph.SPACE_BORDER_Y+Graph.HEIGHT_ARROW+Graph.SPACE_GRADUATION_ARROW;
	        int baseX=Graph.SPACE_BORDER_X;
	        
	        Double gradMax=0.0;
	        Double gradMin=0.0;
	        this.widthBar=Graph.DEFAULT_WIDTH_BAR;
	        
	        int fontSize=18;
	        //Title and graduation
	        f=new Font(MainView.FONT,Font.PLAIN,fontSize);
	       	fm=this.getFontMetrics(f);
	       	g2d.setFont(f);
	       	int width = fm.stringWidth(this.title);
	       	g2d.drawString(this.title,Graph.SPACE_BORDER_X+(sizeX-Graph.SPACE_BORDER_X)/2-width/2,fontSize);
	       	this.size=0;
	       	if(this.datasAllGrad != null || this.datas!=null){
		       	if(this.allGraduation)
		       		this.size=this.datasAllGrad.length;
		       	else
		       		this.size=this.datas.size();
	       	}
		    if(this.size>0){
	    	    // graduations
	       		//1) Vertical
	            f=new Font(MainView.FONT,Font.PLAIN,Graph.FONT_HEIGHT_GRADUATION);
	            fm=this.getFontMetrics(f);	
	            g2d.setFont(f);
	            //Handle Graph scale
	       		//This part should be only computed once => possible small optimization
	            if(!this.allGraduation){
		            //The amount of data 
		        	this.maxNumGrad=(int) Math.round(widthX/(double)widthBar);  	
		        	//actual Number of graduation to display
		        	if(Graph.NUM_STEP_TO_DISPLAY<=0){
		        		Graph.NUM_STEP_TO_DISPLAY=this.maxNumGrad;
		        	}
		        	this.compression=(double) Math.max(1.0,Graph.NUM_STEP_TO_DISPLAY/(double)maxNumGrad);
		        	
		        	this.min_grad=(int) Math.max(0,Math.round(size-compression*maxNumGrad));
		        }
	            else{
	            	this.min_grad=0;
	            	this.widthBar=Math.max(1,(int) Math.floor(widthX/(double)this.size));
	            	this.maxNumGrad=this.size;
	            	this.compression=1;
	            }
	            if(!this.allGraduation){
	            	Double max=Helper.getMax(this.datas,min_grad);
	            	Double min=Helper.getMin(this.datas,min_grad);
		            Double diff=max-min;
		            if(diff==0.0){
		            	if(this.isPercentage)
		            		diff=0.1;
		            	else
		            		diff=10.0;
		            }
		            //Get Bounds
		            //First method
		            if(this.isPercentage){
		            	gradMax=Math.min(100,(max+diff/5)*100);
		            	gradMin=Math.max(0,(min-diff/5)*100);
		           	}
		           	else{
		           		gradMax=max+diff/5;
		            	gradMin=Math.max(0,min-diff/5);
		           	}
	            }
	            else{
	            	gradMax=100.0;
	            	gradMin=0.0;
	            }
	            /*
	            //First method
	            double rapp1=2.0;
	            double rapp2=Math.sqrt(5.0);
	            if(this.isPercentage){
	            	gradMax=this.getMaxGradDown(max*100,100.0,rapp1);
	            	gradMin=gradMax/(rapp1*rapp1);
	           	}
	           	else{
	           		gradMax=this.getMaxGradUp(max,2.0,rapp2);
	           		gradMin=gradMax/(rapp2*rapp2);
	           	}*/
	        	double prec=100.0;
	       		double grad1,grad2,grad3,grad4,grad0;
	       		grad1=Math.round(gradMax*prec)/prec;
	       		grad2=Math.round((gradMin+(((gradMax-gradMin)*3)/4))*prec)/prec;
	       		grad3=Math.round((gradMin+((gradMax-gradMin)/2))*prec)/prec;
	       		grad4=Math.round((gradMin+((gradMax-gradMin)/4))*prec)/prec;
	       		grad0=Math.round(gradMin*prec)/prec;
	       		
        		int widthGrad1,widthGrad2,widthGrad3,widthGrad4,width0;
        		String grad1S,grad2S,grad3S,grad4S,grad0S;
	       		grad1S=NumberFormat.getNumberInstance(Locale.US).format(grad1).toString();
	       		grad2S=NumberFormat.getNumberInstance(Locale.US).format(grad2).toString();
	       		grad3S=NumberFormat.getNumberInstance(Locale.US).format(grad3).toString();
	       		grad4S=NumberFormat.getNumberInstance(Locale.US).format(grad4).toString();
	       		grad0S=NumberFormat.getNumberInstance(Locale.US).format(grad0).toString();
	       		
	        	if(this.isPercentage){
	        		grad1S=grad1S+"%";
	        		grad2S=grad2S+"%";
	        		grad3S=grad3S+"%";
	        		grad4S=grad4S+"%";
	        		grad0S=grad0S+"%";
	        	}
	        	//2 graduations
	        	//Values
	        	widthGrad1 = fm.stringWidth(grad1S);
	        	widthGrad2 = fm.stringWidth(grad2S);
	        	widthGrad3 = fm.stringWidth(grad3S);
	        	widthGrad4 = fm.stringWidth(grad4S);
	        	width0= fm.stringWidth(grad0S);
	        	int posY1=baseY;
	        	int posY2=posY1+heightY/4;
	        	int posY3=posY1+heightY/2;
	        	int posY4=posY1+(3*heightY)/4;
	        	int posY0=posY1+heightY;
	        	g2d.drawString(grad1S,Graph.SPACE_BORDER_X-widthGrad1-Graph.WITH_GRADUATION-Graph.SPACE_GRADUATION_TEXT,posY1+4*Graph.FONT_HEIGHT_GRADUATION/10);
	        	g2d.drawString(grad2S,Graph.SPACE_BORDER_X-widthGrad2-Graph.WITH_GRADUATION-Graph.SPACE_GRADUATION_TEXT,posY2+4*Graph.FONT_HEIGHT_GRADUATION/10);
	        	g2d.drawString(grad3S,Graph.SPACE_BORDER_X-widthGrad3-Graph.WITH_GRADUATION-Graph.SPACE_GRADUATION_TEXT,posY3+4*Graph.FONT_HEIGHT_GRADUATION/10);
	        	g2d.drawString(grad4S,Graph.SPACE_BORDER_X-widthGrad4-Graph.WITH_GRADUATION-Graph.SPACE_GRADUATION_TEXT,posY4+4*Graph.FONT_HEIGHT_GRADUATION/10);
	        	g2d.drawString(grad0S,Graph.SPACE_BORDER_X-width0-Graph.WITH_GRADUATION-Graph.SPACE_GRADUATION_TEXT,posY0+4*Graph.FONT_HEIGHT_GRADUATION/10);
	        	//line
	       		g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,posY1,Graph.SPACE_BORDER_X,posY1);
	       		g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION/2,posY2,Graph.SPACE_BORDER_X,posY2);
	       		g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,posY3,Graph.SPACE_BORDER_X,posY3);
	       		g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION/2,posY4,Graph.SPACE_BORDER_X,posY4);
	       		g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,posY0,Graph.SPACE_BORDER_X,posY0);	
	       		
	       		if(!this.allGraduation){
		       		//Graduation horizontal
		       		//text
		       		String gradHor1,gradHor2,gradHorMiddle;
		       		//Content graduation
	
		       		int maxGradHor=(int) Math.max(Math.max(Graph.NUM_STEP_TO_DISPLAY,this.size),maxNumGrad);
		       		gradHor1=NumberFormat.getNumberInstance(Locale.US).format(min_grad).toString();
		       		gradHor2=NumberFormat.getNumberInstance(Locale.US).format(maxGradHor).toString();
		       		int middle=Math.round((maxGradHor-min_grad)/2);
		       		gradHorMiddle=NumberFormat.getNumberInstance(Locale.US).format(min_grad+middle).toString();
		       		//width text
		       		int widthGradHor1,widthGradHor2,widthGradMiddle;
		       		widthGradHor1 = fm.stringWidth(gradHor1);
		       		widthGradHor2 = fm.stringWidth(gradHor2);
		       		widthGradMiddle = fm.stringWidth(gradHorMiddle);
		       		//placement
		       		g2d.drawString(gradHor1,Graph.SPACE_BORDER_X-widthGradHor1/2,y2+Graph.WITH_GRADUATION+fontSize);
		        	g2d.drawString(gradHor2,sizeX-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW-widthGradHor2/2,y2+Graph.WITH_GRADUATION+fontSize);
		        	g2d.drawString(gradHorMiddle,Graph.SPACE_BORDER_X+widthX/2-widthGradMiddle/2,y2+Graph.WITH_GRADUATION+fontSize);
	
		        	//line
		       		g2d.drawLine(Graph.SPACE_BORDER_X,y2,Graph.SPACE_BORDER_X,y2+Graph.WITH_GRADUATION);
		       		g2d.drawLine(Graph.SPACE_BORDER_X+widthX/2,y2,Graph.SPACE_BORDER_X+widthX/2,y2+Graph.WITH_GRADUATION);
		       		g2d.drawLine(sizeX-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW,y2,sizeX-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW,y2+Graph.WITH_GRADUATION);
	       		}
	       		else{
	       			int widthInfo;
	       			widthInfo = fm.stringWidth(Graph.TXT_GRADUATION_X);
		       		g2d.drawString(Graph.TXT_GRADUATION_X,Graph.SPACE_BORDER_X-widthInfo/2+widthX/2,y2+Graph.WITH_GRADUATION+fontSize);
	       		}
	       		//Actual data
	       		g2d.setColor(this.color);
	       		int currentX=0;
	        	int baseXGraph=baseX+1;
	        	this.values.clear();
	        	for(int i=0;i<this.maxNumGrad && (min_grad+(i*compression)<this.size);i++){
	        		Double value=0.0,previousValue=0.0;
	        		//get projected value
	        		int indexPrevious=Math.min(this.size-1,(int)Math.floor(min_grad+((i-1)*compression)));
	        		int index=Math.min(this.size-1,(int)Math.floor(min_grad+(i*compression)));
	        		if(!this.allGraduation){
	        			value=this.datas.get(index);
	        			if(indexPrevious>=0 && i>0){
	        				previousValue=this.datas.get(indexPrevious);
	        			}
	        			else if(indexPrevious>=0){
	        				previousValue=value;
	        			}
	        			else{
	        				previousValue=this.datas.get(0);
	        			}
	        			//small controll
	        			if(i==0){
	        				previousValue=Math.min(previousValue,value);
	        			}
	        		}
	        		else{
	        			value=this.datasAllGrad[index];
	        			if(indexPrevious>=0){
	        				previousValue=this.datasAllGrad[indexPrevious];
	        			}
	        		}
	        		//System.out.println(value+" "+previousValue+" "+nextValue);
	        		if(value==null){
	        			value=0.0;
	        			previousValue=0.0;
	        		}
		        	//if percentage => multiply by 100
		        	if(this.isPercentage){
		        		value*=100.0;
		        		previousValue*=100;
		        	}
		        	//compute height
		        	int height=(int) Math.round((double)heightY*((value-gradMin)/(gradMax-gradMin)));
		        	int heightPrevious=(int) Math.round((double)heightY*((previousValue-gradMin)/(gradMax-gradMin)));

		        	String valueS=NumberFormat.getNumberInstance(Locale.US).format(Math.round(value*prec)/prec).toString();
		        	if(this.isPercentage)
		        		valueS+="%";
		        	this.values.add(new ValueGraph(value, height,valueS,index));
		        	int x[]={baseXGraph+currentX,baseXGraph+currentX,baseXGraph+currentX+widthBar,baseXGraph+currentX+widthBar};
		        	int y[]={baseY+heightY-1,baseY+(heightY-heightPrevious)-1,baseY+(heightY-height)-1,baseY+heightY-1};

		        	g2d.drawPolygon(x, y, 4);
		        	//g2d.drawRect(baseXGraph+currentX,baseY+(heightY-height),widthBar, height-1);
		        	currentX+=widthBar;
	        	}
	            
	        	int endLine=sizeX-Graph.HEIGHT_ARROW;
	       		int y1Hor=Graph.SPACE_BORDER_Y+Graph.WIDTH_ARROW+Graph.SPACE_GRADUATION_ARROW;
	       		int y2Hor=sizeY-Graph.SPACE_BORDER_Y_BOTTOM;
	        	
	            //Change the graphic2D if it has to display dashed lines
	       		if(Graph.DISPLAY_HORIZONTAL_LINE || Graph.DISPLAY_VERTICAL_LINE || this.displayMessage || (this.allGraduation && Graph.ADD_LINE_X_EQUAL_Y_ALL)){
	       			g2d.setColor(new Color(240,240,240));
	            	g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.15f));
	            	g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);

	            	float dashSpace=4.0f;
	            	float dash1[] = {dashSpace};
	            	BasicStroke dashed =new BasicStroke(1.0f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,dashSpace, dash1, 0.0f);
	            	g2d.setStroke(dashed);
	       		}
	            //Display horizontal lines to help read the graduations
	            if(Graph.DISPLAY_HORIZONTAL_LINE){
	       			g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,posY1,endLine,posY1);
	       			g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION/2,posY2,endLine,posY2);
	       			g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,posY3,endLine,posY3);
	       			g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION/2,posY4,endLine,posY4);
	            }
	            //Display vertical lines to help read the graduations
	       		if(Graph.DISPLAY_VERTICAL_LINE){
	       			g2d.drawLine(Graph.SPACE_BORDER_X+widthX/2,y1Hor,Graph.SPACE_BORDER_X+widthX/2,y2Hor);
	       			g2d.drawLine(sizeX-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW,y1Hor,sizeX-Graph.HEIGHT_ARROW-Graph.SPACE_GRADUATION_ARROW,y2Hor);
	       		}
	       		if(this.allGraduation && Graph.ADD_LINE_X_EQUAL_Y_ALL){
	       			g2d.drawLine(Graph.SPACE_BORDER_X,y2Hor,Graph.SPACE_BORDER_X+this.size*this.widthBar,y1Hor);
	       		}
	       		//Display (if displayMessage is true) dashed lines on the graph crossing to hovered value
	            if(this.displayMessage){
	            	//1) draw two dashed lines
	            	if((m.grad-1)<this.values.size()){
		            	ValueGraph v=this.values.get(m.grad-1);
		            	int positionY=baseY+(heightY-v.height);
		            	//horizontal
		            	g2d.drawLine(Graph.SPACE_BORDER_X-Graph.WITH_GRADUATION,positionY,endLine,positionY);
		            	//vertical
		            	g2d.drawLine(m.posX,y1Hor,m.posX,y2Hor);
	            	}
	            }
	       		if(Graph.DISPLAY_HORIZONTAL_LINE || Graph.DISPLAY_VERTICAL_LINE || this.displayMessage || (this.allGraduation && Graph.ADD_LINE_X_EQUAL_Y_ALL)){
	       			//Reinit
	       			g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
	       			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	       			g2d.setStroke(new BasicStroke(0));
	       		}
	       		//Display (if displayMessage is true) a rectangle showing the value hovered
	            if(this.displayMessage){
	            	//2) Show Message
	            	if((m.grad-1)<this.values.size()){
		            	ValueGraph v=this.values.get(m.grad-1);
		            	String step=NumberFormat.getNumberInstance(Locale.US).format(v.step+1).toString();
		            	
		            	String message=v.valueS;
		            	if(this.allGraduation){
		            		message+=" for "+step+" "+Graph.TXT_GRADUATION_X;
		            	}
		            	else
		            		message+=" at step "+step;
		            	
		            	//Opacity
		            	g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f));
			            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
			            
			            int positionFromTop=43;
			            //font
			            int fontSizeMessage=12;
			            f=new Font(MainView.FONT,Font.PLAIN,fontSizeMessage);
				       	fm=this.getFontMetrics(f);
				       	g2d.setFont(f);
				       	
				       	Integer messageWidth=fm.stringWidth(message);
				       	g2d.setColor(new Color(20,20,20));
				       	int paddingRectangle=10;
			            //rectangle
				       	g2d.fillRoundRect(Graph.SPACE_BORDER_X+(sizeX-Graph.SPACE_BORDER_X)/2-((messageWidth+paddingRectangle)/2),positionFromTop-4-fontSizeMessage, messageWidth+paddingRectangle,fontSizeMessage+paddingRectangle+1, 7, 7);
				       	
				       	//text
			            g2d.setColor(new Color(240,240,240));
				       	g2d.drawString(message,Graph.SPACE_BORDER_X+(sizeX-Graph.SPACE_BORDER_X)/2-messageWidth/2,positionFromTop);
			            
				       	//Reset Opacity
			            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
			            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	            	}
	            }
	            
	        }
		}
		this.parent.notifyRepaint();
   
    }
	/*
	 * Reinit (very important)
	 */
	/**
	 * Reinit the graph number of graduation (very important)
	 */
	private void reinitGraph(){
		this.maxNumGrad=0;
		this.size=0;
	}
	/**
	 * Return the number of graduation displayed
	 * @return
	 */
	public Integer getNumGraduation(){
		return this.maxNumGrad;
	}
	/**
	 * Return the size of the data
	 * @return
	 */
	public Integer getSizeDatas(){
		return this.size;
	}
	/**
	 * Return if the graph is a graph all graduation or not
	 * @return
	 */
	public boolean isAllGraduation(){
		return this.allGraduation;
	}
	/*
	 * Other method to compute the graduation
	 */
	/*
	private Double getMaxGradDown(Double max,Double gradMax,Double rapp){
		if((max)<(gradMax/rapp)){
			return gradMax=this.getMaxGradDown(max,gradMax/rapp,rapp);
		}
		else{
			return gradMax;
		}
	}
	private Double getMaxGradUp(Double max,Double base,Double rapp){
		if(max>(base)){
			return base=this.getMaxGradUp(max,base*rapp,rapp);
		}
		else{
			return base;
		}
	}
	*/
	@Override
	public void mouseDragged(MouseEvent arg0) {
		//...
	}
	@Override
	public void mouseMoved(MouseEvent arg0) {
		Point p=arg0.getPoint();
		if(this.size>0){
			//Control if the mouse is inside the graph
			if(p.y>=(Graph.SPACE_BORDER_Y+Graph.HEIGHT_ARROW+Graph.SPACE_GRADUATION_ARROW) && p.y<=(getHeight()-Graph.SPACE_BORDER_Y_BOTTOM) && p.x>(Graph.SPACE_BORDER_X) && p.x<=(this.getWidth()-Graph.HEIGHT_ARROW+2-Graph.SPACE_GRADUATION_ARROW)){
				//1 convert x position to graduation on the graph
				int posX=(int) Math.round((p.x-Graph.SPACE_BORDER_X)/this.widthBar);
				//control the value of the graduation
				if(posX>0 && posX<=this.maxNumGrad && posX<=(this.values.size())){
					this.displayMessage=true;
					this.m=new Message(posX,p.x, p.y);
				}
			}
			else{
				this.displayMessage=false;
			}
			this.updateView();
		}
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		//...
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		//...
	}
	@Override
	public void mouseExited(MouseEvent e) {
		this.displayMessage=false;
		this.updateView();
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		//...
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		//...
	}

}
