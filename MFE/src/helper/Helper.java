package helper;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import model.Agent;
/**
 * Helper Class
 * @author nicolasbernier
 *
 */
public class Helper {
	/**
	 * print an AbstractList of element T
	 * @param elems the Arry list
	 */
	public static<T> void print(AbstractList<T> elems){
		for(T elem:elems){
			System.out.print(elem.toString()+" ");
		}
		System.out.print("\n");
	}
	/**
	 * Print an element T
	 * @param elem
	 */
	public static<T> void print(T elem){
		System.out.println(elem.toString());
	}
	/**
	 * Get maximum value among a AbstractList of Double
	 * @param list - the AbstractList
	 * @return the maximum value
	 */
	public static double getMax(AbstractList<Double> list){
		Double max=0.0;
		for(int i=0;i<list.size();i++){
			Double elem=list.get(i);
			if(elem>max){
				max=elem;
			}
		}
		return max;
	}
	/**
	 * Get maximum value among a part of a AbstractList of Double
	 * @param list - the AbstractList
	 * @param min_grad - the starting index (will go until the end)
	 * @return the maximum value
	 */
	public static double getMax(AbstractList<Double> list,int min_grad){
		Double max=0.0;
		int size=list.size();
		int min=Math.max(0,min_grad);
		for(int i=min;i<size;i++){
			Double elem=list.get(i);
			if(elem!=null){
				if(elem>max){
					max=elem;
				}
			}
		}
		return max;
	}
	/**
	 * Get the minimum value among a AbstractList of Double
	 * @param list - the AbstractList
	 * @return the minimum value
	 */
	public static double getMin(AbstractList<Double> list){
		Double min=Double.MAX_VALUE;
		for(int i=0;i<list.size();i++){
			Double elem=list.get(i);
			if(elem<min){
				min=elem;
			}
		}
		return min;
	}
	/**
	 * Get the minimum value among a part of a AbstractList of Double
	 * @param list - the AbstractList
	 * @param min_grad - the starting index (will go until the end)
	 * @return the minimum value found
	 */
	public static double getMin(AbstractList<Double> list,int min_grad){
		Double min=Double.MAX_VALUE;
		int size=list.size();
		int min_it=Math.max(0,min_grad);
		for(int i=min_it;i<size;i++){
			Double elem=list.get(i);
			if(elem<min){
				min=elem;
			}
		}
		return min;
	}
	/**
	 * Get index of element in list
	 * @param elem - the element
	 * @param elems - the list
	 * @return the index
	 */
	public static<T> Integer getIndexInArray(T elem,AbstractList<T> elems){
		for(int i=0;i<elems.size();i++){
			if(elem.equals(elems.get(i))){
				return i;
			}
		}
		return -1;
	}
	/**
	 * Get index of element in array
	 * @param elem - the element
	 * @param elems - the array
	 * @return the index
	 */
	public static<T> Integer getIndexInArray(T elem,T[] elems){
		for(int i=0;i<elems.length;i++){
			if(elem.equals(elems[i])){
				return i;
			}
		}
		return -1;
	}
	/**
	 * Empty an array
	 * @param array
	 * @return
	 */
	public static<T> T[] emptyArray(T[] array){
		for(int i=0;i<array.length;i++){
			array[i]=null;
		}
		return array;
	}
	/**
	 * Return if an element elem is a list
	 * @param list
	 * @param elem
	 * @return
	 */
	public static<T> boolean isInside(AbstractList<T> list,T elem){
		for(T t: list){
			if(t.equals(elem)){
				return true;
			}
		}
		return false;
	}
	/**
	 * Shuffle an array
	 * @param array
	 * @return
	 */
	public static<T> T[] shuffle(T[] array){
		Random rand=new Random();
		for(int i=array.length-1;i>0;i--){
			int r=rand.nextInt(i);
			T temp=array[r];
			array[r]=array[i];
			array[i]=temp;
		}
		return array;
		
	}
	/*
	 * Standard statistics calculation
	 */
	/**
	 * Mean
	 * @param data
	 * @return
	 */
    public static Double getMean(Double[] data)
    {
        double sum = 0.0;
        for(Double a : data)
            sum += a;
        return sum/data.length;
    }
    /**
     * Variance
     * @param data
     * @return
     */
    public static Double getVariance(Double[] data)
    {
        double mean = Helper.getMean(data);
        double temp = 0;
        for(Double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.length;
    }
    /**
     * Std Dev
     * @param data
     * @return
     */
    public static Double getStdDev(Double[] data)
    {
        return Math.sqrt(Helper.getVariance(data));
    }
    /**
     * Median
     * @param data
     * @return
     */
    public static Double median(Double[] data) 
    {
       Double[] b = new Double[data.length];
       System.arraycopy(data, 0, b, 0, b.length);
       Arrays.sort(b);

       if (data.length % 2 == 0) 
       {
          return (b[(b.length / 2) - 1] + b[b.length / 2]) / 2.0;
       } 
       else 
       {
          return b[b.length / 2];
       }
    }
    
    /**
     * Get the indexes of the min values of a double array
     * @param array
     * @return the indexes
     */
    public static ArrayList<Integer> getMinIndexes(double[] array){
    	ArrayList<Integer> indexes = new ArrayList<Integer>();
    	if(array.length > 0){
	    	indexes.add(0);
	    	for(int i = 1 ; i < array.length ; ++i){
	    		double min = array[indexes.get(0)];
	    		if(array[i] < min){
	    			indexes.clear();
	    			indexes.add(i);
	    		} else if(array[i] == min){
	    			indexes.add(i);
	    		}
	    	}
    	}
    	return indexes;
    }
    
    public static ArrayList<Integer> getAscendingIndexes(double[] array){
    	Comparator<Double> c = new Comparator<Double>() {

			@Override
			public int compare(Double o1, Double o2) {
				if(o1.equals(o2)){
					return 0;
				} else if(o1 > o2){
					return 1;
				} else {
					return -1;
				}
			}
    		
		};
    	ArrayList<Double> copy = new ArrayList<Double>();
    	for(double d : array){
    		copy.add(d);
    	}
    	Collections.sort(copy, c);
    	ArrayList<Integer> results = new ArrayList<Integer>();
    	for(double d : copy){
    		for(int index = 0 ; index < array.length ; ++index){
    			if(d == array[index]){
    				if(!results.contains(index)){
    					results.add(index);
    					break;
    				}
    			}
    		}
    	}
    	return results;
    }
    
	public static double getMax(double[] array) {
		double max = array[0];
		for(int i = 1 ; i < array.length ; ++i){
			if(array[i] > max)
				max = array[i];
		}
		return max;
	}
	public static double getMin(double[] array) {
		double min = array[0];
		for(int i = 1 ; i < array.length ; ++i){
			if(array[i] < min)
				min = array[i];
		}
		return min;
	}
	public static double getMax(Double[] array) {
		double max = array[0];
		for(int i = 1 ; i < array.length ; ++i){
			if(array[i] > max)
				max = array[i];
		}
		return max;
	}
	public static double getMin(Double[] array) {
		double min = array[0];
		for(int i = 1 ; i < array.length ; ++i){
			if(array[i] < min)
				min = array[i];
		}
		return min;
	}
	
	public static double computeGini(double[] array){
        double gini = 0.0;
        //Double total = this.computeGlobalUtility();
        //get last total
        double total = 0;
        ArrayList<Double> copy = new ArrayList<Double>();
        for(int i = 0 ; i < array.length ; ++i){
        	total += array[i];
        	copy.add(array[i]);
        }
        
        if(total > 0){
	        Comparator<Double> c = new Comparator<Double>() {
				@Override
				public int compare(Double o1, Double o2) {
					if(o1.equals(o2)){
						return 0;
					} else if(o1 > o2){
						return 1;
					} else {
						return -1;
					}
				}
			};
			Collections.sort(copy, c);
	
	        Double share = 1.0 / array.length;
	        Double cumul = 0.0;
	        Double a = 0.0;
	        Double prevCumul = 0.0;
	
	        for(Double d : copy)
	        {
	            cumul += d / total;
	            a = prevCumul + cumul;
	            prevCumul = cumul;
	            gini += a * share;
	        }
	        
	        gini = Math.max(gini,0);
	        gini = Math.min(gini,1);
	        double value = Math.abs(1 - gini);
	        return value;
        } else {
        	return 0;
        }
    }
	
	public static double computeGini(int[] array){
        double gini = 0.0;
        //Double total = this.computeGlobalUtility();
        //get last total
        double total = 0;
        ArrayList<Double> copy = new ArrayList<Double>();
        for(int i = 0 ; i < array.length ; ++i){
        	total += array[i];
        	copy.add(Double.valueOf(array[i]));
        }
        
        Comparator<Double> c = new Comparator<Double>() {
			@Override
			public int compare(Double o1, Double o2) {
				if(o1.equals(o2)){
					return 0;
				} else if(o1 > o2){
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(copy, c);

        Double share = 1.0 / array.length;
        Double cumul = 0.0;
        Double a = 0.0;
        Double prevCumul = 0.0;

        for(Double d : copy)
        {
            cumul += d / total;
            a = prevCumul + cumul;
            prevCumul = cumul;
            gini += a * share;
        }
        
        gini = Math.max(gini,0);
        gini = Math.min(gini,1);
        double value = Math.abs(1 - gini);
        return value;
    }
}
