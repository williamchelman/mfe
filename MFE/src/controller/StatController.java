package controller;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

import model.output.WorldOutputFactory;
import model.output.sql.MYSQLFactory;
import model.stat.Stat;
import model.stat.parameter.BoolParameter;
import model.stat.parameter.DiminishingMarginaleUtility;
import model.stat.parameter.DoubleParameter;
import model.stat.parameter.Influenceability;
import model.stat.parameter.InitialMoney;
import model.stat.parameter.InitialProduction;
import model.stat.parameter.InitialUtility;
import model.stat.parameter.IntParameter;
import model.stat.parameter.MarketRandomness;
import model.stat.parameter.MaximumBenefit;
import model.stat.parameter.MinQuality;
import model.stat.parameter.NumberOfAgents;
import model.stat.parameter.NumberOfLowcostAgents;
import model.stat.parameter.NumberOfPatents;
import model.stat.parameter.NumberOfProducts;
import model.stat.parameter.Parameter;
import model.stat.parameter.ProductionsForSpecialization;
import model.stat.parameter.RandomProduction;
import model.stat.parameter.TransactionsToConsider;
import view.stat.BooleanLine;
import view.stat.NumericalLine;
import view.stat.OptionPanel;
import view.stat.StatView;

public class StatController {
	private StatView sv;
	private Thread statThread;
	
	public StatController(StatView sv){
		this.sv = sv;
	}
	
	public void run() {
		ArrayList<Parameter> parameters = getParameters();
		int maxStep = sv.getOptionPanel().getStepsNumber();
		int maxIter = sv.getOptionPanel().getIterationNumber();
		int threadNumber = sv.getOptionPanel().getThreadsNumber();
		
		try {
			WorldOutputFactory wof = createOutputFactory(sv.getOptionPanel());
			Stat s = new Stat(maxIter, maxStep, threadNumber,parameters,wof);
			sv.setEnabled(false);
			s.addObserver(sv);
			statThread = new Thread() {
		        public void run() {
	        		sv.handleStart();
					s.run();
					sv.handleStop();
		        }
		        
		        public void interrupt(){
		        	super.interrupt();
		        	s.stop();
		        }
		    };
		    statThread.start();
			
		} catch (Exception e) {
			sv.handleStop();
			sv.error(e);
		} 
	}
	
	public WorldOutputFactory createOutputFactory(OptionPanel op) throws SQLException, FileNotFoundException{
		WorldOutputFactory wo = null;
		if(op.getOutputType().equals(OptionPanel.MYSQLOutput)){
			wo = new MYSQLFactory(op.getMYSQLUrl(), op.getMYSQLPort(), op.getMYSQLDB(), op.getMYSQLUser(), op.getMYSQLPassword(),op.getMYSQLTableSuffix(),op.getMYSQLSaveHistory(),op.getMYSQLSaveLorentz(),op.getMYSQLSaveAgents(), op.getMYSQLSaveProducts(),op.getMYSQLSaveAgentsProducts(),op.getMYSQLSaveTransactions(),op.getMYSQLSavePatents());
		} 
		
		return wo;
	}
	
	public void stop() {
		if(statThread != null){
			statThread.interrupt();
			sv.handleStop();
		}
	}
	
	public ArrayList<Parameter> getParameters() {
		ArrayList<Parameter> parameters = new ArrayList<Parameter>();
		
		NumericalLine nl = sv.getNumberOfProducts();
		Parameter p = new NumberOfProducts();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getNumberOfAgents();
		p = new NumberOfAgents();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getNumberOfLastTransactions();
		p = new TransactionsToConsider();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getInfluenceability();
		p = new Influenceability();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getPatentNumber();
		p = new NumberOfPatents();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getLowCostNumber();
		p = new NumberOfLowcostAgents();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getLowCostQuality();
		p = new MinQuality();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getInitialMoneyAgent();
		p = new InitialMoney();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getInitialProduction();
		p = new InitialProduction();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getInitialUtilityAgent();
		p = new InitialUtility();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getMarketRandomness();
		p = new MarketRandomness();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getMaxBenefit();
		p = new MaximumBenefit();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getSpecializedProduction();
		p = new ProductionsForSpecialization();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		nl = sv.getPartRandomProduction();
		p = new RandomProduction();
		applyNumericalTo(p,nl);
		parameters.add(p);
		
		BooleanLine bl = sv.getDiminishingMarginalUtility();
		BoolParameter bp = new DiminishingMarginaleUtility();
		applyBooleanTo(bp,bl);
		parameters.add(bp);
		
		return parameters;
	}
	
	private void applyNumericalTo(Parameter p, NumericalLine pl){
		if(p instanceof DoubleParameter){
			DoubleParameter dp = (DoubleParameter) p;
			if(pl.isFixed()){
				double value = Double.valueOf(pl.getValue());
				dp.setMin(value);
				dp.setMax(value);
				dp.setStep(1);
			} else {
				dp.setMin(Double.valueOf(pl.getMin()));
				dp.setMax(Double.valueOf(pl.getMax()));
				dp.setStep(Double.valueOf(pl.getStep()));
			}
		} else if(p instanceof IntParameter){
			IntParameter ip = (IntParameter) p;
			if(pl.isFixed()){
				int value = Integer.valueOf(pl.getValue());
				ip.setMin(value);
				ip.setMax(value);
				ip.setStep(1);
			} else {
				ip.setMin(Integer.valueOf(pl.getMin()));
				ip.setMax(Integer.valueOf(pl.getMax()));
				ip.setStep(Integer.valueOf(pl.getStep()));
			}
		} 
	}
	
	private void applyBooleanTo(BoolParameter bp, BooleanLine pl){
		if(pl.isFixed()){
			boolean value = pl.getValue();
			bp.setValue(value);
			bp.setVariates(false);
		} else {
			bp.setVariates(true);
		}
	}

	
}
