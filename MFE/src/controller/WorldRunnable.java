package controller;

import model.MarketFailingException;
import model.World;
/**
 * A Class controlling the running of the simulation
 * @author nicolasbernier
 *
 */
public class WorldRunnable implements Runnable {

	public static int MAX_NUMBER_OF_STEP=70000;
	private World world;
	private Integer numSteps;
	private boolean continueRunning=true;
	private boolean running=true;
	private boolean needRestart=false;
	private static int MILISECOND_REFRESH=25;
	private WorldController controller;
	/**
	 * Constructor
	 * @param w - the World to run
	 * @param controller - the controller that want to run the world
	 */
	public WorldRunnable(World w,WorldController controller){
		this.world=w;
		this.continueRunning=false;
		this.controller=controller;
		this.numSteps=0;
	}
	/**
	 * Pause the running (possible to restart it)
	 */
	public void pause(){
		this.continueRunning=false;
	}
	/**
	 * Radical pause (used for a complete reset)
	 */
	public void radicalPause(){
		this.continueRunning=false;
		this.needRestart=true;
	}
	@Override
	public void run() {
		while(this.running){
			try{
				if(this.continueRunning && !this.needRestart){
					if(world.getCurrentStep()<WorldRunnable.MAX_NUMBER_OF_STEP){
						for(int i=0;i<numSteps;i++){
							try {
								world.run();
							} catch (MarketFailingException e) {
								this.radicalPause();
								//Prevent the controller that an error accured
								this.controller.errorWorld(this.world);
								break;
							}
							if(world.getCurrentStep()>=WorldRunnable.MAX_NUMBER_OF_STEP){
								this.radicalPause();
								this.controller.messageTooMuchSteps();
								break;
							}
							if(!this.continueRunning && this.controller.getMode()==Mode.single){
								break;
							}
							if(!this.continueRunning && this.needRestart){
								break;
							}
						}
						//if the World is more advanced that the other(s) wait them 
						while(this.controller.controlSimultaneity(world)==1)
							Thread.sleep(10);
						}
						//if the World is retarded regarding the others (re-catch them) (not necessary
						/*while(this.controller.controlSimultaneity(world)==-1){
							try {
								world.run();
							} catch (MarketFailingException e) {
								this.pause();
								//Prevent the controller that an error accurate
								this.controller.errorWorld(this.world);
							}
							if(!this.continueRunning && this.controller.getMode()==Mode.single){
								break;
							}
							if(!this.continueRunning && this.needRestart){
								break;
							}
						}*/
					}
					Thread.sleep(WorldRunnable.MILISECOND_REFRESH);
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Number of steps per second
	 * @param numSteps
	 */
	public void setNumSteps(Integer numSteps){
		this.numSteps=(int) Math.max(1,Math.floor(WorldRunnable.MILISECOND_REFRESH*numSteps/1000.0));
		//this.numSteps=numSteps;
	}
	/**
	 * (Re)start the thread
	 */
	public void start(){
		this.continueRunning=true;
	}
	/**
	 * Stop the running 
	 */
	public void stop(){
		this.running=false;
	}
	/**
	 * Reinitialize the running
	 */
	public void reinit(){
		this.needRestart=false;
	}

}
