package controller;
/**
 * Different mode of the simulation 2
 * @author nicolasbernier
 *
 */
public enum Mode {
	single,
	comparison,
	stat
}
