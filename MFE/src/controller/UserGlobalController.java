package controller;


import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import view.MainView;
import view.WorldDialog;
import model.World;
/**
 * Global Controller of the application
 * @author nicolasbernier
 *
 */
public class UserGlobalController {
	private LinkedHashMap <String,World> worldList=new LinkedHashMap <String, World>();
	private WorldController worldController;
	private MainView view;
	private Mode mode;
	
	public UserGlobalController(MainView view){
		//initial world (default option)
		this.view=view;
		this.worldController=new WorldController(this.view);
	}
	/**
	 * Add a world to the world list
	 * @param w - the World to add
	 */
	public void addWorld(World w){
		//add world to the world list
		this.worldList.put(w.getName(),w);
		//add world name to the menu
		this.view.getMenuView().addWorld(w.getName());
		//Add an observer to this world (the view)
		w.addObserver(this.view);
	}
	/**
	 * Check whether the mode can be change or not into the comparison mode
	 * @return true or false
	 */
	public boolean checkCanChangeMode(){
		if(this.worldList.size()>=2){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	 * Check if the name of a world is not already takken
	 * @param name - the new name
	 * @param oldName - the old name (if any)
	 * @return true or false
	 */
	public boolean checkName(String name, String oldName) {
		Iterator<String> itr=this.worldList.keySet().iterator();
		while(itr.hasNext()){
			String nextKey=itr.next();
			if(nextKey.equals(name) && !nextKey.equals(oldName))
				return true;
		}
		return false;
	}
	/**
	 * Delete a world by name (name are unique)
	 * @param name - the name of the world to delete
	 */
	public void deleteWorld(String name){
		if(this.worldList.size()>1){
			//case 1:world is not currently running
			//case 2.1 world is currently running (single mode)
			//case 2.2 world is currently running (comparison mode)
			
			//Case single mode
			if(this.mode==Mode.single){
				this.worldList.remove(name);
				this.view.getMenuView().removeWorld(name);
				//If current world has been deleted, set the next world active
				if(name.equals(this.worldController.getWorld()[0].getName())){
					String nextKey=this.worldList.keySet().iterator().next();
					this.setWorld(this.worldList.get(nextKey));
				}
			}
			//Case comparison mode
			else if(this.mode==Mode.comparison){
				World worldDeleted=this.worldList.remove(name);
				this.view.getMenuView().removeWorld(name);
				//if one of the active world has been deleted and there is only one world remaining => go the single mode
				if(this.worldList.size()==1){
					if(worldDeleted.getName().equals(this.worldController.getWorld()[0].getName())){
						this.worldController.setWorld(this.worldController.getWorld()[1]);
					}
					this.view.getMenuView().handleSingleMode();
					this.view.getMenuView().computeGraphChecked();
					this.setMode(Mode.single);
				}
				//there is still at least two worlds
				else{
					//If the world deleted was the first world in comparison mode
					if(name.equals(this.worldController.getWorld()[0].getName())){
						Iterator<String> itr=this.worldList.keySet().iterator();
						while(itr.hasNext()){
							String nextKey=itr.next();
							if(!nextKey.equals(this.worldController.getWorld()[1].getName())){
								World w1,w2;
								w1=this.worldList.get(nextKey);
								w2=this.worldController.getWorld()[1];
								this.setWorlds(w1,w2);
								break;
							}
						}		
					}
					//If the world deleted was the second world in comparison mode
					else if(name.equals(this.worldController.getWorld()[1].getName())){
						Iterator<String> itr=this.worldList.keySet().iterator();
						while(itr.hasNext()){
							String nextKey=itr.next();
							if(!nextKey.equals(this.worldController.getWorld()[0].getName())){
								World w1,w2;
								w1=this.worldController.getWorld()[0];
								w2=this.worldList.get(nextKey);
								this.setWorlds(w1,w2);
								break;
							}
						}	
					}
					//Else => don't care
				}
			}
			this.worldController.reinitChecked();
		}
		else{
			//NOTIFY VIEW THAT THERE IS NOT ENOUGH WORLDS
			this.view.showPopupError(new String("You must keep at least one world !").toUpperCase(), "DELETE ERROR");
		}
	}
	/**
	 * Edit a world
	 * @param oldName the name that the world has before edition (allows to retrieve him)
	 * @param w - the dialog used to edit this world (so we can retrieve it's parameters)
	 */
	public void editWorld(String oldName,WorldDialog w){
		this.worldController.stopRunner();
		World world=this.worldList.remove(oldName);
		this.view.getMenuView().removeWorld(oldName);
		world.reset();
		world.setName(w.getNameWorld());
		world=this.setWorldParameters(world, w);
		world.init();
		this.addWorld(world);
		if(this.mode==Mode.comparison){
			if(world.getName().equals(this.getWorldController().getWorld()[0].getName())){
				this.getWorldController().getWorld()[1].reset();
			}
			else if(world.getName().equals(this.getWorldController().getWorld()[1].getName())){
				this.getWorldController().getWorld()[0].reset();
			}
		}
		this.worldController.reinit();
		this.worldController.reinitChecked();
		this.worldController.changeGraph();
	}
	/**
	 * Get sub user world controller
	 * @return the WorldController of this global controller
	 */
	public WorldController getWorldController(){
		return this.worldController;
	}
	/**
	 * Function called has an initialization 
	 */
	public void init(){
		this.mode=Mode.single;
		World w=new World("World");
		w.init();
		World w2=new World("World II");
		w2.init();
		World w3=new World("World III");
		w3.init();
		
		this.addWorld(w);
		this.addWorld(w2);
		this.addWorld(w3);
		this.setWorld(w);
		this.view.getMenuView().setSelectedWord(w.getName());
	}
	/**
	 * Save a new world
	 * @param w - the WorldDialog use to setup the different parameters
	 */
	public void saveWorld(WorldDialog w){
		//this function is called after verification
		World world=new World(w.getNameWorld());
		world=this.setWorldParameters(world, w);
		world.init();
		this.addWorld(world);
		this.worldController.reinitChecked();
	}	
	/**
	 * Select the unique world to display by name in single mode
	 * @param name - the name of the world
	 */
	public void selectWorld(String name){
		World w=this.worldList.get(name);
		this.setWorld(w);
	}
	/**
	 * Tell the worldController the world selected
	 * @param current -  the name of the world selected
	 * @return this
	 */
	public UserGlobalController setWorld(World current){
		this.worldController.setWorld(current);
		return this;
	}
	/**
	 * Select the two worlds to display by names in comparison mode
	 * @param world1
	 * @param world2
	 */
	public void selectWorld(String world1,String world2){
		World w1, w2;
		w1=this.worldList.get(world1);
		w2=this.worldList.get(world2);
		this.setWorlds(w1, w2);
	}
	/**
	 * Tell the worldController the worlds selected
	 * @param w1 - the name of the first world selected
	 * @param w2 - the name of the second world selected
	 * @return this
	 */
	public UserGlobalController setWorlds(World w1,World w2){
		this.worldController.setWorlds(w1,w2);
		//this.view.getMenuView().setSelectedWord(name);
		return this;
	}
	/**
	 * Change the current mode
	 * @param m - the mode targetted
	 */
	public void setMode(Mode m){
		this.mode=m;
		this.worldController.setMode(m);
		this.view.setMode(this.mode);
		//single
		if(this.mode==Mode.single || this.mode==Mode.stat){
			//keep the first world
			World w1=this.worldController.getWorld()[0];
			this.setWorld(w1);
		}
		//comparison
		else if(this.mode==Mode.comparison){
			if(this.worldList.size()>=2){
				String world1=this.worldController.getWorld()[0].getName();
				World w1=this.worldController.getWorld()[0];
				Set<String> keys=this.worldList.keySet();
				
				String world2=null;
				for(String s:keys){
					if(!s.equals(world1)){
						world2=s;
						break;
					}
				}
				World w2=this.worldList.get(world2);
				this.view.getMenuView().setSelectedWord(world2);
				this.setWorlds(w1,w2);
				
			}
		}
	}
	/**
	 * Set the world parameters
	 * @param world - the World
	 * @param w - the WorldDialog used to enter the parameters
	 * @return the new world
	 */
	public World setWorldParameters(World world,WorldDialog w){
		world.setNumberOfProducts(w.getNumberOfProducts());
		world.setNumberOfAgents(w.getNumberOfAgents());
		world.setInitialMoneyAgent(w.getInitialMoneyAgent());
		world.setInitialUtilityAgent(w.getInitialUtilityAgent());
		world.setInitialProduction(w.getInitialProduction());
		world.setMaxBenefit(w.getMaxBenefit());
		world.setNumberOfLastTransactions(w.getNumberOfLastTransactions());
		world.setPartRandomProduction(w.getPartRandomProduction());
		world.setSpecializedProduction(w.getSpecializedProduction());
		world.setDecreasingUtility(w.getDecreasingUtility());
		world.setInfluenceability(w.getInfluenceability());
		world.setNumberOfPatents(w.getPatentNumber());
		
		return world;
	}
	/**
	 * Set the WorldDialog parameters values 
	 * @param w
	 * @param worldName
	 */
	public void setWorldValues(WorldDialog w,String worldName){
		World world=this.worldList.get(worldName);
		
		w.setNameWorld(world.getName())
		.setNumberOfProducts(world.getNumberOfProducts())
		.setNumberOfAgents(world.getNumberOfAgents())
		.setInitialMoneyAgent(world.getInitialMoneyAgent())
		.setInitialUtilityAgent(world.getInitialUtilityAgent())
		.setInitialProduction(world.getInitialProduction())
		.setMaxBenefit(world.getMaxBenefit())
		.setNumberOfLastTransactions(world.getNumberOfLastTransactions())
		.setPartRandomProduction(world.getPartRandomProduction())
		.setSpecializedProduction(world.getSpecializedProduction())
		.setDecreasingUtility(world.getDecreasingUtility())
		.setInfluenceability(world.getInfluenceability())
		.setPatentNumber(world.getNumberOfPatents());
	}
}
