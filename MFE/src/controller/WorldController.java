package controller;



import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import view.Graph;
import view.GraphView;
import view.MainView;
import view.MenuView;
import model.World;

/**
 * States that can have the controller WorldController
 * @author nicolasbernier
 *
 */
enum State{
	started,
	paused,
	reset,
	notStarted
};

/**
 * Controller controlling the current world(s) displayed
 * @author nicolasbernier
 *
 */
public class WorldController{
	public static int NUM_WORLD_COMPARISON=2;
	
	private State state;

	private World[] world=new World[2];
	private boolean[] continueWolrd=new boolean[2];
	private MainView view;
	private Integer numSteps;
	private Thread[] thread=new Thread[2];
	private WorldRunnable[] worldRunnable=new WorldRunnable[2];
	private Mode mode;
	/**
	 * Constructor
	 * @param view
	 */
	public WorldController(MainView view){
		
		this.mode=Mode.single;
		this.state=State.notStarted;
		this.view=view;
		for(int i=0;i<NUM_WORLD_COMPARISON;i++){
			this.world[i]=null;
			this.thread[i]=null;
			this.worldRunnable[i]=null;
			this.continueWolrd[i]=true;
		}
	}
	/**
	 * Allows to change the graph displayed (inform the view)
	 */
	public void changeGraph(){
		//1)Mode 1
		if(this.mode==Mode.single){
			ArrayList<Integer> listIndexGraph=this.view.getMenuView().getGraphChecked();
			HashMap<Integer,ArrayList<Double>> listGraph=this.world[0].getMappingIndexGraph();	
			ArrayList<Graph> graphs=this.view.getGraphPanel().getGraphs();
			//Set the data, the title and the color
			for(int i=0;i<listIndexGraph.size();i++){
				String name=MenuView.GRAPH_NAMES[listIndexGraph.get(i)];
				Color c=GraphView.COLORS[listIndexGraph.get(i)];
				Graph g=graphs.get(i);
				
				g.setTitle(name);
				g.setColor(c);
				//The four last graph are different from the other (static view at one moment)
				if(listIndexGraph.get(i)>=12)
					g.setDatas(this.world[0].getLorentz(listIndexGraph.get(i)+1));

				else
					g.setDatas(listGraph.get(listIndexGraph.get(i)+1));
			}
			//Remove the unnecessary graphs
			if(listIndexGraph.size()<GraphView.NUM_GRAPH){
				for(int i=listIndexGraph.size();i<GraphView.NUM_GRAPH;i++){
					Graph g=graphs.get(i);
					g.setTitle(null);
					g.setDatas((ArrayList<Double>)null);
				}
			}
			if(this.continueWolrd[0])
				this.world[0].notifyView();
		}
		//2)Mode 2
		else if(this.mode==Mode.comparison){
			HashMap<Integer,ArrayList<Double>> listGraph1=this.world[0].getMappingIndexGraph();	
			HashMap<Integer,ArrayList<Double>> listGraph2=this.world[1].getMappingIndexGraph();

			
			ArrayList<Integer> listIndexGraph=this.view.getMenuView().getGraphChecked();
			ArrayList<Graph> graphs=this.view.getGraphPanel().getGraphs();
			//Set the data, the title and the color of the graphs on the left
			for(int i=0;i<listIndexGraph.size();i++){
				String name=MenuView.GRAPH_NAMES[listIndexGraph.get(i)];
				Color c=GraphView.COLORS[listIndexGraph.get(i)];
				Graph g=graphs.get(i*2);
				
				g.setTitle(name);
				g.setColor(c);
				//The four last graph are different from the other (static view at one moment)
				if(listIndexGraph.get(i)>=12)
					g.setDatas(this.world[0].getLorentz(listIndexGraph.get(i)+1));
				else
					g.setDatas(listGraph1.get(listIndexGraph.get(i)+1));
			}
			//Set the data, the title and the color of the graphs on the right
			for(int i=0;i<listIndexGraph.size();i++){
				String name=MenuView.GRAPH_NAMES[listIndexGraph.get(i)];
				Color c=GraphView.COLORS[listIndexGraph.get(i)];
				Graph g=graphs.get((i*2)+1);
				
				g.setTitle(name);
				g.setColor(c);
				if(listIndexGraph.get(i)>=12)
					g.setDatas(this.world[1].getLorentz(listIndexGraph.get(i)+1));
				else
					g.setDatas(listGraph2.get(listIndexGraph.get(i)+1));
			}
			//Remove the unnecessary graphs
			if(listIndexGraph.size()<GraphView.NUM_GRAPH/2){
				for(int i=listIndexGraph.size();i<GraphView.NUM_GRAPH/2;i++){
					Graph g=graphs.get(i*2);
					g.setTitle(null);
					g.setDatas((ArrayList<Double>)null);
					g=graphs.get((i*2)+1);
					g.setTitle(null);
					g.setDatas((ArrayList<Double>)null);
				}
			}
			if(this.continueWolrd[0])
				this.world[0].notifyView();
			if(this.continueWolrd[1])
				this.world[1].notifyView();
			//if both market have failed, only the more advanced one notify the view
			if(!this.continueWolrd[0] && !this.continueWolrd[1]){
				if(this.world[0].getCurrentStep()>this.world[1].getCurrentStep()){
					this.world[0].notifyView();
				}
				else if(this.world[0].getCurrentStep()<this.world[1].getCurrentStep()){
					this.world[1].notifyView();
				}
				else{
					this.world[0].notifyView();
					this.world[1].notifyView();
				}
			}
		}
	}
	/**
	 * 
	 */
	public void reinitNumStepToDisplay(){
		this.view.getControlPanel().setNumStepToDisplay(0);
	}
	/**
	 * Tell the view to dispay an error
	 * @param World w
	 */
	public void errorWorld(World w){
		this.handlePause();
		if(this.world[0].getName().equals(w.getName())){
			this.continueWolrd[0]=false;
		}
		if(this.mode==Mode.comparison){
			if(this.world[1].getName().equals(w.getName())){
				this.continueWolrd[1]=false;
			}
		}
		this.view.showPopupError(new String("<html>THE MARKET "+w.getName().toUpperCase()+" HAS FAILED, PLEASE RESET OR CHANGE ITS PARAMETERS.<br/>"
				+ "<b style='color:rgb(210,50,23)'>/!\\</b><i> Reseting keep the same agents and sometimes, the randomness of the initialization<br/>"
				+ "does not allow the competitive world to evolve. In that case please update the world and click<br/>"
				+ "on validate without changing anything to reinitialize the agents</i> <b style='color:rgb(210,50,23)'>/!\\</b>"), "Market Error");
	}
	/**
	 * Get the world used by this controller
	 * @return the worlds
	 */
	public World[] getWorld() {
		return world;
	}
	/**
	 * Change the percentage of market randomness in a world
	 * @param value (the percentage of market randomness
	 * @param worldTargetted (the world targeted)
	 */
	public void handleCursor(Integer value,Integer worldTargetted){
		double percent=(double)value/100.0;
		if(worldTargetted==1)
			this.world[0].setPartRandomMarket(percent);
		if(worldTargetted==2)
			this.world[1].setPartRandomMarket(percent);
		
	}
	/**
	 * Action called when an user hits the button pause.
	 */
	public void handlePause(){
		if(state.equals(State.started)){
			state=State.paused;
			this.worldRunnable[0].pause();
			if(this.mode==Mode.comparison){
				if(this.worldRunnable[1]!=null)
					this.worldRunnable[1].pause();
			}
		}
	}
	/**
	 * Action called when an user hits the button reset
	 * @param value - the amount of step per seconds to run the simulation (if start is directly called)
	 */
	public void handleReset(Integer value, Boolean launchStart){
		if(!state.equals(State.notStarted)){
			state=State.reset;
			this.worldRunnable[0].radicalPause();
			this.world[0].reset();
			this.continueWolrd[0]=true;
			this.continueWolrd[1]=true;
			this.worldRunnable[0].reinit();
			if(this.mode==Mode.comparison){
				if(this.worldRunnable[1]!=null){
					this.worldRunnable[1].radicalPause();
				}
				if(this.world[1]!=null)
					this.world[1].reset();
				if(this.worldRunnable[1]!=null)
					this.worldRunnable[1].reinit();
			}
			this.changeGraph();
			if(launchStart){
				this.handleStart(value);
			}
			this.reinitNumStepToDisplay();
		}

	}
	/**
	 * Action called when an user hits the button start
	 * @param value - the amount of step per seconds to run the simulation 
	 */
	public void handleStart(Integer value){
		if(!state.equals(State.started)){
			this.numSteps=value;
			if(this.numSteps>0){
				state=State.started;
				this.worldRunnable[0].setNumSteps(this.numSteps);
				if(this.continueWolrd[0])
					this.worldRunnable[0].start();
				if(this.mode==Mode.comparison){
					this.worldRunnable[1].setNumSteps(this.numSteps);
					if(this.continueWolrd[1])
						this.worldRunnable[1].start();
				}
			}
		}
	}
	/**
	 * Stop the current simulation
	 */
	public void handleStop(){
		this.stopRunner();
		if(this.worldRunnable[0]!=null){
			this.worldRunnable[0].stop();
		}
		if(this.worldRunnable[1]!=null){
			this.worldRunnable[1].stop();
		}
	}
	public void messageTooMuchSteps(){
		this.view.showPopupInfo("The maximum number of computational steps has been reached", "SIMULATION END");
	}
	/**
	 * Tell the view to reinitialize some of its content (depending on the mode)
	 */
	public void reinit(){
		state=State.paused;
		this.continueWolrd[0]=true;
		this.worldRunnable[0].reinit();
		if(this.mode==Mode.single){
			this.view.getControlPanel().reinit(this.world[0].getPartRandomMarket(),this.world[0].getName(),this.world[0].getCurrentStep());
		}
		else if(this.mode==Mode.comparison){
			this.continueWolrd[1]=true;
			this.worldRunnable[1].reinit();
			this.view.getControlPanel().setTitles(this.world[0].getName(), this.world[1].getName());
			this.view.getControlPanel().setRandomness(this.world[0].getPartRandomMarket(),this.world[1].getPartRandomMarket());
			this.view.getGraphPanel().setTitles(this.world[0].getName(), this.world[1].getName());
		}
		this.reinitNumStepToDisplay();
	}
	/**
	 * Tell the view to reinitialize the worlds checked in the global Menu
	 */
	public void reinitChecked(){
		this.view.getMenuView().resetWorldChecked();
		this.view.getMenuView().setSelectedWord(this.world[0].getName());
		if(this.mode==Mode.comparison){
			this.view.getMenuView().setSelectedWord(this.world[1].getName());
		}
	}
	/**
	 * Change the mode
	 * @param m - Mode targetted
	 */
	public void setMode(Mode m){
		this.mode=m;
	}
	/**
	 * Give one world to this controller and start it
	 * @param current - World to use 
	 * @return this
	 */
	public WorldController setWorld(World current){
		this.handleStop();
		this.startWorld(current,0,false);
		this.reinit();
		this.continueWolrd[0]=true;
		this.thread[0].start();
		//always call changeGraph when world are changed
		this.changeGraph();
		return this;
	}
	/**
	 * Give two worlds to this controller and start them
	 * @param w1 - first World to use
	 * @param w2 - second World to use 
	 * @return this
	 */
	public WorldController setWorlds(World w1,World w2){
		this.handleStop();
		this.startWorld(w1, 0,true);
		this.startWorld(w2, 1,true);
		this.reinit();
		this.continueWolrd[0]=true;
		this.continueWolrd[1]=true;
		this.thread[0].start();
		this.thread[1].start();
		//always call changeGraph when world are changed
		this.changeGraph();
		return this;
	}
	/**
	 * Setup a world in a thread so it can start
	 * @param world - the World
	 * @param index - the world targeted among the world list
	 * @param reset - boolean to say if the world has to be resetted 
	 */
	public void startWorld(World world,int index,boolean reset){
		this.world[index]=world;
		if(reset)
			this.world[index].reset();
		this.worldRunnable[index]=new WorldRunnable(this.world[index],this);
		this.thread[index]=new Thread(worldRunnable[index]);
		this.world[index].notifyView();
	}
	/**
	 * Control the simultaneity when two world are running in comparison mode
	 */
	public Integer controlSimultaneity(World w){
		w.setUpdateView(true);
		if(this.mode==Mode.comparison && this.continueWolrd[0]==true && this.continueWolrd[1]==true && this.world[0]!=null && this.world[1]!=null){
			//if conroll on world 1
			if(w.getName()==this.world[0].getName()){
				if(w.getCurrentStep()>this.world[1].getCurrentStep())
					return 1;
				else if(w.getCurrentStep()<this.world[1].getCurrentStep()){
					//do not update view
					w.setUpdateView(false);
					return -1;
				}
				else
					return 0;
			}
			//if control on world II
			else if(w.getName()==this.world[1].getName()){
				if(w.getCurrentStep()>this.world[0].getCurrentStep())
					return 1;
				else if(w.getCurrentStep()<this.world[0].getCurrentStep()){
					//do not update view
					w.setUpdateView(false);
					return -1;
				}
				else
					return 0;
			}
		}
		return 0;
	}
	/**
	 * Stop the threads containing the worlds 
	 */
	public void stopRunner(){
		if(this.worldRunnable[0]!=null)
			this.worldRunnable[0].radicalPause();
		if(this.mode==Mode.comparison){
			if(this.worldRunnable[1]!=null)
				this.worldRunnable[1].radicalPause();
		}
	}
	/**
	 * return the mode
	 * @return
	 */
	public Mode getMode(){
		return this.mode;
	}
}
