package test.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import model.Agent;
import model.monopoly.PatentOffice;

import org.junit.Test;

public class PatentOfficeTest {

	@Test
	public void patentTest() {
		for(int j = 0 ;  j < 10000 ; ++j){
			int numberOfPatents = 10;
			
			Agent[] agents = new Agent[50];
			Double money = new Double(500);
			Double utility = new Double(0);
			int numberOfProducts = 10;
			int specializedProduction = 100;
			double partRandomProduction = 0;
			boolean includeStocksInWealth = false;
			boolean decreasingUtility = false;
			boolean unequalMoney = false;
			
			PatentOffice po = new PatentOffice();
			po.setNumberOfPatents(numberOfPatents);
			
			for(int i = 0 ; i < agents.length ; ++i){
				agents[i] = new Agent(money, utility, numberOfProducts, specializedProduction, partRandomProduction, includeStocksInWealth, decreasingUtility, unequalMoney, i,po);
			}
			
			po.init(agents);
			
			assertTrue("Patent number",po.getPatents().size() == numberOfPatents);
			
			ArrayList<Agent> agentsWithPatent = new ArrayList<Agent>();
			ArrayList<Integer> unpatentedProducts = new ArrayList<Integer>();
			for(int product = 0 ; product < numberOfProducts ; ++product){
				Agent a = po.getPatentOwner(product);
				if(a != null){
					assertFalse("Agent have two patents",agentsWithPatent.contains(a));
					assertTrue("Agent can't produce",po.canProduce(a, product));
					agentsWithPatent.add(a);
				} else {
					unpatentedProducts.add(product);
				}
			}
			
			for(int product : unpatentedProducts){
				for(int i = 0 ; i < agents.length ; ++i){
					assertTrue("Agent cannot produce unpatented product",po.canProduce(agents[i], product));
				}
			}
		}
	}
	
	@Test
	public void noPatentTest() {
		int numberOfPatents = 0;
		
		Agent[] agents = new Agent[50];
		Double money = new Double(500);
		Double utility = new Double(0);
		int numberOfProducts = 10;
		int specializedProduction = 100;
		double partRandomProduction = 0;
		boolean includeStocksInWealth = false;
		boolean decreasingUtility = false;
		boolean unequalMoney = false;
		
		PatentOffice po = new PatentOffice();
		po.setNumberOfPatents(numberOfPatents);
		for(int i = 0 ; i < agents.length ; ++i){
			agents[i] = new Agent(money, utility, numberOfProducts, specializedProduction, partRandomProduction, includeStocksInWealth, decreasingUtility, unequalMoney, i,po);
		}
		
		po.init(agents);
		
		boolean allAgentsCanProduceEverything = true;
		for(int product = 0 ; product < numberOfProducts ; ++product){
			for(Agent a : agents){
				if(!po.canProduce(a, product)){
					allAgentsCanProduceEverything = false;
					break;
				}
			}
		}
		
		assertTrue("An agent has a product he cannot produce",allAgentsCanProduceEverything);
	}
}
