package test.helper;

import static org.junit.Assert.*;

import java.util.ArrayList;

import helper.Helper;

import org.junit.Test;

public class HelperTester {

	@Test
	public void getMinIndexesTest() {
		double[] array = new double[5];
		array[0] = 0.1;
		array[1] = 0.1;
		array[2] = 0.05;
		array[3] = 0.2;
		array[4] = 0.05;
		
		ArrayList<Integer> indexes = Helper.getMinIndexes(array);
		
		assertTrue("Size 2",indexes.size() == 2);
		assertTrue("2 in", indexes.contains(2));
		assertTrue("4 in", indexes.contains(4));
	}
	
	@Test
	public void getAscendingIndexesTest(){
		double[] array = new double[5];
		array[0] = 0.1;
		array[1] = 0.1;
		array[2] = 0.05;
		array[3] = 0.2;
		array[4] = 0.05;
		
		ArrayList<Integer> indexes = Helper.getAscendingIndexes(array);
		
		assertTrue(indexes.get(0) == 2 || indexes.get(0) == 4 );
		assertTrue(indexes.get(1) == 2 || indexes.get(1) == 4 );
		assertTrue(indexes.get(0) != indexes.get(1));
	
		assertTrue(indexes.get(2) == 0 || indexes.get(2) == 1 );
		assertTrue(indexes.get(3) == 0 || indexes.get(3) == 1 );
		assertTrue(indexes.get(2) != indexes.get(3));
		
		assertTrue(indexes.get(4) == 3);
	}
}
